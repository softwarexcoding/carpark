<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'th',
    'timeZone' => 'Asia/Bangkok',
    'aliases'=>[
        '@msoft/user' => '@common/modules/yii2-user',
        '@msoft/admin' => '@common/modules/yii2-admin',
        '@msoft/menu' => '@common/modules/yii2-menu',
        '@metronic' => '@common/themes/metronic',
        '@authen' => '@common/themes/metronic',
    ],
    'modules' => [
        'user' => [
            'class' => 'msoft\user\Module',
            'enableConfirmation' => false,
            'enableFlashMessages' => false,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin'],
        ],
        'core' => [
            'class' => 'msoft\core\Module',
        ],
        'admin' => [
            'class' => 'msoft\admin\Module',
            'layout' => 'top-menu'
        ],
        'menu' => [
            'class' => 'msoft\menu\Module',
        ],
        'gridview' => [
            'class' => 'msoft\widgets\grid\Module'
        ],
    ],
    'components' => [
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'en_US',
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ],
            ],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeFormat' => 'php:H:i:s',
            'defaultTimeZone' => 'Asia/Bangkok',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\PhpManager'
        ]
    ],
    'params' => [
        'icon-framework' => 'fa',  // Font Awesome Icon framework
        'robotEmail' => 'yii2-startup@gmail.com',
    ]
];
