<?php
return [
    'supportEmail' => 'support@example.com',
    'adminEmail' => getenv('ADMIN_EMAIL'),
    'robotEmail' => getenv('ROBOT_EMAIL'),
    'user.passwordResetTokenExpire' => 3600,
];
