<?php

namespace msoft\core\assets;

use yii\web\AssetBundle;

class FontAwesomeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@msoft/core/assets/font-awesome';

    /**
     * @inheritdoc
     */
    public $publishOptions = [
        'only' => ['fonts/*', 'css/*']
    ];

    public $css = [
		'css/font-awesome.css'
	];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    public $depends = [
		'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
	];
}
