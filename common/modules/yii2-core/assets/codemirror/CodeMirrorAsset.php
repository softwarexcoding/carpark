<?php
namespace msoft\core\assets\codemirror;

use Yii;
use yii\web\AssetBundle;

class CodeMirrorAsset extends AssetBundle
{
    public $sourcePath = '@msoft/core/assets/codemirror';
    public $css = [
        'lib/codemirror.css?v=1.0',
        'theme/ambiance.css?v=1.0'
    ];

    public $js = [
        'lib/codemirror.js?v=1.0',
        'mode/javascript/javascript.js?v=1.0',
        'mode/htmlmixed/htmlmixed.js?v=1.0',
        'mode/css/css.js?v=1.0',
        'mode/xml/xml.js?v=1.0',
        'mode/php/php.js?v=1.0',
        'mode/clike/clike.js?v=1.0',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}