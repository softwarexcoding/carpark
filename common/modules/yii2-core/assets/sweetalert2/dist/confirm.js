yii.allowAction = function ($e) {
    var message = $e.data("confirm");
    return message === underfined || yii.confirm(message, $e);
};
yii.confirm = function (message,ok,cancel) {
    swal({
        title: message,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Confirm",
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function(isConfirm) {
            if (isConfirm) {
                if (isConfirm) {
                    !ok || ok();
                } else {
                    !cancel || cancel();
                }
            }
    });
    return false;
};