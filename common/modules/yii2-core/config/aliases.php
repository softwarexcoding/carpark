<?php
Yii::setAlias('@msoft/core', '@common/modules/yii2-core');
Yii::setAlias('@msoft/widgets', '@msoft/core/widgets');
Yii::setAlias('@msoft/mpdf', '@msoft/core/components/mpdf');
Yii::setAlias('@msoft/helpers', '@msoft/core/helpers');
Yii::setAlias('@msoft/components', '@msoft/core/components');
Yii::setAlias('@msoft/behaviors', '@msoft/core/behaviors');
Yii::setAlias('@msoft/utils', '@msoft/core/utils');
Yii::setAlias('@msoft/gii', '@msoft/core/gii-template');
/*
=== Aliases Config ===
#Add to index.php @frontend/web, @backend/web

require(__DIR__ . '/../../common/modules/yii2-core/config/aliases.php');

=== Component Config ===

'i18n' => [
    'translations' => [
        'core' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@msoft/core/messages', // if advanced application, set @frontend/messages
            'sourceLanguage' => 'en_US',
            'fileMap' => [
                'app' => 'app.php',
            ],
        ],
        'kvdynagrid' => [
            'class'=>'yii\i18n\PhpMessageSource',
            'basePath'=>'@msoft/widgets/dynagrid/messages',
            'forceTranslation'=>true
        ],
    ],
],

=== Module Config ===

'modules' => [
    'core' => [
        'class' => '\msoft\core\Module'
    ],
    
    'gridview' =>  [
        'class' => '\msoft\widgets\grid\Module'
    ],

    'dynagrid'=> [
        'class'=>'\msoft\widgets\dynagrid\Module',
        // other module settings
    ],
],


=== Params Config ===

'params' => [
    'icon-framework' => 'fa',  // Font Awesome Icon framework
]*/