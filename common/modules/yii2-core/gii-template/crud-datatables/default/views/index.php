<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use <?= $generator->indexWidgetType === 'grid' ? "msoft\widgets\Datatables" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */
RegisterJS::regis(['sweetalert'],$this);

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
');
?>
<?= "<?= " ?>SwalAlert::widget(); ?>
<?= "<?= " ?>Panel::begin([
    'title'=> '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>',
    'type' => Panel::TYPE_PRIMARY,
    'footer' => 'Footer'
]);
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">

    <?php /*<h1><?= "<?= " ?>Html::encode($this->title) ?></h1>*/?>
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>
<?php /*
    <p>
        <?= "<?= " ?>Html::a(<?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn dark btn-outline']) ?>
    </p>*/?>
<?= $generator->enablePjax ? '<?php Pjax::begin(["id" => "crud-datatable-pjax"]); ?>' : '' ?>
<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>Datatables::widget([
        'dataProvider' => $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'clientOptions' => [
            'language' => [
                "search" => "ค้นหา: _INPUT_" .'&nbsp;'. Html::a(Icon::show('plus'),['create'],['class' => 'btn btn-default','role' => 'modal-remote']),
            ],
            /*"dom"=> "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            'buttons' => [
                ['extend' => 'print','className' => 'btn dark btn-outline'],
                ['extend' => 'copy','className' => 'btn red btn-outline'],
                ['extend' => 'pdf','className' => 'btn green btn-outline'],
                ['extend' => 'excel','className' => 'btn yellow btn-outline'],
                ['extend' => 'csv','className' => 'btn purple btn-outline'],
                ['extend' => 'colvis','className' => 'btn dark btn-outline'],
            ],*/
        ],
        'hover' => true,
        'datatables' => true,
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            ['class' => 'msoft\widgets\grid\SerialColumn'],
            [
                'class'=>'msoft\widgets\grid\ExpandRowColumn',
                'width'=>'50px',
                'value'=>function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'headerOptions'=>['class'=>'kartik-sheet-style'] ,
                'expandOneOnly'=>true,
                'detailUrl' => yii\helpers\Url::to(['/data-expand'])
            ],
<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            // '" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        } else {
            echo "            // '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>

            [
                'class' => 'msoft\widgets\ActionColumn',
                'noWrap' => TRUE,
                'viewOptions' => [
                    'class' => 'btn btn-xs btn-success tooltips',
                    'role' => 'modal-remote'
                ],
                'updateOptions' => [
                    'class' => 'btn btn-xs btn-primary tooltips',
                    'role' => 'modal-remote'
                ],
                'deleteOptions' => [
                    'class' => 'btn btn-xs btn-danger tooltips',
                ],
            ],
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>
<?= $generator->enablePjax ? '<?php Pjax::end(); ?>' : '' ?>
</div>
<?= "<?= " ?>Panel::end(); ?>

<?= "<?php echo " ?>$this->render('modal'); ?>