<?php

namespace msoft\helpers;

use Yii;
use yii\helpers\BaseHtml;
use msoft\widgets\Icon;
use yii\helpers\Url;
use yii\web\JsExpression;

class Html extends BaseHtml {

	public static function getMsgSuccess() {
		return '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ';
	}

	public static function getMsgError() {
		return '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ';
	}

	public static function getBtnAdd() {
		return '<span class="glyphicon glyphicon-plus"></span>';
	}

	public static function getBtnDelete() {
		return '<span class="glyphicon glyphicon-minus"></span>';
	}

	public static function getBtnRepeat() {
		return '<span class="glyphicon glyphicon-repeat"></span>';
	}

	public static function textDelete(){
		return Icon::show('trash-o').Yii::t('yii','Delete');
	}

	public static function textEdit(){
		return Icon::show('icon-pencil',[],Icon::I).'แก้ไข';
	}

	public static function textView(){
		return Icon::show('icon-eye',[],Icon::I).Yii::t('yii','View');
	}

	public static function aDelete($text, $url = null, $options = [])
    {
        if ($url !== null) {
			$options['href'] = Url::to($url);
			$pjaxid = isset($options['pjaxreload']) ? $options['pjaxreload'] : '#';
			$method = isset($options['method']) ? $options['method'] : 'post';
			$options['onclick'] = new JsExpression('
				swal({
					title: "ยืนยันการลบ?",
					text: "",
					icon: "warning",
					buttons: ["Cancel", "Confirm"],
				})
				.then((willDelete) => {
					if (willDelete) {
						$.'.$method.'("'.$options['data-url'].'", function( data ) {
							if("'.$pjaxid.'" !== "#"){
								$.pjax.reload({container:"'.$pjaxid.'"});
							}
						});
					}
				});
			');
        }
        return Html::tag('a', $text, $options);
	}
	
}