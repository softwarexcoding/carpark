<?php

namespace msoft\core\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use msoft\core\models\CoreForms;

/**
 * CoreFormsSearch represents the model behind the search form about `msoft\core\models\CoreForms`.
 */
class CoreFormsSearch extends CoreForms
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_id', 'labelSpan', 'order', 'created_by', 'updated_by'], 'integer'],
            [['row', 'action_id', 'attributes', 'label', 'type', 'labelOptions', 'hint', 'items', 'widgetClass', 'options', 'columnOptions', 'columns', 'created_at', 'updated_at', 'visible', 'table_name','value','fieldConfig'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreForms::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'form_id' => $this->form_id,
            'visible' => $this->visible,
            'labelSpan' => $this->labelSpan,
            'order' => $this->order,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'row', $this->row])
            ->andFilterWhere(['like', 'action_id', $this->action_id])
            ->andFilterWhere(['like', 'attributes', $this->attributes])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'labelOptions', $this->labelOptions])
            ->andFilterWhere(['like', 'hint', $this->hint])
            ->andFilterWhere(['like', 'items', $this->items])
            ->andFilterWhere(['like', 'widgetClass', $this->widgetClass])
            ->andFilterWhere(['like', 'options', $this->options])
            ->andFilterWhere(['like', 'columnOptions', $this->columnOptions])
            ->andFilterWhere(['like', 'table_name', $this->table_name])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'columns', $this->columns]);

        return $dataProvider;
    }
}