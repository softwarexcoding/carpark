<?php
use msoft\widgets\CodeMirror;
use msoft\widgets\JsTree;
use yii\helpers\Html;
use yii\web\JsExpression;
use msoft\widgets\Icon;
use yii\helpers\Url;
use msoft\helpers\RegisterJS;
use msoft\widgets\ActiveForm;
use msoft\core\classes\JSQuery;
use yii\jui\JuiAsset;

#RegisterJS
JuiAsset::register($this);
RegisterJS::regis(["sweetalert","bootbox"],$this);


$this->title = 'Editor';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.vakata-context li a{
    padding-left: 25px;
}
.vakata-context ul {
    z-index: 1000;
}
.icon-state-success {
    color: #36c6d3;
}
.icon-state-warning {
    color: #F1C40F;
}
.CodeMirror {
    font-size:11pt;
    border: 1px solid #eee;
    height: auto;
}
</style>

<div class="row">
    <div class="col-md-3" style="overflow: auto;">
        <p><?= Html::input('text', 'search', '', ['class' => 'form-control','id' => 'search','placeholder' => 'Search']) ?></p>
        <?=  JsTree::widget([
                'name' => 'js_tree',
                'id' => 'js_tree',
                'core' => [
                    'themes' => [
                        'responsive' => false
                    ],
                    'check_callback' => true,
                    'multiple' => false,
                    /*'data' => [
                        [
                            'id' => 'myapp',
                            "text" => "NSDU-APP ". Html::a(Icon::show('refresh'),false,['class' => 'btn dark btn-xs btn-outline','onclick' => 'Apps.Refresh(this);']),
                            'children' => 
                            [
                                // ['text' => 'backend','children' => $backend],
                                // ['text' => 'common','children' => $common],
                                // ['text' => 'frontend','children' => $frontend],
                            ],
                            "state" => [
                                "opened" => true
                            ],
                        ]
                    ],*/
                ],
                'plugins' => [
                    "contextmenu", "dnd", "state", "types","search","sort","unique","wholerow","changed",
                ],
                'contextmenu' => [
                    'items' => new JsExpression(<<<JS
                        function(node) {
                            var tree = $("#jsTree_js_tree").jstree(true);
                            return {
                                "New Folder": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Folder",
                                    "icon" : "fa fa-plus",
                                    "action": function (data) {
                                        bootbox.prompt("Enter your folder name?", function(result) {
                                            if (result === null) {
                                                console.log("Prompt dismissed");
                                            } else {
                                                var inst = $.jstree.reference(data.reference),
                                                    obj = inst.get_node(data.reference);

                                                var d = new Date();
                                                var nodeid = tree.create_node(node.id, { 
                                                    id:d.getTime(),
                                                    text: result,
                                                    icon:"fa fa-folder icon-state-warning icon-lg",
                                                    type:"default",
                                                    data: node.data + "/" + result
                                                }, 'last');
                                                var newnode = inst.get_node(nodeid);
                                                Apps.CreateFolder(newnode,node);
                                            }
                                        });
                                        //tree.edit(node);
                                    }
                                },  
                                "New File": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New File",
                                    "icon" : "fa fa-plus",
                                    "action": function (data) {
                                        bootbox.prompt("Enter your file name?", function(result) {
                                            if (result === null) {
                                                console.log("Prompt dismissed");
                                            } else {
                                                var inst = $.jstree.reference(data.reference),
                                                    obj = inst.get_node(data.reference);

                                                var d = new Date();
                                                var nodeid = tree.create_node(node.id, { 
                                                    id:d.getTime(),
                                                    text: result,
                                                    icon:"fa fa-file-text-o icon-state-default",
                                                    type:"file",
                                                    data: node.data + "/" + result
                                                }, 'last');
                                                var newnode = inst.get_node(nodeid);
                                                Apps.CreateFile(newnode);
                                            }
                                        });
                                        
                                        // //node = tree.create_node(node);
                                        // var newnode = tree.edit(currentnode);
                                    // tree.set_icon(node,"test.php");
                                    }
                                },
                                "Rename": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "Rename",
                                    "icon" : "fa fa-pencil-square-o",
                                    "action": function (data) { 
                                        var inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);

                                        bootbox.prompt({
                                            title: "Enter your new name?",
                                            value: node.text,
                                            callback: function (result) {
                                                if (result === null) {
                                                    console.log("Prompt dismissed");
                                                } else {
                                                    var parentnode = inst.get_node(node.parent);
                                                    var old = node.text;
                                                    var text = result;
                                                    Apps.Rename(parentnode,old,text,node,inst);
                                                }
                                            }
                                        });

                                        
                                        //tree.delete_node(node);
                                        
                                        //tree.edit(node);
                                        //tree.set_text(node,"444");
                                    }
                                },
                                "ccp": {
                                    "separator_before": true,
                                    "separator_after": false,
                                    "label": "Edit",
                                    "icon" : "fa fa-pencil-square-o",
                                    "action": false,
                                    "submenu" : {
                                        "cut" : {
                                            "separator_before"	: false,
                                            "separator_after"	: false,
                                            "label"				: "Cut",
                                            "action"			: function (data) {
                                                var inst = $.jstree.reference(data.reference),
                                                    obj = inst.get_node(data.reference);
                                                if(inst.is_selected(obj)) {
                                                    inst.cut(inst.get_top_selected());
                                                }
                                                else {
                                                    inst.cut(obj);
                                                }
                                                var ids = [];
                                                var nodes = $('#jsTree_js_tree').jstree('get_selected', true);
                                                for (i = 0; i < nodes.length; i++) { 
                                                    ids.push(nodes[i].id);
                                                }
                                                $('input[id="input-copy"]').val(ids);
                                                $('#input-paste_type').val('cut');
                                            }
                                        },
                                        "copy" : {
                                            "separator_before"	: false,
                                            "icon"				: false,
                                            "separator_after"	: false,
                                            "label"				: "Copy",
                                            "action"			: function (data) {
                                                var inst = $.jstree.reference(data.reference),
                                                    obj = inst.get_node(data.reference);
                                                
                                                if(inst.is_selected(obj)) {
                                                    inst.copy(inst.get_top_selected());
                                                }
                                                else {
                                                    inst.copy(obj);
                                                }
                                                
                                                //Set Value
                                                var ids = [];
                                                var nodes = $('#jsTree_js_tree').jstree('get_selected', true);
                                                for (i = 0; i < nodes.length; i++) { 
                                                    ids.push(nodes[i].id);
                                                }
                                                $('input[id="input-copy"]').val(ids);
                                                $('#input-paste_type').val('copy');
                                            }
                                        },
                                        "paste" : {
                                            "separator_before"	: false,
                                            "icon"				: false,
                                            "_disabled"			: function (data) {
                                                return !$.jstree.reference(data.reference).can_paste();
                                            },
                                            "separator_after"	: false,
                                            "label"				: "Paste",
                                            "action"			: function (data) {
                                                var inst = $.jstree.reference(data.reference),
                                                    obj = inst.get_node(data.reference);
                                                
                                                Apps.Paste(obj,inst);
                                            }
                                        }
                                    }
                                },                        
                                "Delete": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "Delete",
                                    "icon" : "fa fa-trash-o text-danger",
                                    "action": function (data) {
                                        var inst = $.jstree.reference(data.reference),
                                            obj = inst.get_node(data.reference);
                                        var parentnode = inst.get_node(obj.parent);
                                        // if(inst.is_selected(obj)) {
                                        //     inst.delete_node(inst.get_selected());
                                        // }
                                        // else {
                                        //     inst.delete_node(obj);
                                        // }
                                        Apps.DeleteNode(obj,parentnode);
                                        //tree.delete_node(node);
                                    }
                                }
                            }
                        }
JS
),
                ],
                'types' => [
                    'default' => [
                        'icon' => 'fa fa-folder icon-state-warning icon-lg'
                    ],
                    'file' => [
                        'icon' => 'fa fa-file icon-state-warning icon-lg'
                    ],
                ],
            ]); ?>
    </div>
    <div class="col-md-9">
    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL,'id' => 'form-code']); ?>
        <div class="form-group">
            <?= Html::tag('label','&nbsp;&nbsp; File Path',['class' => 'control-label']); ?>
            <div class="col-sm-12">
                <p><?= Html::input('text', 'dir', '', ['class' => 'form-control','id' => 'input-dir']) ?></p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <?php
                echo CodeMirror::widget([
                    'name' =>'Code',
                    'options' => [
                        'id' => 'code',
                    ],
                    'clientOptions' => [
                        'lineNumbers' => true,
                        'matchBrackets' => true,
                        'styleActiveLine' => true,
                        'theme' => 'ambiance',
                        'mode' => 'text/x-php',
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="form-group" style="text-align:right;">
            <div class="col-sm-12">
                <div class="skin-option" id="draggable">
                    <?= Html::submitButton(Icon::show('save'). ' Save', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>

<?= Html::hiddenInput('copy','',['class' => 'form-control','id' => 'input-copy']); ?>
<?= Html::hiddenInput('paste_type','',['class' => 'form-control','id' => 'input-paste_type']); ?>
<?php
JSQuery::generateJS('core/core-editor/index',$this);
?>