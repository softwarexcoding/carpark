<?php

use msoft\helpers\Html;
use msoft\widgets\ActiveForm;
use msoft\widgets\Form;
use msoft\helpers\Noty;
use msoft\widgets\Typeahead;
use msoft\widgets\Select2;
use msoft\widgets\CheckboxX;
use msoft\core\classes\CoreQuery;
use msoft\core\models\CoreFields;
use yii\helpers\ArrayHelper;
use msoft\widgets\CodeMirror;
use msoft\widgets\TouchSpin;
use msoft\widgets\DepDrop;
use yii\helpers\Url;
?>
<style>
.CodeMirror {
    border: 1px solid #eee;
    height: auto;
}
</style>
<div class="core-forms-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName(),]); ?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="itemModalLabel"><?= $this->title ?></h4>
    </div>
    <div class="modal-body">
    <?php 
        #Rows 1
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>4,
            'attributes'=>[
                'row' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => TouchSpin::classname(),
                    'options' => [
                        'options'=>['placeholder'=>'Row...'],
                        'pluginOptions' => [
                            'buttonup_class' => 'btn btn-default', 
                            'buttondown_class' => 'btn btn-default', 
                            'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
                            'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                        ]
                    ]
                ],
                'action_id' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => Typeahead::classname(),
                    'options'=>[
                        'options' => ['placeholder' => 'Filter Action...'],
                        'pluginOptions' => ['highlight' => true, 'minLength' => 0],
                        'dataset' => [
                            [
                                'local' => CoreQuery::getAppsRoutes(),
                                'limit' => 20
                            ]
                        ]
                    ]
                ],
                'table_name' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options'=>[
                        'options' => ['placeholder' => 'Filter Table Name...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'data' => CoreQuery::getTableNames(),
                    ]
                ],
                'attributes' => [ 'type'=>Form::INPUT_TEXT, 'options'=>[]],
                /*
                'attributes' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => DepDrop::classname(),
                    'options'=>[
                        'type'=>DepDrop::TYPE_SELECT2,
                        'data' => $model->getColumn($model['table_name'],$model['attributes']),
                        'pluginOptions'=>[
                            'depends'=>['coreforms-table_name'],
                            'placeholder'=>'Select...',
                            'url'=> Url::to(['/core/core-forms/child-columns'])
                        ],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    ]
                ],*/
            ]
        ]);
        #Rows 2
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>3,
            'attributes'=>[
                'label' => [ 'type'=>Form::INPUT_TEXT, 'options'=>['maxlength' => true]],
                'type' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options'=>[
                        'options' => ['placeholder' => 'Select InputType...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'data' => [
                            Form::INPUT_TEXT => 'INPUT_TEXT',
                            Form::INPUT_TEXTAREA => 'INPUT_TEXTAREA',
                            Form::INPUT_PASSWORD => 'INPUT_PASSWORD',
                            Form::INPUT_DROPDOWN_LIST => 'INPUT_DROPDOWN_LIST',
                            Form::INPUT_LIST_BOX => 'INPUT_LIST_BOX',
                            Form::INPUT_CHECKBOX => 'INPUT_CHECKBOX',
                            Form::INPUT_RADIO => 'INPUT_RADIO',
                            Form::INPUT_CHECKBOX_LIST => 'INPUT_CHECKBOX_LIST',
                            Form::INPUT_RADIO_LIST => 'INPUT_RADIO_LIST',
                            Form::INPUT_MULTISELECT => 'INPUT_MULTISELECT',
                            Form::INPUT_FILE => 'INPUT_FILE',
                            Form::INPUT_HTML5 => 'INPUT_HTML5',
                            Form::INPUT_WIDGET => 'INPUT_WIDGET',
                            Form::INPUT_STATIC => 'INPUT_STATIC',
                            Form::INPUT_HIDDEN => 'INPUT_HIDDEN',
                            Form::INPUT_HIDDEN_STATIC => 'INPUT_HIDDEN_STATIC',
                            Form::INPUT_RAW => 'INPUT_RAW',
                            Form::INPUT_CHECKBOX_BUTTON_GROUP => 'INPUT_CHECKBOX_BUTTON_GROUP',
                            Form::INPUT_RADIO_BUTTON_GROUP => 'INPUT_RADIO_BUTTON_GROUP'
                        ]
                    ]
                ],
                'visible' => [ 'type'=>Form::INPUT_TEXT, 'options'=>[]],
            ]
        ]);
        #Rows 3
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>3,
            'attributes'=>[
                'labelSpan' => [ 'type'=>Form::INPUT_TEXT, 'options'=>[]],
                'widgetClass' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options'=>[
                        'options' => ['placeholder' => 'Select widgetClass...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'data' => ArrayHelper::map(CoreFields::find()->where(['<>', 'field_class', ''])->asArray()->all(),'field_class','field_class')
                    ]
                ],
                'order' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => TouchSpin::classname(),
                    'options' => [
                        'options'=>['placeholder'=>'จัดเรียง...'],
                        'pluginOptions' => [
                            'buttonup_class' => 'btn btn-default', 
                            'buttondown_class' => 'btn btn-default', 
                            'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
                            'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                        ]
                    ]
                ],
            ]
        ]);

        #Rows 4
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>12,
            'attributes'=>[
                'columns' => [ 
                    'type'=>Form::INPUT_TEXT, 
                    'options'=>['maxlength' => true],
                    'columnOptions'=>['colspan'=>4],
                ],
                'hint' => [ 
                    'type'=>Form::INPUT_TEXT, 
                    'options'=>[],
                    'columnOptions'=>['colspan'=>8],
                ],
            ]
        ]);

        #Rows 5
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>1,
            'attributes'=>[
                'value' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => CodeMirror::classname(),
                    'columnOptions'=>['colspan'=>8],
                    'options'=>[
                        'options' => [
                            'id' => 'value'
                        ],
                        'clientOptions' => [
                            'lineNumbers' => true,
                            'matchBrackets' => true,
                            'styleActiveLine' => true,
                            'theme' => 'ambiance',
                            'mode' => 'text/x-php',
                        ],
                    ]
                ],
            ]
        ]);

        #Rows 6
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>1,
            'attributes'=>[
                'options' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => CodeMirror::classname(),
                    'hint' => 'Array',
                    'options'=>[
                        'options' => [
                            'id' => 'options',
                            'value' => $model->isNewRecord ? "[]"  : $model['options']
                        ],
                        'clientOptions' => [
                            'lineNumbers' => true,
                            'matchBrackets' => true,
                            'styleActiveLine' => true,
                            'theme' => 'ambiance',
                            'mode' => 'text/x-php',
                        ],
                    ]
                ],
            ]
        ]);

        #Rows 7
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>1,
            'attributes'=>[
                'fieldConfig' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => CodeMirror::classname(),
                    'hint' => 'Array',
                    'options'=>[
                        'options' => [
                            'id' => 'fieldConfig',
                            'value' => $model->isNewRecord ? "['showLabels' => false]"  : $model['fieldConfig']
                        ],
                        'clientOptions' => [
                            'lineNumbers' => true,
                            'matchBrackets' => true,
                            'styleActiveLine' => true,
                            'theme' => 'ambiance',
                            'mode' => 'text/x-php',
                        ],
                    ]
                ],
            ]
        ]);

        #Rows 8
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>1,
            'attributes'=>[
                'columnOptions' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => CodeMirror::classname(),
                    'columnOptions'=>['colspan'=>8],
                    'hint' => 'Array',
                    'options'=>[
                        'options' => [
                            'id' => 'columnOptions',
                            'value' => $model->isNewRecord ? "['colspan'=>4]"  : $model['columnOptions']
                        ],
                        'clientOptions' => [
                            'lineNumbers' => true,
                            'matchBrackets' => true,
                            'styleActiveLine' => true,
                            'theme' => 'ambiance',
                            'mode' => 'text/x-php',
                        ],
                    ]
                ],
            ]
        ]);
        #Rows 9
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>1,
            'attributes'=>[
                'labelOptions' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => CodeMirror::classname(),
                    'hint' => 'Array',
                    'options'=>[
                        'options' => [
                            'id' => 'labelOptions',
                            'value' => $model->isNewRecord ? "['class'=>'col-sm-2 control-label']"  : $model['labelOptions']
                        ],
                        'clientOptions' => [
                            'lineNumbers' => true,
                            'matchBrackets' => true,
                            'styleActiveLine' => true,
                            'theme' => 'ambiance',
                            'mode' => 'text/x-php',
                        ],
                    ]
                ],
            ]
        ]);
        #Rows 10
        echo Form::widget([
            'model'=>$model,
            'form'=>$form,
            'columns'=>1,
            'attributes'=>[
                'items' => [
                    'type'=> Form::INPUT_WIDGET,
                    'widgetClass' => CodeMirror::classname(),
                    'hint' => 'Array',
                    'options'=>[
                        'options' => [
                            'id' => 'items'
                        ],
                        'clientOptions' => [
                            'lineNumbers' => true,
                            'matchBrackets' => true,
                            'styleActiveLine' => true,
                            'theme' => 'ambiance',
                            'mode' => 'text/x-php',
                        ],
                    ]
                ],
            ]
        ]);
        
    ?>
    </div>

    <div class="modal-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-danger']) ?>
        <?php if (!Yii::$app->request->isAjax) : ?>
            <?= Html::a('Close',Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
        <?php else : ?>
            <?= Html::button('Close', ['class' => 'btn btn-default','data-dismiss' => 'modal']) ?>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php if (Yii::$app->request->isAjax) : ?>
<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
		\$form.attr('action'), //serialize Yii2 form
		\$form.serialize()
    ).done(function(result){
		if(result.status == 'success'){
			". Noty::show('result.message', 'result.status') ."
			if(result.action == 'create'){
				$(\$form).trigger('reset');
				$.pjax.reload({container:'#core-forms-grid-pjax'});
				//$('#rules').html(result.rules);
			} else if(result.action == 'update'){
				$(document).find('#modal-core-forms').modal('hide');
				$.pjax.reload({container:'#core-forms-grid-pjax'});
			}
		} else{
			". Noty::show('result.message', 'result.status') ."
		} 
    }).fail(function(){
		". Noty::show("'" . Html::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
    });
    return false;
});

");?>
<?php elseif (Yii::$app->controller->action->id == 'update') : ?>
<?php

$this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
		\$form.attr('action'), //serialize Yii2 form
		\$form.serialize()
    ).done(function(result){
		if(result.status == 'success'){
			". Noty::show('result.message', 'result.status') ."
			if(result.action == 'create'){
				$(\$form).trigger('reset');
			} else if(result.action == 'update'){
				$(document).find('#modal-core-forms').modal('hide');
			}
		} else{
			". Noty::show('result.message', 'result.status') ."
		} 
    }).fail(function(){
		". Noty::show("'" . Html::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
    });
    return false;
});

");?>
<?php endif; ?>