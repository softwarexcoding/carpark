<?php

use yii\helpers\Html;
use msoft\widgets\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel metronic\core\models\CoreGenerateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core Generates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-generate-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Core Generate', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'gen_id',
            'gen_group',
            'gen_name',
            'gen_tag:ntext',
            'gen_link:ntext',
            // 'gen_process:ntext',
            // 'gen_ui:ntext',
            // 'template_php:ntext',
            // 'template_html:ntext',
            // 'template_js:ntext',
            // 'updated_at',
            // 'updated_by',
            // 'created_at',
            // 'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>