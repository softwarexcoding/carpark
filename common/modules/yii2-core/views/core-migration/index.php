<?php
use msoft\widgets\GridView;
use msoft\widgets\ToastrFlash;
use msoft\helpers\Html;
use yii\bootstrap\Modal;
use msoft\helpers\RegisterJS;
use yii\widgets\Pjax;

RegisterJS::regis(['ajaxcrud'],$this);

$this->title = 'Core Migrations';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= ToastrFlash::widget() ?>

<style>
.modal-dialog {
    width: 80%;
}
</style>

<h3><?= $this->title ?></h3>
<?php Pjax::begin(['id' => 'table-migration-pjax']); ?>
<?php 
echo GridView::widget([
    'dataProvider'=> $dataProvider,
    'panel' => [
        'heading'=> false,
        'before'=>	Html::a('Generate',['/core/core-migration/gii-migration'],['class' => 'btn btn-success']).' '.
                    Html::a('DeleteAll',false,['class' => 'btn btn-danger','onclick' => 'Apps.DeleteAll(this);']),
        'after'=>false,
    ],
    'hover' => true,
    'pjax' => true,
    'condensed' => true,
    'export' => [
        'fontAwesome' => true
    ],
    'columns' => [
        [
            'class' => '\msoft\widgets\grid\SerialColumn'
        ],
        [
            'attribute' => 'folder_name',
            'format' => 'text',
            'noWrap' => true,
            'hAlign' => 'left',
            'group'=>true,
            'groupedRow'=>true,
            'groupOddCssClass'=>'kv-grouped-row',
            'groupEvenCssClass'=>'kv-grouped-row',
        ],
        [
            'attribute' => 'filename',
            'format' => 'text',
        ],
        [
            'attribute' => 'size',
            'format' => 'shortSize',
            'hAlign' => 'center'
        ],
        [
            'attribute' => 'directory_file',
            'format' => 'text',
        ],
        [
            'class' => 'msoft\widgets\ActionColumn',
            'template'=>'<div class="btn-group btn-group-sm text-center" role="group" style="width:70px"> {view} {delete} </div>',
            'buttons' => [
                'view' => function($url, $model, $key){
                    return Html::a('<i class="glyphicon glyphicon-eye-open"></i>',['/core/core-migration/view','id' => $model['folder_name'],'filename' => $model['filename']],[
                        'role' => 'modal-remote',
                        'class' => 'btn btn-default'
                    ]);
                }
            ],
            'updateOptions' => [
                'class' => 'btn btn-sm btn-default',
                'data-action' => 'update'
            ],
        ]
    ],
    'responsive'=>true,
    'hover'=>true
]);
?>
<?php Pjax::end(); ?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",
    "size" => "modal-lg"
])?>
<?php Modal::end(); ?>

<?php
$this->registerJs(<<<JS
Apps = {
    DeleteAll: function(){
        krajeeDialog.confirm("Are you sure you want to delete this item?", function (result) {
            if (result) { // ok button was pressed
                $.ajax({
                    "type":"POST",
                    "url":"/core/core-migration/delete-all",
                    "async": false,
                    "dataType":"json",
                    "success":function (result) {
                        $.pjax.reload({container:'#table-migration-pjax'});
                        swal("Success!", " ", "success");
                    },
                    "error":function (xhr, status, error) {
                        swal(error, status, "error");
                    }
                });
            } else { // confirmation was cancelled
                // execute your code for cancellation
            }
        });
    }
};
JS
);?>