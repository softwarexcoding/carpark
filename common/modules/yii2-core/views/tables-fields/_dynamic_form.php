<?php 
use msoft\widgets\ActiveForm;
use msoft\helpers\Html;
use msoft\helpers\ArrayHelper;
use msoft\widgets\DynamicFormWidget;
use msoft\widgets\Icon;
use msoft\widgets\Typeahead;
use yii\helpers\Json;
use msoft\core\models\TablesFields;
use msoft\core\classes\CoreFunc;
use msoft\core\models\CoreFields;
use msoft\widgets\Select2;
use msoft\widgets\CheckboxX;
use msoft\core\assets\codemirror\CodeMirrorAsset;
use msoft\widgets\TouchSpin;
use yii\bootstrap\Modal;
use msoft\helpers\RegisterJS;
use msoft\core\widgets\ModalForm;
use msoft\widgets\DepDrop;
use yii\helpers\Url;

CodeMirrorAsset::register($this);
RegisterJS::regis(['sweetalert','ajaxcrud'],$this);
$this->title = 'Dynamic Form';

$schema = Yii::$app->db->schema;
$Tablenames = [];
foreach($schema->getTablenames() as $name){
    $Tablenames[$name] = $name;
}
$query = TablesFields::find()->all();

$this->registerCss("
.line-dashed {
    background-color: transparent;
    border-bottom: 1px dashed #c9302c !important;
}
.CodeMirror {
    border: 1px solid #eee;
    height: auto;
}
/*
textarea:focus { 
    background-color: #434a54;
    color:white;
}
input:focus { 
    background-color: #434a54;
    color:white;
}*/
");
?>
<?php $form = ActiveForm::begin(['id' => 'dynamic-form']);?>
    <div class="dynamic-form">
        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 50, // the maximum times, an element can be cloned (default 999)
            'min' => 0, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $models[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'full_name',
            ],
        ]); ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::button(Icon::show('plus').' เพิ่ม',['class' => 'add-item btn btn-success btn-sm']); ?>
                <?= Html::button(' Show all',['class' => 'show-all btn btn-default btn-sm']); ?>
                <?= Html::button(' Hide all',['class' => 'hide-all btn btn-default btn-sm']); ?>
                <span class="pull-right"><?= Html::encode('Tables Fields'); ?></span>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body container-items"><!-- widgetContainer -->
                <?php foreach ($models as $index => $model): ?>
                    <div class="item"><!-- widgetBody -->
                        <div class="padding-v-sm">
                            <div class="line line-dashed"></div>
                        </div>
                        <p>
                            
                            &nbsp;
                        </p>
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="pull-left title-dynamicform" style="font-size:12pt;"><?= Html::encode('Fields# '); ?><?= $model['input_label']; ?> <?= ($index + 1) ?> </span>
                            </div>
                            <div class="col-sm-6" style="text-align:right;">
                                <?= Html::a('Show',false,['class' => 'show-content-form btn btn-primary btn-xs','onclick' => 'Dynamic.toggleContent(this);']); ?>
                                <?= !$model->isNewRecord ? Html::a('Generate Code',false, ['class' => 'btn btn-default btn-xs','id' => 'btn-generate-code','table_id' => $model['table_id'],'onclick' => 'Dynamic.giiCodeTemplate(this);']) : '' ?>
                                <?= !$model->isNewRecord ? Html::a('RawSql',false, ['class' => 'btn btn-default btn-xs','id' => 'btn-generate-code','table_id' => $model['table_id'],'actionid' => $model['action_id'],'onclick' => 'Dynamic.giiRawSql(this);']) : '' ?>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i> ลบ</button>
                            </div>
                        </div>
                        <br>
                        <?php
                            // necessary for update action.
                            if (!$model->isNewRecord) {
                                echo Html::activeHiddenInput($model, "[{$index}]table_id");
                            }
                        ?>
                        <div class="content-form">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?=
                                        $form->field($model, "[{$index}]table_name")->widget(Select2::classname(), [
                                            'data' => $Tablenames,
                                            'options' => ['placeholder' => 'Select field ...'],
                                            'pluginOptions' => [
                                                    'allowClear' => true
                                            ],
                                            'pluginEvents' => [
                                                //"change" => "function() { Dynamic.initTypeaHeadOnchange($(this).val(),".$index."); }",
                                            ]
                                        ]);
                                    ?>
                                </div>

                                <div class="col-md-6">
                                    <?=
                                        $form->field($model, "[{$index}]action_id")->widget(Typeahead::classname(), [
                                            'options' => ['placeholder' => 'Filter Action...'],
                                            'pluginOptions' => ['highlight' => true, 'minLength' => 0],
                                            'dataset' => [
                                                [
                                                    'local' => ArrayHelper::getColumn($query,'action_id'),
                                                    'limit' => 20
                                                ]
                                            ]
                                        ]);
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field($model, "[{$index}]table_varname")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-3 sdbox-col">
                                    <?=
                                        $form->field($model, "[{$index}]table_field_type")->widget(Select2::classname(), [
                                                'data' => CoreFunc::itemAlias('field_type'),
                                                'options' => ['placeholder' => 'Select field ...'],
                                                'pluginOptions' => [
                                                        'allowClear' => true
                                                ],
                                        ]);
                                    ?>
                                </div>
                                <div class="col-md-2 sdbox-col">
                                    <?= $form->field($model, "[{$index}]table_length")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-2 sdbox-col">
                                    <?= $form->field($model, "[{$index}]table_default")->textInput([]) ?>
                                </div>
                                <div class="col-md-2 sdbox-col">
                                    <?= $form->field($model, "[{$index}]table_index")->dropDownList(CoreFunc::itemAlias('field_index'), ['prompt'=>'None']) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                <?=
                                    $form->field($model, "[{$index}]input_field")->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(CoreFields::find()->all(), 'field_code', 'field_code'),
                                            'options' => ['placeholder' => 'Select field ...'],
                                            'pluginOptions' => [
                                                    'allowClear' => true
                                            ],
                                    ]);
                                ?>
                                </div>
                                <div class="col-md-3 sdbox-col">
                                    <?= $form->field($model, "[{$index}]input_label")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-3 sdbox-col" style="padding-top: 21px;">
                                    <?= $form->field($model, "[{$index}]input_required",['showLabels'=>false])->widget(CheckboxX::classname(), [
                                        'pluginOptions'=>['threeState'=>false],
                                        'labelSettings' => [
                                            'label' => 'จำเป็น',
                                            'position' => CheckboxX::LABEL_RIGHT
                                        ],
                                        'options'=>['value' => !empty($model['input_required']) ? 1 : 0]
                                        
                                    ]);  ?>
                                    <?php // $form->field($model, 'input_required')->checkbox() ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, "[{$index}]input_hint")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                                <div class="col-md-6 sdbox-col">
                                    <?= $form->field($model, "[{$index}]input_data")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]input_validate")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]input_meta")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, "[{$index}]input_specific")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>

                                <div class="col-md-2">
                                    <?php 
                                        echo $form->field($model, "[{$index}]input_order")->widget(TouchSpin::classname(), [
                                            'options'=>['placeholder'=>'จัดเรียง...'],
                                            'pluginOptions' => [
                                                'buttonup_class' => 'btn btn-default', 
                                                'buttondown_class' => 'btn btn-default', 
                                                'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
                                                'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                                            ]
                                        ]);
                                    ?>
                                </div>

                                <div class="col-md-2 sdbox-col" style="padding-top: 25px;">
                                    <?= $form->field($model, "[{$index}]status")->widget(CheckboxX::classname(), [
                                        'pluginOptions'=>['threeState'=>false],
                                        'labelSettings' => [
                                            'label' => '',
                                            'position' => CheckboxX::LABEL_RIGHT
                                        ],
                                        'options'=>['value' => $model->isNewRecord ? 1 : $model['status']]
                                        
                                    ])->label('Show on Form');  ?>
                                </div>
                                <div class="col-md-2 sdbox-col" style="padding-top: 25px;">
                                    <?= !$model->isNewRecord ?  Html::a('<i class="glyphicon glyphicon-list"></i> Sort All',['/core/tables-fields/sort-input','id' => $model['table_id']],['class' => 'btn btn-primary','role' => 'modal-remote']) : ''; ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]begin_html")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, "[{$index}]end_html")->textArea(['rows' => 3,'class' => 'form-control auto-size']);?>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>
            </div><!-- End PanelBody /-->
        </div><!-- End Panel /-->
        <?php DynamicFormWidget::end(); ?>
    </div>
    <div class="modal-footer">
        <?= Html::a('Close',['/core/tables-fields/list-data'], ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>

<?php
$tbnames = Json::encode($schema->getTablenames());
$actionid = Json::encode(ArrayHelper::getColumn($query,'action_id'));
$this->registerJs($this->render('_auto_size.js'));
$this->registerJs(<<<JS
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    $('.auto-size').autosize({append: "\\n"});
    var indexs = [];

    jQuery(".dynamicform_wrapper a.show-content-form").each(function(index) {
        $(this).attr("content-form","content-form-"+ (index + 1));
    });

    jQuery(".dynamicform_wrapper div.content-form").each(function(index) {
        $(this).addClass("content-form-"+ (index + 1));
    });

    jQuery(".dynamicform_wrapper .title-dynamicform").each(function(index) {
        jQuery(this).html("Fields "  + (index + 1) +' '+ $('#tablesfields-'+index+'-input_label').val());
        indexs.push(parseInt(index));
    });
    Dynamic.initCodeMirror(indexs);
    Dynamic.hideContentForm(indexs);
    Dynamic.initTypeaHead(indexs);
    Dynamic.initCheckboxX(indexs);
    Dynamic.initSelect2(indexs);
    $('html, body').animate({
        scrollTop: $('footer').offset().top
    }, 'slow');
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

Dynamic = {
    initCodeMirror: function(indexs){
        var myTextAreainput_input_hint = document.getElementById('tablesfields-'+Math.max.apply(null, indexs)+'-input_hint');
        CodeMirror.fromTextArea(myTextAreainput_input_hint,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});

        var myTextAreainput_input_data = document.getElementById('tablesfields-'+Math.max.apply(null, indexs)+'-input_data');
        CodeMirror.fromTextArea(myTextAreainput_input_data,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});

        var myTextAreainput_input_validate = document.getElementById('tablesfields-'+Math.max.apply(null, indexs)+'-input_validate');
        CodeMirror.fromTextArea(myTextAreainput_input_validate,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});

        var myTextAreainput_input_meta = document.getElementById('tablesfields-'+Math.max.apply(null, indexs)+'-input_meta');
        CodeMirror.fromTextArea(myTextAreainput_input_meta,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});

        var myTextAreainput_input_specific = document.getElementById('tablesfields-'+Math.max.apply(null, indexs)+'-input_specific');
        CodeMirror.fromTextArea(myTextAreainput_input_specific,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});

        var myTextAreainput_begin_html = document.getElementById('tablesfields-'+Math.max.apply(null, indexs)+'-begin_html');
        CodeMirror.fromTextArea(myTextAreainput_begin_html,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});

        var myTextAreainput_end_html = document.getElementById('tablesfields-'+Math.max.apply(null, indexs)+'-end_html');
        CodeMirror.fromTextArea(myTextAreainput_end_html,{"lineNumbers":true,"matchBrackets":true,"styleActiveLine":true,"theme":"ambiance","mode":"text/x-php"});
    },
    toggleContent : function(e){
        if($('div.' + $(e).attr('content-form')).css('display') == 'none'){
            e.innerHTML = 'Hide';
            $('div.' + $(e).attr('content-form')).css('display','block');
        }else{
            e.innerHTML = 'Show';
            $('div.' + $(e).attr('content-form')).css('display','none');
        }
    },
    hideContentForm : function(indexs){
        //init form content hide
        var maxindex = Math.max.apply(null, indexs) + 1;
        $('div.content-form-' + maxindex ).css('display','none');
    },
    initCheckboxX : function(indexs){
        var index = Math.max.apply(null, indexs);
        if (jQuery('#tablesfields-'+index+'-input_required').data('checkboxX')) { 
            jQuery('#tablesfields-'+index+'-input_required').checkboxX('destroy'); 
        }
        jQuery('#tablesfields-'+index+'-input_required').checkboxX({"threeState":false});

        if (jQuery('#tablesfields-'+index+'-status').data('checkboxX')) { 
            jQuery('#tablesfields-'+index+'-status').checkboxX('destroy'); 
        }
        jQuery('#tablesfields-'+index+'-status').checkboxX({"threeState":false});
    },
    initTypeaHead : function (indexs){
        var index = Math.max.apply(null, indexs);
        /*var tablesfields_table_name_data = new Bloodhound({
            "datumTokenizer":Bloodhound.tokenizers.whitespace,
            "queryTokenizer":Bloodhound.tokenizers.whitespace,
            "local":{$tbnames}
        });
        kvInitTA('tablesfields-'+index+'-table_name', {"highlight":true,"minLength":0}, [{
            "limit":10,
            "name":"tablesfields_table_name_data",
            "source":tablesfields_table_name_data.ttAdapter()
        }]);*/

        var tablesfields_action_id_data = new Bloodhound({
            "datumTokenizer":Bloodhound.tokenizers.whitespace,
            "queryTokenizer":Bloodhound.tokenizers.whitespace,
            "local":{$actionid}
        });
        kvInitTA('tablesfields-'+index+'-action_id', {"highlight":true,"minLength":0}, [{
            "limit":10,
            "name":"tablesfields_action_id_data",
            "source":tablesfields_action_id_data.ttAdapter()
        }]);
    },
    initForm : function (){
        var indexs = [];
        jQuery(".dynamicform_wrapper a.show-content-form").each(function(index) {
            $(this).attr("content-form","content-form-"+ (index + 1));
        });

        jQuery(".dynamicform_wrapper div.content-form").each(function(index) {
            $(this).addClass("content-form-"+ (index + 1));
        });

        jQuery(".dynamicform_wrapper .title-dynamicform").each(function(index) {
            jQuery(this).html("Fields "  + (index + 1) +' '+ $('#tablesfields-'+index+'-input_label').val());
            indexs.push(parseInt(index));
            Dynamic.initCodeMirror([index]);
            Dynamic.hideContentForm([index]);
            Dynamic.initTypeaHead([index]);
            Dynamic.initCheckboxX([index]);
        });
    },
    initSelect2: function(indexs){
        var index = Math.max.apply(null, indexs);
        if (jQuery('#tablesfields-'+index+'-table_name').data('select2')) { 
            jQuery('#tablesfields-'+index+'-table_name').select2('destroy'); 
        }
        jQuery.when(
            jQuery('#tablesfields-'+index+'-table_name').select2({
                "allowClear":true,
                "theme":"krajee",
                "width":"100%",
                "placeholder":
                "Select field ...",
                "language":"th-TH"
        })).done(
            initS2Loading('tablesfields-'+index+'-table_name',{"themeCss":".select2-container--krajee","sizeCss":"","doReset":true,"doToggle":false,"doOrder":false}));
        jQuery('#tablesfields-'+index+'-table_name').on('change', function() {
            Dynamic.initTypeaHeadOnchange($(this).val(),index); 
        });
    },
    giiCodeTemplate: function(e){
        var id = $(e).attr('table_id');
        $.ajax({
            "type":"GET",
            "url":"/core/tables-fields/gen-code",
            "data":{id:id},
            "async": false,
            "dataType":"text",
            "success":function (result) {
                $('#modal-tables-fields .modal-body').html('<pre>'+result+'</pre>');
                $('#modal-tables-fields').modal('show');
                //$('#code-preview').html(result);
            },
            "error":function (xhr, status, error) {
                swal(error, status, "error");
            }
        });
    },
    giiRawSql:function(e){
        var table_id = $(e).attr('table_id');
        var actionid = $(e).attr('actionid');
        $.ajax({
            "type":"POST",
            "url":"/core/tables-fields/get-rawsql",
            "data":{table_id:table_id,actionid:actionid},
            "async": false,
            "dataType":"json",
            "success":function (result) {
                $('#modal-tables-fields .modal-body').html('<pre></pre>');
                $('#modal-tables-fields .modal-body > pre').text(result.join(''));
                $('#modal-tables-fields').modal('show');
            },
            "error":function (xhr, status, error) {
                swal(error, status, "error");
            }
        });
    },
    initTypeaHeadOnchange : function(tb_name,index){
        var columns;
        $.ajax({
            type: 'POST',
            url: 'get-column-names',
            data: {tb_name:tb_name},
            async: false,
            dataType: "json",
            success: function (result) {
                if(result){
                    columns = $.map(result, function(el) { return el });
                }
            },
            error: function (xhr, status, error) {
                swal(error, status, "error");
            },
        });
        
        var tablesfields = new Bloodhound({
            "datumTokenizer":Bloodhound.tokenizers.whitespace,
            "queryTokenizer":Bloodhound.tokenizers.whitespace,
            "local":columns
        });

        kvInitTA('tablesfields-'+index+'-table_varname', {"highlight":true,"minLength":0}, [{
            "limit":100,
            "name":"tablesfields",
            "source":tablesfields.ttAdapter()
        }]);
    }
};

$('button.show-all').on('click',function(){
    jQuery(".dynamicform_wrapper a.show-content-form").each(function(index) {
        $('div.content-form-'+(index+1)).css('display','block');
        var e = $('a.show-content-form');
        e[index].innerHTML = 'Hide';
    });
});
$('button.hide-all').on('click',function(){
    jQuery(".dynamicform_wrapper a.show-content-form").each(function(index) {
        $('div.content-form-'+(index+1)).css('display','none');
        var e = $('a.show-content-form');
        e[index].innerHTML = 'Show';
    });
});
Dynamic.initForm();
JS
);
?>
<?=  Modal::widget([
    'header' => '<h4 class="modal-title">Code Preview</h4>',
    'id' => 'modal-tables-fields',
    'size' => 'modal-lg',
    'options' => ['tabindex' => false,],
    'footer' => Html::button('Close',['class' => 'btn btn-default' , 'data-dismiss' => 'modal'])
]);
?>
<?php
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // always need it for jquery plugin
    "size" => "modal-lg"
])
?>
<?php Modal::end(); ?>