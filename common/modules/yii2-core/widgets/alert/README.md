yii2-widget-alert
=================
The widget supports all parameters that one would pass for the Yii Bootstrap Alert Widget. The additional parameter settings specially available for this widget are:

type: string the type of the alert widget. Defaults to 'alert-info' or Alert::TYPE_INFO. You can select one of the following options:

Alert::TYPE_INFO: Sets it to 'alert-info' CSS class.

Alert::TYPE_DANGER: Sets it to 'alert-danger' CSS class.

Alert::TYPE_SUCCESS: Sets it to 'alert-success' CSS class.

Alert::TYPE_WARNING: Sets it to 'alert-warning' CSS class.

Alert::TYPE_PRIMARY: Sets it to 'bg-primary' CSS class.

Alert::TYPE_DEFAULT: Sets it to 'well' CSS class.

Alert::TYPE_CUSTOM: Use this for setting no style and adding your own custom style. You can pass in any style/class settings to the options to style your alert.

iconType: string the icon type - can be 'class' (the CSS class) or 'image' (the image url). Defaults to 'class'.

icon: string the class name for the icon to be displayed in the title section. If you want to display an image - you can set an image source here and set the iconType property to image.

iconOptions: array the HTML attributes for the icon.

title: string display a title for the alert (this is not HTML encoded). This will help display a title with a message separator and an optional bootstrap icon. If this is null or not set, it will not be displayed.

titleOptions: array the HTML attributes for the title. Defaults to ['class' => 'kv-alert-title']. The following options are additionally recognized:

tag: string the tag to display the title. Defaults to 'span'.

showSeparator: boolean whether to show the title separator. Only applicable if title is set.

body: string the alert message body.

delay: integer|boolean time in milliseconds after which each alert fades out. If set to 0 or false, alerts will never fade out and will be displayed forever.

options: array the HTML attributes for the alert container.
## Usage

### Alert
```php
use msoft\alert\Alert;

echo Alert::widget([
    'type' => Alert::TYPE_SUCCESS,
    'title' => 'Well done!',
    'icon' => 'glyphicon glyphicon-ok-sign',
    'body' => 'You successfully read this important alert message.',
    'showSeparator' => true,
    'delay' => 2000
]);
echo Alert::widget([
    'type' => Alert::TYPE_INFO,
    'title' => 'Heads up!',
    'icon' => 'glyphicon glyphicon-info-sign',
    'body' => 'This alert needs your attention, but it\'s not super important.',
    'showSeparator' => true,
    'delay' => 4000
]);
echo Alert::widget([
    'type' => Alert::TYPE_WARNING,
    'title' => 'Warning!',
    'icon' => 'glyphicon glyphicon-exclamation-sign',
    'body' => 'Better check yourself, you\'re not looking too good.',
    'showSeparator' => true,
    'delay' => 6000
]);
echo Alert::widget([
    'type' => Alert::TYPE_DANGER,
    'title' => 'Oh snap!',
    'icon' => 'glyphicon glyphicon-remove-sign',
    'body' => 'Change a few things up and try submitting again.',
    'showSeparator' => true,
    'delay' => 8000
]);
```

### AlertBlock
```php
use msoft\alert\AlertBlock;

echo AlertBlock::widget([
	'type' => AlertBlock::TYPE_ALERT,
	'useSessionFlash' => true
]);
```

## License

**yii2-widget-alert** is released under the BSD 3-Clause License. See the bundled `LICENSE.md` for details.