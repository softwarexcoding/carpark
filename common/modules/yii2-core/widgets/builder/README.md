yii2-builder
============

## Form

`\msoft\widgets\Form`

The Form Builder widget allows you to build a form through a configuration array. Key features available:

- Configure your form fields from a model extending `yii\base\model` or `yii\db\ActiveRecord`.
- Ability to support various Bootstrap 3.x form layouts. Uses the advanced [`msoft\widgets\ActiveForm`](http://demos.krajee.com/widget-details/active-form).
- Use Bootstrap column/builder layout styling by just supplying `columns` property.
- Build complex layouts (for example single, double, or multi columns in the same layout) - by reusing the widget for building your attributes.
- Tweak ActiveForm defaults to control field options, styles, templates, and layouts.
- Configure your own hints to display below each active field attribute.
- Various Bootstrap 3.x styling features are available by default. However, one can easily customize and theme it to one's liking using any CSS framework.
- Supports and renders HTML input types (uses [`msoft\widgets\ActiveField`](http://demos.krajee.com/widget-details/active-field)) including input widgets and more:
    - `INPUT_TEXT` or `textInput`
    - `INPUT_TEXTAREA` or `textarea`
    - `INPUT_PASSWORD` or `passwordInput`
    - `INPUT_DROPDOWN_LIST` or `dropdownList`
    - `INPUT_LIST_BOX` or `listBox`
    - `INPUT_CHECKBOX` or `checkbox`
    - `INPUT_RADIO` or `radio`
    - `INPUT_CHECKBOX_LIST` or `checkboxList`
    - `INPUT_CHECKBOX_BUTTON_GROUP` or `checkboxList`
    - `INPUT_RADIO_LIST` or `radioList`
    - `INPUT_MULTISELECT` or `multiselect`
    - `INPUT_FILE` or `fileInput`
    - `INPUT_HTML5` or `input`
    - `INPUT_WIDGET` or `widget`
    - `INPUT_HIDDEN` or `hiddenInput`
    - `INPUT_STATIC` or `staticInput`
    - `INPUT_HIDDEN_STATIC` or `hiddenStaticInput`
    - `INPUT_RAW` or `raw` (any free text or html markup)

Refer the [documentation](http://demos.krajee.com/builder-details/form) for more details.

## FormGrid

`\msoft\widgets\FormGrid`

Create bootstrap grid layouts in a snap. The Form Grid Builder widget offers an easy way to configure your form inputs as a bootstrap grid layout and a single array configuration. It basically uses 
multiple instances of the `\msoft\widgets\Form` widget above to generate this grid. One needs to just setup the rows for the grid,
where each row will be an array configuration as needed by the `Form` widget. However, most of the common settings like `model`, `form`,
`columns` etc. can be defaulted at `FormGrid` widget level.

## Tabular Form 

`msoft\widgets\TabularForm`

The tabular form allows you to update information from multiple models (typically used in master-detail forms). Key features

- Supports all input types as mentioned in the `Form` builder widget
- The widget works like a Yii GridView and uses an ActiveDataProvider to read the models information. 
- Supports features of the builderview like pagination and sorting.
- Allows you to highlight and select table rows
- Allows you to add and configure action buttons for each row.
- Configure your own hints to display below each active field attribute.
- Various Bootstrap 3.x styling features are available by default. However, one can easily customize and theme it to one's liking using any CSS framework.
- Advanced table styling, columns, and layout configuration by using the features available in the [`msoft\widgets\GridView`]([`msoft\widgets\ActiveForm`](http://demos.krajee.com/builder) widget.
- One can easily read and manage the tabular input data using the `loadMultiple` and `validateMultiple` functions in `yii\base\Model`.

> NOTE: The TabularForm widget depends on and uses the [yii2-grid](http://demos.krajee.com/grid) module. Hence, the `gridview` module needs to be setup in your Yii configuration file.

```php
'modules' => [
   'gridview' =>  [
        'class' => '\msoft\widgets\grid\Module'
    ]
];
```

> IMPORTANT: You must follow one of the two options to setup your DataProvider or your columns to ensure primary key for each record is properly identified. 
- **Option 1 (preferred):** Setup your dataProvider query to use `indexBy` method to index your records by primary key. For example:
```php
$query = Model::find()->indexBy('id'); // where `id` is your primary key

$dataProvider = new ActiveDataProvider([
    'query' => $query,
]);
```
- **Option 2 (alternate):** You can setup the primary key attribute as one of your columns with a form input type (and hide if needed) - so that the models are appropriately updated via <code>loadMultiple</code> method (even if you reorder or sort the columns). You must also set this attribute to be `safe` in your model validation rules. This is been depicted in the example below.
```php
'attributes'=>[
    'id'=>[ // primary key attribute
        'type'=>TabularForm::INPUT_HIDDEN, 
        'columnOptions'=>['hidden'=>true]
    ], 
 ]
```

### Demo
You can see detailed [documentation](http://demos.krajee.com/builder) on usage of the extension.

## Usage

### Form
```php
use msoft\widgets\Form;
$form = ActiveForm::begin();
echo Form::widget([
    'model' => $model,
    'form' => $form,
    'columns' => 2,
    'attributes' => [
        'username' => ['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter username...']],
        'password' => ['type'=>Form::INPUT_PASSWORD, 'options'=>['placeholder'=>'Enter password...']],
        'rememberMe' => ['type'=>Form::INPUT_CHECKBOX],
    ]
]);
ActiveForm::end();
```

### FormGrid
```php
use msoft\widgets\Form;
use msoft\widgets\FormGrid;
$form = ActiveForm::begin();
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'autoGenerateColumns' => true,
    'rows' => [
        [
            'attributes' => [
                'username' => ['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter username...']],
                'password' => ['type'=>Form::INPUT_PASSWORD, 'options'=>['placeholder'=>'Enter password...']],
                'rememberMe' => ['type'=>Form::INPUT_CHECKBOX],
            ],
        ],
        [
            'attributes' => [
                'first_name' => ['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter first name...']],
                'last_name' => ['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter last name...']],
            ]
        ]
    ]
]);
ActiveForm::end();
```

### TabularForm
```php
use msoft\widgets\TabularForm;
$form = ActiveForm::begin();
echo TabularForm::widget([
    'form' => $form,
    'dataProvider' => $dataProvider,
    'attributes' => [
        'name' => ['type' => TabularForm::INPUT_TEXT],
        'color' => [
            'type' => TabularForm::INPUT_WIDGET, 
            'widgetClass' => \msoft\widgets\ColorInput::classname()
        ],
        'author_id' => [
            'type' => TabularForm::INPUT_DROPDOWN_LIST, 
            'items'=>ArrayHelper::map(Author::find()->orderBy('name')->asArray()->all(), 'id', 'name')
        ],
        'buy_amount' => [
            'type' => TabularForm::INPUT_TEXT, 
            'options'=>['class'=>'form-control text-right'], 
            'columnOptions'=>['hAlign'=>GridView::ALIGN_RIGHT]
        ],
        'sell_amount' => [
            'type' => TabularForm::INPUT_STATIC, 
            'columnOptions'=>['hAlign'=>GridView::ALIGN_RIGHT]
        ],
    ],
    'gridSettings' => [
        'floatHeader' => true,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> Manage Books</h3>',
            'type' => GridView::TYPE_PRIMARY,
            'after'=> 
                Html::a(
                    '<i class="glyphicon glyphicon-plus"></i> Add New', 
                    $createUrl, 
                    ['class'=>'btn btn-success']
                ) . '&nbsp;' . 
                Html::a(
                    '<i class="glyphicon glyphicon-remove"></i> Delete', 
                    $deleteUrl, 
                    ['class'=>'btn btn-danger']
                ) . '&nbsp;' .
                Html::submitButton(
                    '<i class="glyphicon glyphicon-floppy-disk"></i> Save', 
                    ['class'=>'btn btn-primary']
                )
        ]
    ]     
]); 
ActiveForm::end(); 
```

### Scenario 1 
Simple layout & Inline Form

### Step 1: Setting up model method
```php
public function getFormAttribs() {
    return [
        'username'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter username...']],
        'password'=>['type'=>Form::INPUT_PASSWORD, 'options'=>['placeholder'=>'Enter password...']],
        'rememberMe'=>['type'=>Form::INPUT_CHECKBOX],
        'actions'=>['type'=>Form::INPUT_RAW, 'value'=>Html::submitButton('Submit', ['class'=>'btn btn-primary'])];
    ];
}  
```

### Step 2: Rendering the form (view)
```php
use msoft\widgets\ActiveForm;
use msoft\widgets\Form;
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_INLINE]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'attributes'=>$model->formAttribs
]);
ActiveForm::end();
```

### Scenario 2 
Two columns & Vertical Form
```php
use msoft\widgets\ActiveForm;
use msoft\widgets\Form;
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>$model->formAttribs
]);
echo Html::button('Submit', ['type'=>'button', 'class'=>'btn btn-primary']);
ActiveForm::end();
```

### Scenario 3
Complex layout & input types
```php
use msoft\widgets\ActiveForm;
use msoft\widgets\Form;
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[       // 2 column layout
        'username'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter username...']],
        'password'=>['type'=>Form::INPUT_PASSWORD, 'options'=>['placeholder'=>'Enter password...']]
    ]
]);
echo Form::widget([       // 1 column layout
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        'notes'=>['type'=>Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter notes...']],
    ]
]);
echo Form::widget([     // nesting attributes together (without labels for children)
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'date_range' => [
            'label' => 'Date Range',
            'attributes'=>[
                'begin_date' => [
                    'type'=>Form::INPUT_WIDGET, 
                    'widgetClass'=>'\msoft\widgets\DateControl',
                    'options'=>[
                        'options'=>[
                            'options'=>['placeholder'=>'Date from...']
                        ]
                    ],
                ],
                'end_date'=>[
                    'type'=>Form::INPUT_WIDGET, 
                    'widgetClass'=>'\msoft\widgets\DateControl', 
                    'options'=>[
                        'options'=>[
                            'options'=>['placeholder'=>'Date to...', 'class'=>'col-md-9']
                        ]
                    ]
                ],
            ]
        ],
        'time_range'=>[
            'label' => 'Time Range',
            'attributes'=>[
                'begin_time'=>[
                    'type'=>Form::INPUT_WIDGET, 
                    'widgetClass'=>'\msoft\widgets\TimePicker', 
                    'options'=>['options'=>['placeholder'=>'Time from...']],
                ],
                'end_time'=>[
                    'type'=>Form::INPUT_WIDGET, 
                    'widgetClass'=>'\msoft\widgets\TimePicker', 
                    'options'=>['options'=>['placeholder'=>'Time to...', 'class'=>'col-md-9']]
                ],
            ]
        ],
    ]
]);
echo Form::widget([       // 3 column layout
    'model'=>$model,
    'form'=>$form,
    'columns'=>3,
    'attributes'=>[
        'birthday'=>[
            'type'=>Form::INPUT_WIDGET, 
            'widgetClass'=>'\msoft\widgets\DatePicker', 
            'hint'=>'Enter birthday (mm/dd/yyyy)'
        ],
        'state_1'=>[
            'type'=>Form::INPUT_WIDGET, 
            'widgetClass'=>'\msoft\widgets\Select2', 
            'options'=>['data'=>$model->typeahead_data], 
            'hint'=>'Type and select state'
        ],
        'color'=>[
            'type'=>Form::INPUT_WIDGET, 
            'widgetClass'=>'\msoft\widgets\ColorInput', 
            'hint'=>'Choose your color'
        ],
        'status'=>[
            'type'=>Form::INPUT_RADIO_LIST, 
            'items'=>[true=>'Active', false=>'Inactive'], 
            'options'=>['inline'=>true]
        ],
        'brightness'=>[
            'type'=>Form::INPUT_WIDGET, 
            'label'=>Html::label('Brightness (%)'), 
            'widgetClass'=>'\msoft\widgets\RangeInput', 
            'options'=>['width'=>'80%']
        ],
        'actions'=>[
            'type'=>Form::INPUT_RAW, 
            'value'=>'<div style="text-align: right; margin-top: 20px">' . 
                Html::resetButton('Reset', ['class'=>'btn btn-default']) . ' ' .
                Html::button('Submit', ['type'=>'button', 'class'=>'btn btn-primary']) . 
                '</div>'
        ],
    ]
]);
ActiveForm::end();
```

### Scenario 4
Horizontal form with custom labels

```php
use msoft\widgets\ActiveForm;
use msoft\widgets\Form;
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL, 'formConfig'=>['labelSpan'=>4]]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        'address_detail' => [   // complex nesting of attributes along with labelSpan and colspan
            'label'=>'Address',
            'labelSpan'=>2,
            'columns'=>6,
            'attributes'=>[
                'address'=>[
                    'type'=>Form::INPUT_TEXT, 
                    'options'=>['placeholder'=>'Enter address...'],
                    'columnOptions'=>['colspan'=>3],
                ],
                'phone'=>[
                    'type'=>Form::INPUT_TEXT, 
                    'options'=>['placeholder'=>'Phone...'],
                    'columnOptions'=>['colspan'=>2],
                ],
                'zip_code'=>[
                    'type'=>Form::INPUT_TEXT, 
                    'options'=>['placeholder'=>'Zip...']
                ],
            ]
        ]
    ]
]);
echo Form::widget([ // fields with labels
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'state_1'=>['label'=>'Province 1', 'options'=>['placeholder'=>'Province 1...']],
        'state_1a'=>['label'=>'Province 2', 'options'=>['placeholder'=>'Province 2...']],
    ]
]);
echo Form::widget([ // continuation fields to row above without labels
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'state_2'=>['label'=>'', 'options'=>['placeholder'=>'Province 1 Description...']],
        'state_3'=>['label'=>'', 'options'=>['placeholder'=>'Province 2 Description...']],
    ]
]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'status'=>['label'=>'Severity', 'items'=>[0=>'Low', 1=>'Medium', 2=>'High'], 'type'=>Form::INPUT_RADIO_BUTTON_GROUP],
        'status_1'=>['label'=>'Categories', 'items'=>[0=>'Group 1', 1=>'Group 2', 2=>'Group 3'], 'type'=>Form::INPUT_CHECKBOX_BUTTON_GROUP]
    ]
]);
echo Form::widget([ // hide label and extend input to full width
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        'website'=>[
            'label'=>false, 
            'options'=>[
                'placeholder'=>'Enter website (this input fills the width when label is set to false)...'
            ]
        ],
    ]
]);
echo '<div class="text-right">' . Html::resetButton('Reset', ['class'=>'btn btn-default']) . '</div>';
ActiveForm::end();
```

### Scenario 5
Static only form
```php
use msoft\widgets\ActiveForm;
use msoft\widgets\Form;
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL, 'formConfig'=>['labelSpan'=>4]]);
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        'address_detail' => [   // complex nesting of attributes along with labelSpan and colspan
            'label'=>'Address',
            'labelSpan'=>2,
            'columns'=>6,
            'attributes'=>[
                'address'=>[
                    'type'=>Form::INPUT_HIDDEN_STATIC, 
                    'options'=>['placeholder'=>'Enter address...'],
                    'columnOptions'=>['colspan'=>3],
                ],
                'phone'=>[
                    'type'=>Form::INPUT_TEXT, 
                    'options'=>['placeholder'=>'Phone...'],
                    'columnOptions'=>['colspan'=>2],
                ],
                'zip_code'=>[
                    'type'=>Form::INPUT_TEXT, 
                    'options'=>['placeholder'=>'Zip...']
                ],
            ]
        ]
    ]
]);
echo Form::widget([ // fields with labels
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'state_1'=>['label'=>'Province 1', 'options'=>['placeholder'=>'Province 1...']],
        'state_1a'=>['label'=>'Province 2', 'options'=>['placeholder'=>'Province 2...']],
    ]
]);
echo Form::widget([ // continuation fields to row above without labels
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'state_2'=>['label'=>'', 'options'=>['placeholder'=>'Province 1 Description...']],
        'state_3'=>['label'=>'', 'options'=>['placeholder'=>'Province 2 Description...']],
    ]
]);
echo Form::widget([ // hide label and extend input to full width
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        'website'=>['label'=>false, 'options'=>['placeholder'=>'Enter website (this input fills the width when label is set to false)...']],
    ]
]);
echo '<div class="text-right">' . Html::resetButton('Reset', ['class'=>'btn btn-default']) . '</div>';
ActiveForm::end();
```

### Scenario 6
Usage without model

```php
use yii\helpers\Html;
use msoft\widgets\Form;
echo Html::beginForm('', '', ['class'=>'form-horizontal']);
echo Form::widget([
    // formName is mandatory for non active forms
    // you can get all attributes in your controller 
    // using $_POST['kvform']
    'formName'=>'kvform',
    
    // default grid columns
    'columns'=>2,
    
    // set global attribute defaults
    'attributeDefaults'=>[
        'type'=>Form::INPUT_TEXT,
        'labelOptions'=>['class'=>'col-md-3'], 
        'inputContainer'=>['class'=>'col-md-9'], 
        'container'=>['class'=>'form-group'],
    ],
    
    'attributes'=>[
        'fld1'=>['label'=>'Name', 'value'=>'msoft'],
        'fld2'=>['label'=>'Email', 'value'=>'info@solutions.com'],
        'fld3'=>['label'=>'From Date', 'type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\msoft\widgets\DatePicker'],
        'fld4'=>['label'=>'To Date', 'type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\msoft\widgets\DatePicker'],
        'fld5'=>['label'=>'Details', 'type'=>Form::INPUT_TEXTAREA],
        'fld6'=>['label'=>'Remarks', 'type'=>Form::INPUT_TEXTAREA],
        'fld7'=>[
            'label'=>'Severity', 
            'items'=>[0=>'Low', 1=>'Medium', 2=>'High'], 
            'type'=>Form::INPUT_RADIO_BUTTON_GROUP
        ],
        'fld8'=>[
            'label'=>'Categories', 
            'items'=>[0=>'Group 1', 1=>'Group 2', 2=>'Group 3'], 
            'type'=>Form::INPUT_CHECKBOX_BUTTON_GROUP
        ],
    ]
]);
echo Html::endForm();
```

### Scenario 7
Static Only Without Model

```php
use yii\helpers\Html;
use msoft\widgets\Form;
echo Html::beginForm('', '', ['class'=>'form-horizontal']);
echo Form::widget([
    // formName is mandatory for non active forms
    // you can get all attributes in your controller 
    // using $_POST['kvform']
    'formName'=>'kvform',
    
    // set to static only
    'staticOnly'=>true,
    
    // default grid columns
    'columns'=>2,
    
    // set global attribute defaults
    'attributeDefaults'=>[
        'type'=>Form::INPUT_TEXT,
        'labelOptions'=>['class'=>'col-md-3'], 
        'inputContainer'=>['class'=>'col-md-9'], 
        'container'=>['class'=>'form-group'],
    ],
    
    'attributes'=>[
        'fld1'=>['label'=>'Name', 'value'=>'msoft'],
        'fld2'=>['label'=>'Email', 'value'=>'info@solutions.com'],
        'fld3'=>['label'=>'From Date', 'staticValue'=>'<em>(not set)</em>', 'type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\msoft\widgets\DatePicker'],
        'fld4'=>['label'=>'To Date', 'staticValue'=>'<em>(not set)</em>', 'type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\msoft\widgets\DatePicker'],
        'fld5'=>['label'=>'Details', 'staticValue'=>'<em>(not set)</em>', 'type'=>Form::INPUT_TEXTAREA],
        'fld6'=>['label'=>'Remarks', 'staticValue'=>'<em>(not set)</em>', 'type'=>Form::INPUT_TEXTAREA],
    ]
]);
echo Html::endForm();
```
## License

**yii2-builder** is released under the BSD 3-Clause License. See the bundled `LICENSE.md` for details.