<?php

namespace msoft\widgets\checkbox;

use Yii;
use msoft\widgets\base\AssetBundle;


class CheckboxXAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets/bootstrap-checkbox-x');
        $this->setupAssets('css', ['css/checkbox-x']);
        $this->setupAssets('js', ['js/checkbox-x']);
        parent::init();
    }
}
