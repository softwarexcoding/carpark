<?php

namespace msoft\widgets\ckeditor;

use yii\helpers\ArrayHelper;

trait CKEditorTrait
{
 
    public $preset = 'standard';

    public $clientOptions = [];
    
    protected function initOptions()
    {
        $options = [];
        switch ($this->preset) {
            case 'custom':
                $preset = null;
                break;
            case 'basic':
            case 'full':
            case 'standard':
                $preset = 'presets/' . $this->preset . '.php';
                break;
            default:
                $preset = 'presets/standard.php';
        }
        if ($preset !== null) {
            $options = require($preset);
        }
        $this->clientOptions = ArrayHelper::merge($options, $this->clientOptions);
    }
}