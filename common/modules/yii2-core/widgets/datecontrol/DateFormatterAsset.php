<?php

/**
 * @package   yii2-datecontrol
 * @author    msoft Visweswaran <msoftv2@gmail.com>
 * @copyright Copyright &copy; msoft Visweswaran, Krajee.com, 2014 - 2017
 * @version   1.9.6
 */

namespace msoft\widgets\datecontrol;

use Yii;
use msoft\widgets\base\AssetBundle;

/**
 * Asset bundle for the [Krajee PHP Date Formatter](http://plugins.krajee.com/php-date-formatter) extension.
 *
 * @author msoft Visweswaran <msoftv2@gmail.com>
 * @since 1.0
 */
class DateFormatterAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__. '/assets/php-date-formatter');
        $this->setupAssets('js', ['js/php-date-formatter']);
        parent::init();
    }
}