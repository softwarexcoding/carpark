<?php

/**
 * @copyright Copyright &copy; msoft\widgets Visweswaran, Krajee.com, 2014 - 2016
 * @package yii2-widgets
 * @subpackage yii2-widget-depdrop
 * @version 1.0.4
 */

namespace msoft\widgets\depdrop;

use msoft\widgets\base\AssetBundle;

/**
 * Asset bundle for Dependent Dropdown Extension for Yii
 *
 * @author msoft\widgets Visweswaran <msoft\widgetsv2@gmail.com>
 * @since 1.0
 */
class DepDropExtAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/depdrop']);
        parent::init();
    }
}
