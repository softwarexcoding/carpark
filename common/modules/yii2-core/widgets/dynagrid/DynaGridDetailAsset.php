<?php

namespace msoft\widgets\dynagrid;

use msoft\widgets\base\AssetBundle;

class DynaGridDetailAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/kv-dynagrid-detail']);
        parent::init();
    }
}