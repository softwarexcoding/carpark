<?php

namespace msoft\widgets\fullcalendar;

/**
 * Class ThemeAsset
 * @package msoft\widgets\fullcalendar
 */
class ThemeAsset extends \yii\web\AssetBundle
{
	/** @var array The dependencies for the ThemeAsset bundle */
	public $depends = [
		'yii\jui\JuiAsset',
	];
}