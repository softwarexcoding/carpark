<?php

namespace msoft\widgets\markdown;

use yii\web\AssetBundle;

/**
 * Class MarkdownEditorAsset
 *
 * @package msoft\widgets\markdown
 */
class MarkdownEditorAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = __DIR__ . '/assets';

    /**
     * @var array
     */
    public $css = [
        'dist/simplemde.min.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'dist/simplemde.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
