<?php

namespace msoft\widgets\multipleinput\assets;

use yii\web\AssetBundle;

/**
 * Class MultipleInputAsset
 * @package msoft\multipleinput\assets
 */
class MultipleInputAsset extends AssetBundle
{
    public $depends = [
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->sourcePath = __DIR__ . '/src/';

        $this->js = [
            YII_DEBUG ? 'js/jquery.multipleInput.js' : 'js/jquery.multipleInput.min.js',
            'js/auto-size.js'
        ];

        $this->css = [
            YII_DEBUG ? 'css/multiple-input.css' : 'css/multiple-input.min.css'
        ];

        parent::init();
    }


} 