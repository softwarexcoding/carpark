<?php

namespace msoft\widgets\pdfjs;

use yii\web\AssetBundle;
use yii\web\View;

class PdfJsAsset extends AssetBundle
{
    public $sourcePath = __DIR__ .'/assets';

    public $js = [
        'web/compatibility.js',
        'web/l10n.js',
        'build/pdf.js',
        'web/debugger.js',
        'web/viewer.js'
    ];

    public $jsOptions  = [
      'position'=> View::POS_HEAD
    ];

    public $css = [
        'web/viewer.css'
    ];
}
 ?>
