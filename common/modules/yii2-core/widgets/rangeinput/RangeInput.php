<?php


namespace msoft\widgets\rangeinput;

use Yii;
use yii\helpers\Html;

class RangeInput extends \msoft\widgets\base\Html5Input
{
    public $type = 'range';
    public $orientation;
    
    /**
     * @inherit doc
     */
    public function run() {
        if ($this->orientation == 'vertical') {
            Html::addCssClass($this->containerOptions, 'kv-range-vertical');
            $this->html5Options['orient'] = 'vertical';
        }
        parent::run();
    }
}
