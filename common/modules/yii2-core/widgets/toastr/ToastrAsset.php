<?php
namespace msoft\widgets\toastr;

use yii\web\AssetBundle;

/**
 * Class ToastrAsset
 * @package msoft\widgets\toastr
 */
class ToastrAsset extends AssetBundle
{
    /** @var string $sourcePath  */
    public $sourcePath = __DIR__.'/assets/toastr';

    /** @var array $css */
    public $css = [
        'toastr.min.css'
    ];

    /** @var array $js */
    public $js = [
        'toastr.min.js'
    ];

    /** @var array $depends */
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
