<?php

namespace msoft\menu\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SortMenuAsset extends AssetBundle {

    public $sourcePath = '@msoft/menu/assets/';
    public $css = [
        'css/static_custom.css',
        'toastr/build/toastr.min.css'
    ];
    public $js = [
        'js/jquery.nestable.js',
        //'js/config.js',
        'toastr/build/toastr.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}