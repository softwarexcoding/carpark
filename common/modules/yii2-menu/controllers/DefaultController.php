<?php

namespace msoft\menu\controllers;

use Yii;
use msoft\menu\models\Menu;
use msoft\menu\models\MenuAuth;
use msoft\menu\models\MenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * DefaultController implements the CRUD actions for Menu model.
 */
class DefaultController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSortMenus(){
        $rows = Menu::find()
                ->where(['menu_category.title' => Yii::$app->id])
                ->innerJoin('menu_category','menu.menu_category_id = menu_category.id')
                ->orderBy(['menu.sort' => SORT_ASC])
                ->all();
        return $this->render('sort-menus', [
            'rows' => $rows,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
     public function actionView($id)
     {   
         $request = Yii::$app->request;
         if($request->isAjax){
             Yii::$app->response->format = Response::FORMAT_JSON;
             return [
                     'title'=> "View #".$id,
                     'content'=>$this->renderAjax('view', [
                         'model' => $this->findModel($id),
                     ]),
                     'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                             Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                 ];    
         }else{
             return $this->render('view', [
                 'model' => $this->findModel($id),
             ]);
         }
     }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $request = Yii::$app->request;
        //$session = Yii::$app->session;
        $cache = Yii::$app->cache;
        $model = new Menu();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "บันทึกเมนู",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                if($cache->get(Yii::$app->id)){
                    $cache->delete(Yii::$app->id);
                }
                /*if ($session->has('menus')){
                    $session->remove('menus');
                }*/
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "บันทึกเมนู",
                    'content'=>'<span class="text-success">Create success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];  
            }else{           
                return [
                    'title'=> "บันทึกเมนู",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        //$session = Yii::$app->session;
        $cache = Yii::$app->cache;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                $model->params = $model->params ? Json::encode($model->params) : null;
                return [
                    'title'=> "Update #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                if($cache->get(Yii::$app->id)){
                    $cache->delete(Yii::$app->id);
                }
                /*if ($session->has('menus')){
                    $session->remove('menus');
                }*/
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Update #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                $model->params = $model->params ? Json::encode($model->params) : null;
                 return [
                    'title'=> "Update #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $model->params = $model->params ? Json::encode($model->params) : null;
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        \Yii::$app->getSession()->setFlash('success', 'Deleted!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSavemenuOnchange(){
        $request = Yii::$app->request;
        //$session = Yii::$app->session;
        $cache = Yii::$app->cache;
        if($request->isPost){
            $items = $request->post('items');
            $this->saveMenuItems($items);
            /*if ($session->has('menus')){
                $session->remove('menus');
            }*/
            if($cache->get(Yii::$app->id)){
                $cache->delete(Yii::$app->id);
            }
            return true;
        }
    }

    protected function saveMenuItems($items,$parent_id = null, $sort = 0){
        foreach($items as $key => $item){
            $sort++;
            $model = $this->findModel($item['id']);
            $model->sort = $sort;
            $model->parent_id = $parent_id;
            $model->save(false);
            if (array_key_exists("children", $item)) {
                $this->saveMenuItems($item["children"],$model['id'],$sort);
            }
        }
    }

    public function actionClearcacheMenu(){
        $request = Yii::$app->request;
        //$session = Yii::$app->session;
        $cache = Yii::$app->cache;
        if($request->isPost){
            if($cache->get(Yii::$app->id)){
                $cache->delete(Yii::$app->id);
            }
            /*if ($session->has('menus')){
                $session->remove('menus');
            }*/
            return 'Success!';
        }
    }

}
