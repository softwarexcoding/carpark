<?php

namespace msoft\menu\models;

//use yii\base\Model;
use Yii;
use yii\helpers\Html;
use msoft\menu\models\Menu;
use yii\helpers\ArrayHelper;
use msoft\utils\CoreUtility;
use yii\helpers\Json;
/**
 * Description of navigate
 *
 * @author madone
 */
class Navigate extends \yii\base\Model {

    //put your code here
    public function menu($category) {
        //$session = Yii::$app->session;
        $cache = Yii::$app->cache;
        $menus = $cache->get($category);
        if ($menus !== false) {
            return Json::decode($menus);
        }else{
            $array = Menu::find()
            ->where([
                'menu_category.title' => $category,
                'menu.status' => '1',
                //'menu.parent_id' => null
            ])
            ->innerJoin('menu_category','menu.menu_category_id = menu_category.id')
            ->orderBy(['sort' => SORT_ASC])
            ->all();
            $menus = $this->genarateItems($array);
            $cache->set($category, Json::encode($menus),0);
            return Json::decode($cache->get($category));
        }
        /*
        if ($session->has('menus')){
            return $session->get('menus');
        }else{
            $array = Menu::find()
            ->where([
                'menu_category.title' => $category,
                'menu.status' => '1',
                //'menu.parent_id' => null
            ])
            ->innerJoin('menu_category','menu.menu_category_id = menu_category.id')
            ->orderBy(['sort' => SORT_ASC])
            ->all();
            $menus = $this->genarateItems($array);
            $session->set('menus', $menus);
            return $menus;
        }*/
        
    }

    public function parentMenu($id) {
        $model = Menu::find()
                ->where([
                    'parent_id' => $id,
                    'status' => '1'
                ])
                ->orderBy(['sort' => SORT_ASC])
                ->all();
        return $model ? $this->genArray($model) : null;
    }

    protected function genarateItems($items) {
        $menu = [];
        foreach ($items as $val) {
            $parents = ($val->id && $val->parent_id == null) ? $this->genarateParentMenu($items,$val->id) : null;

            $visible = false;
            if(is_array($val->auth_items)){
                foreach($val->auth_items as $item){
                    if($visible = Yii::$app->user->can($item)){
                        break;
                    }
                }
            }
            
            $this->getCount($val->router);
            $options = CoreUtility::strArray2String($val->options);
            $route = CoreUtility::strArray2String($val->route);

            if(is_array($parents)){
                $menu[] = [
                    'label' => $val->title . $this->count,
                    'encode' => false,
                    'icon' => $val->icon,
                    'url' => [$val->router],
                    'visible' => $visible,
                    'items' => $parents,
                    'options' => empty($val->options) ? ['class' => 'nav-item'] : CoreUtility::string2Array($options),
                    'route' => CoreUtility::string2Array($route),
                    'module_id' => $val->module_id
                ];
            }else if($val->parent_id == null){
                $menu[] = [
                    'label' => $val->title . $this->count,
                    'encode' => false,
                    'icon' => $val->icon,
                    'url' => [$val->router],
                    'visible' => $visible,
                    'options' => empty($val->options) ? ['class' => 'nav-item'] : CoreUtility::string2Array($options),
                    'route' => CoreUtility::string2Array($route),
                    'module_id' => $val->module_id
                ];
            }
        }
        return $menu;
    }

    public function genarateParentMenu($items,$id) {
        $result = ArrayHelper::index($items, null, 'parent_id');
        if(isset($result[$id])){
            $items = [];
            foreach($result[$id] as $key => $val){
                $visible = false;
                if(is_array($val->auth_items)){
                    foreach($val->auth_items as $item){
                        if($visible = Yii::$app->user->can($item)){
                            break;
                        }
                    }
                }
                $options = CoreUtility::strArray2String($val->options);
                $route = CoreUtility::strArray2String($val->route);
                $items[] = [
                    'label' => $val->title . $this->count,//v2.0 ใช้ $val->name
                    'encode' => false,
                    'icon' => $val->icon,
                    'url' => [$val->router],//v2.0 ใช้ $val->route
                    'visible' => $visible,
                    'options' => empty($val->options) ? ['class' => 'nav-item'] : CoreUtility::string2Array($options),
                    'route' => CoreUtility::string2Array($route),
                    'module_id' => $val->module_id
                ];
            }
            return $items;
        }else{
            return null;
        }
    }

    protected $count;
    protected function getCount($router){
        //return $count;
    }

}
