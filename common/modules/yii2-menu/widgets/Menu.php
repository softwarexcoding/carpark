<?php 
namespace msoft\menu\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use msoft\admin\models\Route;
use yii\widgets\Menu AS BaseMenu;

class Menu extends BaseMenu {

    public $submenuTemplate = "\n<ul class='sub-menu'>\n{items}\n</ul>\n";

    public $encodeLabels = false;

    public $labelTemplate = '{label}';

    public $linkTemplate = '<a href="{url}" class="nav-link"><i class="{icon}"></i> <span class="title">{label}</span></a>';

    public $linkTemplateActive = '<a href="{url}" class="nav-link"><i class="arrow open"></i> <span class="title">{label}</span></a>';

    public $icon = '';

    public $activeCssClass = 'active';

    public $firstItemCssClass;

    public $action = '';

    protected function renderItems($items)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];
            //$moduleRoutes = $this->getAppRoutes($item['module_id']);

            if ($item['active']) {
                $class[] = $this->activeCssClass;
            }else{
                if(isset($item['items']) && in_array($this->action, $item['parentUrl']) || in_array(Yii::$app->request->url,$item['parentUrl'])){
                    $class[] = $this->activeCssClass;
                }elseif( isset($item['route']) && in_array($this->action,$item['route']) ){
                    $class[] = $this->activeCssClass;
                }elseif(isset($item['route']) && in_array(Yii::$app->request->url,$item['route'])){
                    $class[] = $this->activeCssClass;
                }
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            Html::addCssClass($options, $class);

            $menu = $this->renderItem($item);
            if (!empty($item['items'])) {
                $submenuTemplate = ArrayHelper::getValue($item, 'submenuTemplate', $this->submenuTemplate);
                $menu .= strtr($submenuTemplate, [
                    '{items}' => $this->renderItems($item['items']),
                ]);
            }
            //print_r(ArrayHelper::getValue($item, 'items'));
            $lines[] = Html::tag($tag, $menu, $options);
        }

        return implode("\n", $lines);
    }

    protected function renderItem($item)
    {
        if (isset($item['url'])) {
            if (isset($item['items'])) {
                $template = $this->templateAlias($item,true);
            }elseif(isset($item['options']['class']) && $item['options']['class'] == 'heading'){
                $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);
            }else{
                $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
            }
            
            return strtr($template, [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{label}' => $item['label'],
                '{icon}' => $item['icon'],
            ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => $item['label'],
                '{icon}' => $item['icon'],
            ]);
        }
    }

    protected function normalizeItems($items, &$active)
    {
        foreach ($items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                unset($items[$i]);
                continue;
            }
            if (!isset($item['label'])) {
                $item['label'] = '';
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $items[$i]['label'] = $encodeLabel ? Html::encode($item['label']) : $item['label'];
            $items[$i]['icon'] = isset($item['icon']) ? $item['icon'] : '';
            $hasActiveChild = false;
            if (isset($item['items'])) {
                $items[$i]['items'] = $this->normalizeItems($item['items'], $hasActiveChild);
                if (empty($items[$i]['items']) && $this->hideEmptyItems) {
                    unset($items[$i]['items']);
                    if (!isset($item['url'])) {
                        unset($items[$i]);
                        continue;
                    }
                }
            }
            if (!isset($item['active'])) {
                if ($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item)) {
                    $active = $items[$i]['active'] = true;
                } else {
                    $items[$i]['active'] = false;
                }
            } elseif ($item['active'] instanceof Closure) {
                $active = $items[$i]['active'] = call_user_func($item['active'], $item, $hasActiveChild, $this->isItemActive($item), $this);
            } elseif ($item['active']) {
                $active = true;
            }
        }

        return array_values($items);
    }

    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $route = Yii::getAlias($item['url'][0]);
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }
            if (ltrim($route, '/') !== $this->route) {
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1) {
                $params = $item['url'];
                unset($params[0]);
                foreach ($params as $name => $value) {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

    protected function templateAlias($item, $parent = false) {
        if ($parent == false) {
            return '<a href="{url}" class="nav-link nav-toggle"><i class="{icon}"></i> <span class="title">{label}</span></a>';
        } elseif (in_array($this->action, $item['parentUrl']) || in_array($this->action, $item['route']) || in_array(Yii::$app->request->url,$item['route']) || in_array(Yii::$app->request->url,$item['parentUrl'])) {
            return '<a href="{url}" class="nav-link nav-toggle"><i class="{icon}"></i> <span class="title">{label}</span><span class="selected"></span><span class="arrow open"></span></a>';
        } else {
            return '<a href="{url}" class="nav-link nav-toggle"><i class="{icon}"></i> <span class="title">{label}</span><span class="arrow"></span></a>';
        }
    }

    protected function getRoutes($menuRoute){
        $routes = [];
        $route = '/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id . '/';
        if(is_array($menuRoute)){
            foreach($menuRoute as $action){
                $routes = ArrayHelper::merge($routes, [$route.$action]);
            }
        }
        return $routes;
    }

    protected function getAppRoutes($moduleId){
        $route = new Route();
        $routes = ($moduleId != '' && $moduleId != null) ? $route->getAppRoutes($moduleId) : [];
        return $routes;
    }
}