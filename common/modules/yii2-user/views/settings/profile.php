<?php

/*
 * This file is part of the msoft project.
 *
 * (c) msoft project <http://github.com/msoft>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use msoft\user\helpers\Timezone;
use msoft\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var msoft\user\models\Profile $model
 */

$this->title = Yii::t('user', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'public_email') ?>
                    <?= $form->field($model, 'website') ?>

                    <?= $form->field($model, 'location') ?>

                    <?= $form
                        ->field($model, 'timezone')
                        ->dropDownList(
                            ArrayHelper::map(
                                Timezone::getAll(),
                                'timezone',
                                'name'
                            )
                    ); ?>
                    <?= $form->field($model,'section_id')->widget(\msoft\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payment\models\TbDiscSection::find()->all(), 'disc_sec_id', 'disc_sec_name'),
                        'pluginOptions' => [
                            'placeholder' => 'เลือกแผนก...',
                            'allowClear' => true,
                        //'disabled' => true
                        ],
                    ]) ?>

                    <?= $form
                        ->field($model, 'gravatar_email')
                        ->hint(Html::a(Yii::t('user', 'Change your avatar at Gravatar.com'), 'http://gravatar.com')) ?>

                    <?= $form->field($model, 'bio')->textarea() ?>

                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                            <br>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
