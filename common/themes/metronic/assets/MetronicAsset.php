<?php
namespace metronic\assets;

use Yii;
use yii\web\AssetBundle;
use yii\base\Exception;

class MetronicAsset extends AssetBundle
{
    const STYLING_BLUE = 'blue';
    const STYLING_DARKBLUE = 'darkblue';
    const STYLING_DEFAULT = 'default';
    const STYLING_GREY = 'grey';
    const STYLING_LIGHT = 'light';
    const STYLING_LIGHT2 = 'light2';
    #Layout Themes
    const LAYOUT_DEFAULT = 'default';
    const LAYOUT_ROUNDED = 'rounded';
    const LAYOUT_MATERIAL = 'material';

    public $styling = self::STYLING_DEFAULT;
    public $layout = self::LAYOUT_MATERIAL;
    public $sourcePath = '@metronic/assets';
    
    public $css = [
        'global/plugins/font-awesome/css/font-awesome.min.css?v=1.0',
        'global/plugins/simple-line-icons/simple-line-icons.min.css?v=1.0',
    ];

    public $js = [
        'global/plugins/js.cookie.min.js?v=1.0',
        'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js?v=1.0',
        'global/plugins/jquery.blockui.min.js?v=1.0',
        'global/scripts/app.min.js?v=1.0',
        'layouts/layout/scripts/layout.min.js?v=1.0',
        'layouts/global/scripts/quick-sidebar.min.js?v=1.0',
        'layouts/global/scripts/quick-nav.min.js?v=1.0',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\AppAsset'
    ];

    public function init()
    {
        switch ($this->layout) {
            case self::LAYOUT_ROUNDED:
                $this->css[] = 'global/css/components-rounded.min.css?v=1.0';
                $this->css[] = 'global/css/plugins.min.css?v=1.0';
                $this->css[] = 'layouts/layout/css/layout.min.css?v=1.0';
                $this->css[] = 'layouts/layout/css/custom.min.css?v=1.0';
                break;
            case self::LAYOUT_MATERIAL:
                $this->css[] = 'global/css/components-md.min.css?v=1.0';
                $this->css[] = 'global/css/plugins-md.min.css?v=1.0';
                $this->css[] = 'layouts/layout/css/layout.min.css?v=1.0';
                $this->css[] = 'layouts/layout/css/custom.min.css?v=1.0';
                break;
            case self::LAYOUT_DEFAULT:
                $this->css[] = 'global/css/components.min.css?v=1.0';
                $this->css[] = 'global/css/plugins.min.css?v=1.0';
                $this->css[] = 'layouts/layout/css/layout.min.css?v=1.0';
                $this->css[] = 'layouts/layout/css/custom.min.css?v=1.0';
                break;
            default;
        }
        switch ($this->styling) {
            case self::STYLING_BLUE:
                $this->css[] = 'layouts/layout/css/themes/blue.min.css?v=1.0';
                break;
            case self::STYLING_DARKBLUE:
                $this->css[] = 'layouts/layout/css/themes/darkblue.min.css?v=1.0';
                break;
            case self::STYLING_GREY:
                $this->css[] = 'layouts/layout/css/themes/grey.min.css?v=1.0';
                break;
            case self::STYLING_LIGHT:
                $this->css[] = 'layouts/layout/css/themes/light.min.css?v=1.0';
                break;
            case self::STYLING_LIGHT2:
                $this->css[] = 'layouts/layout/css/themes/light2.min.css?v=1.0';
                break;
            case self::STYLING_DEFAULT:
                $this->css[] = 'layouts/layout/css/themes/default.css?v=1.0';
                break;
            default;
            //$this->css[] = sprintf('layouts/layout/css/themes/%s.min.css', $this->skin);
        }
        parent::init();
    }
}