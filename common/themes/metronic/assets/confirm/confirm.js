
yii.confirm = function (message,ok,cancel) {
    var e = this;
    swal({
        title: message,
        text: "",
        icon: "warning",
        buttons: true,
    })
      .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: e.getAttribute('data-method'),
                url: e.getAttribute('href'),
                success: function(response){
                    AppNotify.Show('success','fa fa-trash','Completed!','ลบข้อมูลสำเร็จ!');
                },
                error: function(jqXHR,textStatus,errorThrown){
                    console.log(errorThrown);
                },
                complete: function(jqXHR,textStatus){
                    if (e.getAttribute('pjax-reload') !== undefined && e.getAttribute('pjax-reload')) {
                        $.pjax.reload({container: e.getAttribute('pjax-reload')});
                    }
                }
            });
        } else {
            
        }
    }).catch(err => {
        if (err) {
            swal("Error!", err, "error");
        } else {
            swal.close();
        }
    });
    return false;
};