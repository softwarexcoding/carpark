<?php 
namespace metronic\components;

use Yii;
use yii\base\Component;
use DateTime;

class DateConvert extends Component {
    public static function convertToPhysical($date){
        $result = '';
        if(!empty($date) && $date != '0000-00-00'){
            $arr = explode("/", $date);
            $y = $arr[2] - 543;
            $m = $arr[1];
            $d = $arr[0];
            $result = "$y-$m-$d";
        }
        return $result;
    }
   public static function convertDate($date=null) {
        //แปลงวันที่ลง mysql
        if(!empty($date)){
            $arr = explode("/", $date);
            if($arr[2]>'2000'&&$arr[2]<'2500'){
                $y = $arr[2];
                $m = sprintf("%02d", $arr[1]);
                $d = sprintf("%02d", $arr[0]);
            }else if($arr[2]>'2500'){
                $y = ($arr[2] - 543);
                $y = $arr[2];
                $m = sprintf("%02d", $arr[1]);
                $d = sprintf("%02d", $arr[0]);
            }
            return "$y-$m-$d";
        }else{
            return "0000-00-00";
        }
    }

    public static function convertToPhysicalDateTime($date){
        $result = '';
        if(!empty($date) && $date != '0000-00-00'){
            $arr = explode("/", $date);
            $y = $arr[2] - 543;
            $m = $arr[1];
            $d = $arr[0];
            $result = "$y-$m-$d".' '.date('H:i:s');
        }
        return $result;
    }

    public static function convertToLogical($date){
        $result = '';
        if(!empty($date) && $date != '0000-00-00'){
            $arr = explode("-", $date);
            $y = $arr[0] + 543;
            $m = $arr[1];
            $d = $arr[2];
            $result = "$d/$m/$y";
            //$result = Yii::$app->formatter->asDate($date, 'dd/MM/yyyy');
        }
        return $result;
    }
    public static function convertToLogical2($date){
        $result = '';
        if(!empty($date) && $date != '0000-00-00'){
            $arr = explode("-", $date);
            $y = $arr[0];
            $m = $arr[1];
            $d = $arr[2];
            $result = "$d/$m/$y";
            //$result = Yii::$app->formatter->asDate($date, 'dd/MM/yyyy');
        }
        return $result;
    }

    public static function convertToLogicalDateOnly($date){
        $result = '';
        if(!empty($date) && $date != '0000-00-00'){
            $newdate = Yii::$app->formatter->asDate($date, 'yyyy-MM-dd');
            $arr = explode("-", $newdate);
            $y = $arr[0] + 543;
            $m = $arr[1];
            $d = $arr[2];
            $result = "$d/$m/$y";
        }
        return $result;
    }

    public static function mysql2phpDateTime($phpDate){
        if(!empty($phpDate) && $phpDate != '0000-00-00 00:00:00'){
            $arrType = explode(' ', $phpDate);
            $arr = explode('-', $arrType[0]);
            $time = (isset($arrType[1]))?' '.$arrType[1]:'';
            $date = new DateTime($arr[2].'-'.$arr[1].'-'.($arr[0] + 543).$time);
            return $date->format('d/m/Y H:i:s');
        }else{
            return '';
        }
    }
    public static function mysql2phpTime($phpDate){
        if(!empty($phpDate) && $phpDate != '0000-00-00 00:00:00'){
            $arrType = explode(' ', $phpDate);
            $arr = explode('-', $arrType[0]);
            $time = (isset($arrType[1]))?' '.$arrType[1]:'';
            $date = new DateTime($arr[2].'-'.$arr[1].'-'.($arr[0] + 543).$time);
            return $date->format('H:i:s');
        }else{
            return '';
        }
    }
    
    public static function phpDateTime2mysql($phpDate){
        if(!empty($phpDate) && $phpDate != '0000-00-00 00:00:00'){
            $arrType = explode(' ', $phpDate);
            $arr = explode('/', $arrType[0]);
            $time = (isset($arrType[1]))?' '.$arrType[1]:'';
            $date = new DateTime(($arr[2] - 543).'-'.$arr[1].'-'.$arr[0].$time);
            return $date->format('Y-m-d H:i:s');
        }else{
            return '';
        }
	}
    public function convertToPhysicalDate($phpDate){
        if(!empty($phpDate) && $phpDate != '0000-00-00 00:00:00'){
            $arrType = explode(' ', $phpDate);
            $arr = explode('/', $arrType[0]);
            $time = (isset($arrType[1]))?' '.$arrType[1]:'';
            $date = new DateTime(($arr[2]).'-'.$arr[1].'-'.$arr[0].$time);
            return $date->format('d/m/Y');
        }else{
            return '';
        }
    }
}