ArrayQuery Component for Yii2
=============================

Allows searching/filtering of an array. This component is very useful when displaying array data in GridViews with an
ArrayDataProvider.


Querying Data
-------------

You may execute complex query on the array data using [[\metronic\components\query\ArrayQuery]] class. This class works similar to regular [[\yii\db\Query]] and uses same syntax. For example:

```php
$data = [
    [
        'id' => 1,
        'username' => 'admin',
        'email' => 'admin@example.com'
    ],
    [
        'id' => 2,
        'username' => 'test',
        'email' => 'test@example.com'
    ],
];

$query = new ArrayQuery();
$query->from($data);
$query->where(['username' => 'admin']);

$rows = $query->all();
```

Using with ArrayDataProvider
----------------------------

You may perform filtering using [[\metronic\components\ArrayQuery]] class. For example:
```php
<?php

// Some search model
    
/**
 * Setup search function for filtering and sorting.
 * @param $params
 * @return ArrayDataProvider
 */
public function search($params)
{
    $models = User::find()->asArray()->all();

    $query = new ArrayQuery();
    $query->from($models);

    // load the search form data and validate
    if ($this->load($params) && $this->validate()) {
        // adjust the query by adding the filters
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);
    }
    
    // prepare the ArrayDataProvider
    return new ArrayDataProvider([
        'allModels' => $query->indexBy('id')->all(),
        'sort' => [
            'attributes' => ['id', 'username', 'email', 'status'],
        ],
        'pagination' => [
            'pageSize' => 10
        ],
    ]);
}

```