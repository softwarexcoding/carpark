<?php 
namespace metronic\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Component;

class RegisterJS extends Component{

    public function regis($assets,$view){
        $asset = [];
        if(!empty($assets) && is_array($assets)){
            foreach($assets as $val){
                $class = RegisterJS::getAssetClass($val);
                $class::register($view);
            }
        }
    }

    public function getAssetClass($key){
        $items = [
            'sweetalert' => 'metronic\assets\SweetAlertAsset',
            'bootbox' => 'metronic\assets\BootBoxAsset',
            'ajaxcrud' => 'metronic\widgets\ajaxcrud\CrudAsset',
            'growl' => 'metronic\widgets\growl\GrowlAsset',
            'codemirror' => 'metronic\assets\CodeMirrorAsset',
            'waitme' => 'metronic\assets\WaitMeAsset'
        ];
        return ArrayHelper::getValue($items,$key);
    }
}