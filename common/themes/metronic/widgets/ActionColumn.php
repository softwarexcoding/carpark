<?php

namespace metronic\widgets;

use Yii;
use yii\helpers\Html;
use yii\grid\ActionColumn as BaseActionColumn;
use yii\helpers\ArrayHelper;
/**
 * ActionColumn class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @link http://www.appxq.com/
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @package 
 * @version 2.0.0 Date: Sep 5, 2015 9:52:45 AM
 * @example 
 */
class ActionColumn extends BaseActionColumn {

	public $pjax_id;

	public $template = '<div class="btn-group btn-group-circle" role="group"> {view} {update} {delete} </div>';

	public $viewOptions = [];

	public $updateOptions = [];

	public $deleteOptions = [];

	
	/**
	 * Initializes the default button rendering callbacks.
	 */
	protected function initDefaultButtons() {
		if (!isset($this->buttons['view'])) {
			$this->buttons['view'] = function ($url, $model, $key) {
				return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,ArrayHelper::merge([
							'data-action' => 'view',
							'title' => Yii::t('yii', 'View'),
							'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
							//'class' => 'btn btn-outline btn-default grey-mint btn-sm black',
				],$this->viewOptions));
			};
		}
		if (!isset($this->buttons['update'])) {
			$this->buttons['update'] = function ($url, $model, $key) {
				return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ArrayHelper::merge([
							'data-action' => 'update',
							'title' => Yii::t('yii', 'Update'),
							'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
							//'class' => 'btn btn-outline btn-default grey-mint btn-sm black'
				],$this->updateOptions));
			};
		}
		if (!isset($this->buttons['delete'])) {
			$this->buttons['delete'] = function ($url, $model, $key) {
				return Html::a('<i class="glyphicon glyphicon-trash text-danger"></i>', $url, [
							'data-action' => 'delete',
							'title' => Yii::t('yii', 'Delete'),
							'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
							'data-method' => 'post',
							'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
							//'class' => 'btn btn-outline btn-default grey-mint btn-sm black'
				]);
			};
		}
	}

}
