<?php
namespace metronic\widgets;

use msoft\widgets\Icon;
use yii\bootstrap\Widget;
use yii\helpers\Html;

class DashboardStat extends Widget
{
    /**
     * @var string small box background color
     */
    public $color = 'blue';

    /**
     * @var string font awesome icon name
     */
    public $icon;

    /**
     * @var string header text
     */
    public $desc;


    public $text;

    /**
     * @var string value number
     */
    public $number;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, 'dashboard-stat dashboard-stat-v2');
        if (!empty($this->color)) {
            Html::addCssClass($this->options, $this->color);
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo Html::beginTag('a', $this->options);
        echo Html::beginTag('div',['class' => 'visual']);
        if (!empty($this->icon)) {
            echo Icon::show($this->icon);
        }
        echo Html::endTag('div');
        echo Html::beginTag('div', ['class' => 'details']);
        echo Html::beginTag('div', ['class' => 'number']);
        echo Html::tag('span', $this->number, ['data-counter' => 'counterup','data-value' => $this->number]);
        echo $this->text;
        echo Html::endTag('div');
        echo Html::tag('div', $this->desc, ['class' => 'desc']);
        echo Html::endTag('div');
        echo Html::endTag('a');
    }
}