<?php 

namespace metronic\widgets\ajaxcrud;

use yii\web\AssetBundle;

class CrudAsset extends AssetBundle
{
    public $sourcePath = '@metronic/widgets/ajaxcrud/assets';

    public $css = [
        'ajaxcrud.css?v1.0'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'msoft\widgets\grid\GridViewAsset',
    ];
    
   public function init() {
       
       $this->js = YII_DEBUG ? [
           'ModalRemote.js?v1.0',
           'ajaxcrud.js?v1.0',
       ]:[
           'ModalRemote.min.js?v1.0',
           'ajaxcrud.min.js?v1.0',
       ];

       parent::init();
   }
}
