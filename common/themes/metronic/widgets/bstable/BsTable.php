<?php
/**
 * @copyright Federico Nicolás Motta
 * @author Federico Nicolás Motta <fedemotta@gmail.com>
 * @license http://opensource.org/licenses/mit-license.php The MIT License (MIT)
 * @package yii2-widget-datatables
 */
namespace metronic\widgets\bstable;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use metronic\widgets\bstable\BootstrapTableAsset;

/**
 * Datatables Yii2 widget
 * @author Federico Nicolás Motta <fedemotta@gmail.com>
 */
class BsTable extends \msoft\widgets\GridView
{
    /**
    * @var array the HTML attributes for the container tag of the datatables view.
    * The "tag" element specifies the tag name of the container element and defaults to "div".
    * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
    */
    public $options = [];

    public $striped = false;
    /**
    * @var array the HTML attributes for the datatables table element.
    * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
    */
    public $tableOptions = [
        "class"=>"table table-hover",
        'data-show-export' => 'true',
        'data-resizable' => 'true',
        'data-reorderable-columns' => 'true',
        "width"=>"100%"
    ];
    
    /**
    * @var array the HTML attributes for the datatables table element.
    * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
    */
    public $clientOptions = [];
    
    public $bstables = true;

    public $export = false;

    public $resizable = false;

    /**
     * Runs the widget.
     */
    public function run()
    {
        $clientOptions = ArrayHelper::merge($this->defaultClientOptions(),$this->clientOptions);
        $view = $this->getView();
        $id = $this->tableOptions['id'];
        $this->initBootstrapStyle();
        
        //Bootstrap3 Asset by default
        //DataTablesBootstrapAsset::register($view);
        if($this->bstables){
            $options = Json::encode($clientOptions);
            $view->registerJs("var $id = $('#$id').bootstrapTable($options);");
            $bundle = BootstrapTableAsset::register($view);
            if($this->export){
                $bundle->js[] = 'extensions/export/bootstrap-table-export.min.js';
                $bundle->js[] = 'extensions/export/tableExport.js';   
            }
            if($this->resizable){
                $bundle->js[] = 'extensions/resizable/bootstrap-table-resizable.min.js';
                $bundle->js[] = 'extensions/resizable/colResizable-1.5.source.js';
            }
        }
        
        
        //TableTools Asset if needed
        /*if (isset($clientOptions["tableTools"]) || (isset($clientOptions["dom"]) && strpos($clientOptions["dom"], 'T')>=0)){
            $tableTools = DataTablesTableToolsAsset::register($view);
            //SWF copy and download path overwrite
            $clientOptions["tableTools"]["sSwfPath"] = $tableTools->baseUrl."/swf/copy_csv_xls_pdf.swf";
        }*/
        
        //base list view run
        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                $content = $this->renderSection($matches[0]);

                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        } else {
            $content = $this->renderEmpty();
        }
        $tag = ArrayHelper::remove($this->options, 'tag', 'div');
        echo Html::tag($tag, $content, $this->options);
    }
    
    /**
     * Initializes the datatables widget disabling some GridView options like 
     * search, sort and pagination and using DataTables JS functionalities 
     * instead.
     */
    public function init()
    {
        parent::init();
        
        //disable filter model by grid view
        $this->filterModel = null;
        
        //disable sort by grid view
        $this->dataProvider->sort = false;
        
        //disable pagination by grid view
        $this->dataProvider->pagination = false;
        
        //layout showing only items
        if($this->bstables){
            $this->layout = "{items}";
        }
        //the table id must be set
        if (!isset($this->tableOptions['id'])) {
            $this->tableOptions['id'] = 'bstables_'.$this->getId();
        }
    }
    /**
     * Returns the options for the datatables view JS widget.
     * @return array the options
     */
    protected function defaultClientOptions()
    {
        //return $this->clientOptions;
        $default = [
            'locale' => 'th-TH',
            'search' => true,
            'pagination' => true,
            'pageSize' => 20,
            'showColumns' => true
        ];
        return $default;
    }
    
    public function renderTableBody()
    {
        $models = array_values($this->dataProvider->getModels());
        if (count($models) === 0) {
            return "<tbody>\n</tbody>";
        } else {
            return parent::renderTableBody();
        }
    }

    protected function initBootstrapStyle()
    {
        Html::addCssClass($this->tableOptions, 'kv-grid-table');
        if (!$this->bootstrap) {
            return;
        }
        Html::addCssClass($this->tableOptions, 'table');
        if ($this->hover) {
            Html::addCssClass($this->tableOptions, 'table-hover');
        }
        if ($this->bordered) {
            Html::addCssClass($this->tableOptions, 'table-bordered');
        }
        if ($this->striped) {
            Html::addCssClass($this->tableOptions, 'table-striped');
        }
        if ($this->condensed) {
            Html::addCssClass($this->tableOptions, 'table-condensed');
        }
        if ($this->floatHeader) {
            if ($this->perfectScrollbar) {
                $this->floatOverflowContainer = true;
            }
            if ($this->floatOverflowContainer) {
                $this->responsive = false;
                Html::addCssClass($this->containerOptions, 'kv-grid-wrapper');
            }
        }
        if ($this->responsive) {
            Html::addCssClass($this->containerOptions, 'table-responsive');
        }
        if ($this->responsiveWrap) {
            Html::addCssClass($this->tableOptions, 'kv-table-wrap');
        }
    }
}
