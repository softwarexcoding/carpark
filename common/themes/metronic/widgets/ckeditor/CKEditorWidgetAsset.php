<?php

namespace metronic\widgets\ckeditor;

use yii\web\AssetBundle;

class CKEditorWidgetAsset extends AssetBundle
{
    public $sourcePath = '@metronic/widgets/ckeditor/assets';
    public $js = [
        'ckeditor/ckeditor.js',
        'ckeditor/dosamigos-ckeditor.widget.js',
        'ckeditor/plugin.js',
        'ckeditor/plugins/uploadwidget/plugin.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];
}