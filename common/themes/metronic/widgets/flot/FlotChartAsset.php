<?php
/**
 * @author Bogdan Burim <bgdn2007@ukr.net> 
 */

namespace metronic\widgets\flot;

use yii\web\AssetBundle;
use yii;

class FlotChartAsset extends AssetBundle
{

	public static $extra_js = [];

	public function init() {
		//Yii::setAlias('@metronic/assets/global/plugins/flot');

		foreach (static::$extra_js as $js_file) {
			$this->js[]= $js_file;
		}

		return parent::init();
	}

	public $sourcePath = '@metronic/assets/global/plugins/flot';

	public $css = [
	];

	public $js = [
		'jquery.flot.min.js'
	];

	public $depends = [
		'yii\web\JqueryAsset',
	];

}
