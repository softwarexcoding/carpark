<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace metronic\widgets\grid;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use msoft\widgets\grid\ActionColumn as BaseActionColumn;
use yii\helpers\ArrayHelper;

class ActionColumn extends BaseActionColumn
{
    public $headerOptions = ['class' => 'action-column'];

    public $template = '<div class="clearfix"><div class="btn-group" role="group">{view} {update} {delete}</div></div>';
    
    public $buttonOptions = [];

    public $viewOptions = ['role'=>'modal-remote'];

    public $updateOptions = ['role'=>'modal-remote'];

    public $deleteOptions = ['data-method' => 'post'];

    public $contentOptions = ['style' => 'text-align:center;'];

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url) {
                $options = $this->viewOptions;
                $title = Yii::t('yii', 'View');
                $icon = '<span class="glyphicon glyphicon-eye-open"></span>';
                $label = ArrayHelper::remove($options, 'label', $icon);
                $options = array_replace_recursive(['title' => $title, 'data-pjax' => '0'], $options);
                return Html::a($label, $url, $options);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url) {
                $options = $this->updateOptions;
                $title = Yii::t('yii', 'Update');
                $icon = '<span class="glyphicon glyphicon-pencil"></span>';
                $label = ArrayHelper::remove($options, 'label', $icon);
                $options = array_replace_recursive(['title' => $title, 'data-pjax' => '0'], $options);
                return Html::a($label, $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url) {
                $options = $this->deleteOptions;
                $title = Yii::t('yii', 'Delete');
                $icon = '<span class="glyphicon glyphicon-trash text-danger"></span>';
                $label = ArrayHelper::remove($options, 'label', $icon);
                $defaults = ['title' => $title, 'data-pjax' => 'false','data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?')];
                $options = array_replace_recursive($defaults, $options);
                $css = $this->grid->options['id'] . '-action-del';
                Html::addCssClass($options, $css);
                $view = $this->grid->getView();
                return Html::a($label, $url, $options);
            };
        }
    }
}
