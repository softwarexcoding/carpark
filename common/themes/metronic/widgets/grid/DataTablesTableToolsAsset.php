<?php
/**
 * @copyright Federico Nicolás Motta
 * @author Federico Nicolás Motta <fedemotta@gmail.com>
 * @license http://opensource.org/licenses/mit-license.php The MIT License (MIT)
 * @package yii2-widget-datatables
 */
namespace metronic\widgets\datatable;
use yii\web\AssetBundle;

/**
 * Asset for the DataTables TableTools JQuery plugin
 * @author Federico Nicolás Motta <fedemotta@gmail.com>
 */
class DataTablesTableToolsAsset extends AssetBundle 
{
    public $sourcePath = '@metronic/widgets/datatable/assets'; 

    public $css = [
        "TableTools/css/dataTables.tableTools.min.css",
    ];

    public $js = [
        "TableTools/js/dataTables.tableTools.min.js",
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'metronic\widgets\datatables\DataTableAsset',
    ];
}