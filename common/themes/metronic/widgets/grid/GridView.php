<?php
/**
 * @copyright Federico Nicolás Motta
 * @author Federico Nicolás Motta <fedemotta@gmail.com>
 * @license http://opensource.org/licenses/mit-license.php The MIT License (MIT)
 * @package yii2-widget-datatables
 */
namespace metronic\widgets\grid;

use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use metronic\widgets\datatables\DataTableAsset;

/**
 * Datatables Yii2 widget
 * @author Federico Nicolás Motta <fedemotta@gmail.com>
 */
class GridView extends \msoft\widgets\GridView
{
    /**
    * @var array the HTML attributes for the container tag of the datatables view.
    * The "tag" element specifies the tag name of the container element and defaults to "div".
    * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
    */
    public $options = [];

    public $striped = false;
    /**
    * @var array the HTML attributes for the datatables table element.
    * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
    */
    public $tableOptions = ["class"=>"table table-hover","cellspacing"=>"0", "width"=>"100%"];
    
    /**
    * @var array the HTML attributes for the datatables table element.
    * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
    */
    public $clientOptions = [];
    
    public $datatables = true;
    /**
     * Runs the widget.
     */
    public function run()
    {
        $clientOptions = ArrayHelper::merge($this->defaultClientOptions(),$this->clientOptions);
        $view = $this->getView();
        $id = $this->tableOptions['id'];
        $this->initBootstrapStyle();
        
        //Bootstrap3 Asset by default
        //DataTablesBootstrapAsset::register($view);
        if($this->datatables){
            $options = Json::encode($clientOptions);
            $view->registerJs("var $id = $('#$id').DataTable($options);");
            $view->registerJs("$id.on( 'order.dt search.dt', function () {
                $id.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();");
            $bundle = DataTableAsset::register($view);
        }
        
        
        //TableTools Asset if needed
        /*if (isset($clientOptions["tableTools"]) || (isset($clientOptions["dom"]) && strpos($clientOptions["dom"], 'T')>=0)){
            $tableTools = DataTablesTableToolsAsset::register($view);
            //SWF copy and download path overwrite
            $clientOptions["tableTools"]["sSwfPath"] = $tableTools->baseUrl."/swf/copy_csv_xls_pdf.swf";
        }*/
        
        //base list view run
        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                $content = $this->renderSection($matches[0]);

                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        } else {
            $content = $this->renderEmpty();
        }
        $tag = ArrayHelper::remove($this->options, 'tag', 'div');
        echo Html::tag($tag, $content, $this->options);
    }
    
    /**
     * Initializes the datatables widget disabling some GridView options like 
     * search, sort and pagination and using DataTables JS functionalities 
     * instead.
     */
    public function init()
    {
        parent::init();
        
        //disable filter model by grid view
        $this->filterModel = null;
        
        //disable sort by grid view
        $this->dataProvider->sort = false;
        
        //disable pagination by grid view
        $this->dataProvider->pagination = false;
        
        //layout showing only items
        if($this->datatables){
            $this->layout = "{items}";
        }
        //the table id must be set
        if (!isset($this->tableOptions['id'])) {
            $this->tableOptions['id'] = 'datatables_'.$this->getId();
        }
    }
    /**
     * Returns the options for the datatables view JS widget.
     * @return array the options
     */
    protected function defaultClientOptions()
    {
        //return $this->clientOptions;
        $default = [
            "lengthMenu"=> [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            "info"=>true,
            "responsive"=>true,
            "pageLength" =>10,
            'language' => [
                "sProcessing" =>   "กำลังดำเนินการ...",
                "sLengthMenu" =>   "แสดง _MENU_ แถว",
                "sZeroRecords" =>  "ไม่พบข้อมูล",
                "sInfo" =>         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty" =>    "แสดง 0 ถึง 0 จาก 0 แถว",
                "sInfoFiltered" => "(กรองข้อมูล _MAX_ ทุกแถว)",
                "sInfoPostFix" =>  "",
                "sSearch" =>       "ค้นหา: ",
                "oPaginate" => [
                    "sFirst" =>    "หน้าแรก",
                    "sPrevious" => "ก่อนหน้า",
                    "sNext" =>     "ถัดไป",
                    "sLast" =>     "หน้าสุดท้าย"
                ]
            ]
        ];
        return $default;
    }
    
    public function renderTableBody()
    {
        $models = array_values($this->dataProvider->getModels());
        if (count($models) === 0) {
            return "<tbody>\n</tbody>";
        } else {
            return parent::renderTableBody();
        }
    }

    protected function initBootstrapStyle()
    {
        Html::addCssClass($this->tableOptions, 'kv-grid-table');
        if (!$this->bootstrap) {
            return;
        }
        Html::addCssClass($this->tableOptions, 'table');
        if ($this->hover) {
            Html::addCssClass($this->tableOptions, 'table-hover');
        }
        if ($this->bordered) {
            Html::addCssClass($this->tableOptions, 'table-bordered');
        }
        if ($this->striped) {
            Html::addCssClass($this->tableOptions, 'table-striped');
        }
        if ($this->condensed) {
            Html::addCssClass($this->tableOptions, 'table-condensed');
        }
        if ($this->floatHeader) {
            if ($this->perfectScrollbar) {
                $this->floatOverflowContainer = true;
            }
            if ($this->floatOverflowContainer) {
                $this->responsive = false;
                Html::addCssClass($this->containerOptions, 'kv-grid-wrapper');
            }
        }
        if ($this->responsive) {
            Html::addCssClass($this->containerOptions, 'table-responsive');
        }
        if ($this->responsiveWrap) {
            Html::addCssClass($this->tableOptions, 'kv-table-wrap');
        }
    }
}
