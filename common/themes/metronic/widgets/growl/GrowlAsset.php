<?php

namespace metronic\widgets\growl;

use yii\web\AssetBundle;

/**
 * Class AlertAsset
 *
 * @package yii2mod\alert
 */
class GrowlAsset extends AssetBundle
{
    /**
     * @var string the directory that contains the source asset files for this asset bundle
     */
    public $sourcePath = '@metronic/assets';

    /**
     * @var array list of JavaScript files that this bundle contains
     */
    public $js = [
        'global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}