<?php

namespace metronic\widgets\imperavi;
use yii\web\AssetBundle;

class ImagemanagerImperaviRedactorPluginAsset extends AssetBundle
{
    public $sourcePath = '@metronic/widgets/imperavi/assets/plugins/imagemanager';
    public $js = [
        'imagemanager.js'
    ];
    public $css = [

    ];
    public $depends = [
        'metronic\widgets\imperavi\ImperaviRedactorAsset'
    ];
}