<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace metronic\widgets\imperavi;
use yii\web\AssetBundle;


class VideoImperaviRedactorPluginAsset extends AssetBundle
{
    public $sourcePath = '@metronic/widgets/imperavi/assets/plugins/video';

    public $js = [
        'video.js',
    ];

    public $css = [

    ];
    public $depends = [
        'metronic\widgets\imperavi\ImperaviRedactorAsset'
    ];

}