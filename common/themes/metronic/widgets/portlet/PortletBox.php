<?php
namespace metronic\widgets\portlet;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use msoft\widgets\DropdownX;
use yii\bootstrap\Tabs;

class PortletBox extends Widget{
    public $title = 'PortletBox';
    public $content;
    public $type = 'green-meadow';
    public $icon = '';
    public $tools = [];
    public $bodyOptions = ['class' => 'portlet-body'];
    public $actions = [];
    public $options = ['class' => 'portlet box '];
    public $captionHelper = '';
    public $dropdown = false;
    public $dropdownOptions = [];
    public $dropdownButton = ['class' => 'btn btn-default'];
    public $dropdownMenu = [];
    public $titleOptions = ['class'=>'portlet-title'];
    public $caption = ['class' => 'caption'];
    public $tabs = false;
    public $tabsMenu = [];
    
    
    const TYPE_GREEN_MEADOW = 'green-meadow';
    const TYPE_YELLOW = 'yellow';
    const TYPE_RED = 'red';
    const TYPE_GREEN = 'green';
    const TYPE_BLUE_HOKI = 'blue-hoki';
    const TYPE_RED_SUNGLO = 'red-sunglo';
	
	public function init(){
		parent::init();
        $this->_initHeader();
    }
    
	public function run(){
        echo $this->content;
        echo Html::endTag('div');
        echo Html::endTag('div');
	}

    private function _initHeader(){
        $id = $this->options['id'] = $this->getId();
        echo Html::beginTag('div', $this->options);
        if($this->title){
            $icon = Html::tag('i','',['class' => $this->icon]);
            $captionhelper = Html::tag('span',$this->captionHelper,['class' => 'caption-helper']);
            $caption = Html::tag('div',$icon.$this->title.$captionhelper,$this->caption);
            $tools = $this->renderTools();
            $actions = $this->renderActions();
            $dropdown = $this->renderDropdown();
            $tabs = $this->renderTabs();
            echo Html::tag('div',$caption.$tools.$actions.$dropdown.$tabs,$this->titleOptions);
        }
        echo Html::beginTag('div', $this->bodyOptions);
    }

    public function renderTools(){
        if($this->tools){
            $tools = '<div class="tools">';
            if(!empty($this->tools) && is_array($this->tools)){
                foreach ($this->tools as $tool) {
                    $tools .= Html::decode($tool); 
                }
            }
            $tools .= '</div>';
            return $tools;
        }
    }

    public function renderActions(){
        
        if(!empty($this->actions) && is_array($this->actions)){
            $actions = Html::beginTag('div',['class' => 'actions']);
            foreach ($this->actions as $action) {
				$actions .= Html::decode(' '.$action); 
            }
            $actions .= Html::endTag('div');
            return $actions;
        }
    }

    public function renderDropdown(){
        if($this->dropdown){
            $options = $this->dropdownButton;
            $label = ArrayHelper::remove($options, 'label', Yii::t('kvgrid', 'Actions'));
            $caret = ArrayHelper::remove($options, 'caret', ' <span class="caret"></span>');
            $options = array_replace_recursive($options, ['type' => 'button', 'data-toggle' => 'dropdown']);
            Html::addCssClass($options, 'dropdown-toggle');
            Html::addCssClass($this->dropdownOptions, 'dropdown');
            return  Html::beginTag('div',['class' => 'actions']).
                    Html::beginTag('div', $this->dropdownOptions).
                    Html::button($label . $caret, $options).
                    DropdownX::widget($this->dropdownMenu).
                    Html::endTag('div').
                    Html::endTag('div');
        }
    }

    public function renderTabs(){
        if($this->tabs){
            return Tabs::widget($this->tabsMenu);
        }
    }

    public function defaultTools(){
        return [
            Html::a('', 'javascript:;',['class' => 'collapse']),
            Html::a('', '#portlet-config',['data-toggle' => 'modal','class' => 'config']),
            Html::a('', 'javascript:;',['class' => 'fullscreen']),
            Html::a('', 'javascript:;',['class' => 'reload']),
            Html::a('', 'javascript:;',['class' => 'remove'])
        ];
    }
    
    public static function begin($config = array()) {
        parent::begin($config);
    }
    
    public static function end() {
        parent::end();
    }
}
/*
<?=
PortletBox::begin([
    'title' => '<span class="caption-subject bold uppercase">' . $this->title . '</span>',
    'icon' => 'icon-calendar font-dark',
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'dropdown' => true,
    'dropdownMenu' => [
        'items' => [
            ['label' => Icon::show('plus').'Action', 'url' => '#'],
            ['label' => 'Submenu 1', 'items' => [
                ['label' => 'Action', 'url' => '#'],
                ['label' => 'Another action', 'url' => '#'],
                ['label' => 'Something else here', 'url' => '#'],
                '<li class="divider"></li>',
                ['label' => 'Submenu 2', 'items' => [
                    ['label' => 'Action', 'url' => '#'],
                    ['label' => 'Another action', 'url' => '#'],
                    ['label' => 'Something else here', 'url' => '#'],
                    '<li class="divider"></li>',
                    ['label' => 'Separated link', 'url' => '#'],
                ]],
            ]],
            ['label' => 'Something else here', 'url' => '#'],
            '<li class="divider"></li>',
            ['label' => 'Separated link', 'url' => '#'],
        ],
        'encodeLabels' => false
    ],
    'dropdownOptions' => [],
    'dropdownButton' => ['class' => 'btn btn-circle btn-default '],

    #Tabs
    'tabs' => true,
    'tabsMenu' => [
        'items' => [
            [
                'label' => 'One',
                'active' => true,
                'options' => [
                    'id' => 'portlet_tab1'
                ]
            ],
            [
                'label' => 'Two',
                'headerOptions' => [],
                'options' => [
                    'id' => 'portlet_tab2'
                ]
            ],
            [
                'label' => 'Tree',
                'headerOptions' => [],
                'options' => [
                    'id' => 'portlet_tab3'
                ]
            ],
        ],
        'renderTabContent' => false
    ],
]);
?>
content
<?= PortletBox::end(); ?>
*/