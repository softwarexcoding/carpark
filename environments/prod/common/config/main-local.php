<?php
return [
    'components' => [
        // 'db' => [
        //     'class' => 'yii\db\Connection',
        //     'dsn' => 'mysql:host='.getenv('HOST').';port='.getenv('MYSQL_PORT').';dbname='.getenv('MYSQL_DATABASE'),
        //     'username' => getenv('MYSQL_USER'),
        //     'password' => getenv('MYSQL_PASSWORD'),
        //     'charset' => 'utf8',
        //     'enableQueryCache' => true,
        //     'enableSchemaCache' => true,
        //     'schemaCache' => 'cache',
        // ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=db;dbname=carpark',
            'username' => 'Andaman',
            'password' => 'b8888888',
            'charset' => 'utf8',
            'enableQueryCache' => true,
            'enableSchemaCache' => true,
            //'schemaCache' => 'cache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
