<?php
namespace metronic\assets;

use Yii;
use yii\web\AssetBundle;

class BootBoxAsset extends AssetBundle
{
    public $sourcePath = '@metronic/assets';
    public $css = [
    ];

    public $js = [
        'global/plugins/bootbox/bootbox.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}