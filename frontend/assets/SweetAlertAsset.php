<?php
namespace metronic\assets;

use Yii;
use yii\web\AssetBundle;

class SweetAlertAsset extends AssetBundle
{
    public $sourcePath = '@metronic/assets';
    public $css = [
        //'global/plugins/bootstrap-sweetalert/sweetalert.css'
    ];

    public $js = [
        //'global/plugins/bootstrap-sweetalert/sweetalert.min.js',
        'sweetalert2/dist/sweetalert.min.js?v1.0',
        'confirm/confirm.js?v1.0'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}