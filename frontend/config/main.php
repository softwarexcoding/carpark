<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','msoft\user\Bootstrap'],
    'controllerNamespace' => 'frontend\controllers',
    'homeUrl' => '/',
    'name' => 'CAR PARK',
    'modules' => [
        'user' => [
            'class' => 'msoft\user\Module',
            'enableConfirmation' => false,
            'enableFlashMessages' => false,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin'],
            'controllerMap' => [
                'security' => [
                    'class' => 'common\themes\authen\user\controllers\SecurityController',
                    'layout' => '@frontend/views/layouts/main-login.php',
                ],
                'recovery' => [
                    'class' => 'common\themes\authen\user\controllers\RecoveryController',
                    'layout' => '@frontend/views/layouts/main-login.php',
                ],
                'registration' => [
                    'class' => 'common\themes\authen\user\controllers\RegistrationController',
                    'layout' => '@frontend/views/layouts/main-login.php',
                ],
            ]
        ],
        'card' => [
            'class' => 'frontend\modules\card\Module',
        ],
        'gate' => [
            'class' => 'frontend\modules\gate\Module',
        ],
        'payment' => [
            'class' => 'frontend\modules\payment\Module',
        ],
        'setting' => [
            'class' => 'frontend\modules\setting\Module',
        ],
        'report' => [
            'class' => 'frontend\modules\report\Module',
        ],
    ],
    'components' => [
        'fileStorage'=>[
            'class' => 'trntv\filekit\Storage',
            'baseUrl' => '@web/uploads',   
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeFormat' => 'php:H:i:s',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => 'msoft\user\models\User',
            'identityCookie' => [
                'name' => '_identity-frontend',
                'httpOnly' => true,
            ],
            'enableAutoLogin' => true,
        ],
        'session' => [
            //'class' => 'yii\web\DbSession',
            'name' => 'FRONTENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path' => '/',
            ],
            'timeout' => 60*60*24*14,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,   // do not publish the bundle
                    /*'js' => [
                        '//code.jquery.com/jquery-2.2.4.min.js',
                    ],*/
                    'js' => [
                        YII_ENV_DEV ? 'js/jquery-2.2.4.js?v2.2.4' : 'js/jquery-2.2.4.min.js?v2.2.4'
                    ]
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@msoft/user/views' => '@frontend/views/user/views',
                ],
            ],
        ],
    ],
    'as access' => [
        'class' => 'msoft\admin\components\AccessControl',
        'allowActions' => [
            'user/registration/*',
            'user/security/*',
            'user/recovery/*',
            'gate/transaction/*'
        ]
    ],
    'params' => $params,
];
