<?php

namespace frontend\modules\card\models;

use Yii;

/**
 * This is the model class for table "tb_company".
 *
 * @property int $company_id
 * @property string $company_name
 */
class TbCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id'], 'integer'],
            [['company_name'], 'string', 'max' => 255],
            [['company_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'company_name' => 'Company Name',
        ];
    }
}
