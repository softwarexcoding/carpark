<?php
use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use msoft\widgets\Select2;
use msoft\widgets\CheckboxX;
use frontend\modules\card\models\TbCardModel;
use frontend\modules\card\models\TbCompany;
use frontend\modules\card\models\TbSection;
use frontend\modules\card\models\TbCardType;
use frontend\modules\card\models\TbCardStatus;
use frontend\modules\card\models\TbLotFloor;
use msoft\widgets\DateTimePicker;
use metronic\widgets\portlet\PortletBox;
use msoft\widgets\Icon;
use msoft\widgets\Panel;
use yii\web\View;

?>
<div class="tb-card-form">
    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);?>
        <div class="form-group">
            <?= Html::activeLabel($model, 'card_seq', ['label' => 'เลขที่', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-10">
                <?= $form->field($model, 'card_seq',['showLabels' => false])->textInput([
                    'maxlength' => true,
                    //'readonly' => $model->isNewRecord ? false : true,
                     
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'card_id', ['label' => 'หมายเลขบัตร', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-10">
                <?= $form->field($model, 'card_id',['showLabels' => false])->textInput([
                    'maxlength' => true,
                    //'readonly' => $model->isNewRecord ? false : true,
                     
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'card_type_id', ['label' => 'ประเภทบัตร', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'card_type_id',['showLabels' => false])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TbCardType::find()->all(), 'card_type_id', 'card_type_name'),
                    'language' => 'en',
                    'pluginOptions' => [
                        'placeholder' => 'เลือกประเภทบัตร...',
                        'allowClear' => true,
                    //'disabled' => true
                    ],
                ]) ?>
            </div>
            <?= Html::activeLabel($model, 'card_model_id', ['label' => 'รุ่นบัตร', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'card_model_id',['showLabels' => false])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TbCardModel::find()->all(), 'card_model_id', 'card_model_name'),
                    'language' => 'en',
                    'pluginOptions' => [
                        'placeholder' => 'เลือกรุ่นบัตร...',
                        'allowClear' => true,
                    //'disabled' => true
                    ],
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'card_owner_name', ['label' => 'ชื่อผู้ถือบัตร', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-10">
                <?= $form->field($model, 'card_owner_name',['showLabels' => false])->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'licenceplate_no', ['label' => 'ทะเบียนรถ', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-10">
                <?= $form->field($model, 'licenceplate_no',['showLabels' => false])->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'company_id', ['label' => 'สถาบัน', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'company_id',['showLabels' => false])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TbCompany::find()->all(), 'company_id', 'company_name'),
                    'language' => 'en',
                    'pluginOptions' => [
                        'placeholder' => 'เลือกสถาบัน...',
                        'allowClear' => true,
                    //'disabled' => true
                    ],
                    'options'=>[
                        'value' => $model->isNewRecord ? $model['company_id'] = 1 : $model['company_id'],
                    ],
                ]) ?>
            </div>
            <?= Html::activeLabel($model, 'section_id', ['label' => 'แผนก', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'section_id',['showLabels' => false])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TbSection::find()->all(), 'section_id', 'section_name'),
                    'language' => 'en',
                    'pluginOptions' => [
                        'placeholder' => 'เลือกแผนก...',
                        'allowClear' => true,
                    //'disabled' => true
                    ],
                    'options'=>[
                        'value' => $model->isNewRecord ? $model['section_id'] = 1 : $model['section_id'],
                    ],
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'lot_floor', ['label' => 'ชั้นจอดรถ', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'lot_floor',['showLabels' => false])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TbLotFloor::find()->all(), 'lot_floor', 'lot_floor_name'),
                    'language' => 'en',
                    'pluginOptions' => [
                        'placeholder' => 'เลือกชั้นจอดรถ...',
                        'allowClear' => true,
                    //'disabled' => true
                    ],
                ]) ?>
            </div>
            <?= Html::activeLabel($model, 'lot_name', ['label' => 'ช่องจอดรถ', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'lot_name',['showLabels' => false])->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'card_issuedate', ['label' => 'วันเริ่มใช้บัตร', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'card_issuedate',['showLabels' => false])->widget(DateTimePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy hh:ii:ss',

                    ],
                    'options' => [
                        'class' => 'form-control',
                        'value' => $model->isNewRecord ? $model->defaultdate : $model['card_issuedate'],
                    ],

                ]) ?>
            </div>
            <?= Html::activeLabel($model, 'card_expdate', ['label' => 'วันหมดอายุ', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'card_expdate',['showLabels' => false])->widget(DateTimePicker::classname(), [
                    'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd/mm/yyyy hh:ii:ss'
                        ],
                    'options' => [
                        'class' => 'form-control',
                    ],
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'card_status', ['label' => 'สถานะบัตร', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-10">
                <?= $form->field($model, 'card_status',['showLabels' => false])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TbCardStatus::find()->all(), 'card_status', 'card_status_name'),
                    'language' => 'en',
                    'pluginOptions' => [
                        'placeholder' => 'เลือกสถานะบัตร...',
                        'allowClear' => true,
                    ],
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::activeLabel($model, 'parking_type', ['label' => 'เงื่อนไขการจอดรถ', 'class' => 'col-sm-2 control-label']) ?>
            <div class="col-sm-10">
                <?= $form->field($model, 'parking_type',['showLabels' => false])->widget(Select2::classname(), [
                    'data' => [
                        'all' => "ไม่มีเงื่อนไข", 
                        'day' => "เงื่อนไขตามวัน", 
                        'date' => "เงื่อนไขตามวันที่",
                    ],
                    'options'=>[
                            'value' => $model->isNewRecord ? 'all' : $model['parking_type'],
                        ],
                    
                    'pluginEvents' => [
                            "change" => "function() { Project.ApplyInputGRP($(this).val()); }",
                        ],
                    'language' => 'en',
                    'pluginOptions' => [
                        'placeholder' => 'เลือกเงื่อนไขการจอดรถ...',
                        'allowClear' => true,
                    ],

                ]) ?>
            </div>
        </div>
        <div class="form-group subgrp-input-date" style="<?= $model['parking_type'] == 'date' ? 'display:block;' : 'display:none;'?>">
            <label class="col-sm-2 control-label">ระบุวันที่</label>
                <div class="col-sm-10" >
                    <table  border="0">
                        <thead>
                        <?php $no = 1; ?>
                        <?php for ($i=1; $i <= 6; $i++) { ?>
                            <tr>
                                <?php for ($x=1; $x <= 6; $x++) {?>
                                    <?php if($no<=31){ ?>
                                    <th style="text-align: right;width:110px;height: 30px;">
                                    <label class="cbx-label" for="$x"><?= $no ?></label>
                                    <?= CheckboxX::widget([
                                        'name'=> 'TbCard['.$no.'][parking_date]',
                                        'options'=>[
                                            'id'=> $no,
                                            
                                        ],
                                        'value' => $model->isNewRecord ? '0' : $model->setValuedate($no),
                                        'pluginOptions'=>[
                                            'size'=>'md',
                                            'threeState' => false,
                                        ],
                                    ]); ?>
                                    </th>
                                    <?php } ?>
                                <?php  $no++;} $no;?>
                            </tr>   
                        <?php } ?>
                        </thead>
                    </table>
                </div>
            </div>       
                
                <div class="form-group subgrp-input-day" style="<?= $model['parking_type'] == 'day' ? 'display:block;' : 'display:none;'?>">
                    <label class="col-sm-2 control-label">ระบุวัน</label>
                        <div class="col-sm-10">
                            <?= CheckboxX::widget([
                                'model' => $model,
                                'attribute'=>'parking_mon', 
                                'options'=>[
                                    'value' => $model->isNewRecord ? '0' : $model['parking_mon'],
                                ],
                                'pluginOptions'=>[
                                    'size'=>'md',
                                    'threeState' => false,
                                ],
                                
                            ]); ?><label class="cbx-label" for="parking_mon">จันทร์  </label>

                            <?= CheckboxX::widget([
                                'model' => $model,
                                'attribute'=>'parking_tue', 
                                'options'=>[
                                    'value' => $model->isNewRecord ? '0' : $model['parking_tue'],
                                ],
                                'pluginOptions'=>[
                                    'size'=>'md',
                                    'threeState' => false,
                                ]
                            ]); ?><label class="cbx-label" for="parking_tue">อังคาร  </label>

                            <?= CheckboxX::widget([
                                'model' => $model,
                                'attribute'=>'parking_wen', 
                                'options'=>[
                                    'value' => $model->isNewRecord ? '0' : $model['parking_wen'],
                                ],
                                'pluginOptions'=>[
                                    'size'=>'md',
                                    'threeState' => false,
                                ]
                            ]); ?><label class="cbx-label" for="parking_wen">พุธ  </label>

                            <?= CheckboxX::widget([
                                'model' => $model,
                                'attribute'=>'parking_thu', 
                                'options'=>[
                                    'value' => $model->isNewRecord ? '0' : $model['parking_thu'],
                                ],
                                'pluginOptions'=>[
                                    'size'=>'md',
                                    'threeState' => false,
                                ]
                            ]); ?><label class="cbx-label" for="parking_thu">พฤหัสบดี  </label>

                            <?= CheckboxX::widget([
                                'model' => $model,
                                'attribute'=>'parking_fri', 
                                'options'=>[
                                    'value' => $model->isNewRecord ? '0' : $model['parking_fri'],
                                ],
                                'pluginOptions'=>[
                                    'size'=>'md',
                                    'threeState' => false,
                                ]
                            ]); ?><label class="cbx-label" for="parking_fri">ศุกร์  </label>

                            <?= CheckboxX::widget([
                                'model' => $model,
                                'attribute'=>'parking_sat',
                                'options'=>[
                                    'value' => $model->isNewRecord ? '0' : $model['parking_sat'],
                                ], 
                                'pluginOptions'=>[
                                    'size'=>'md',
                                    'threeState' => false,
                                ]
                            ]); ?><label class="cbx-label" for="parking_sat">เสาร์  </label>

                            <?= CheckboxX::widget([
                                'model' => $model,
                                'attribute'=>'parking_sun', 
                                'options'=>[
                                    'value' => $model->isNewRecord ? '0' : $model['parking_sun'],
                                ],
                                'pluginOptions'=>[
                                    'size'=>'md',
                                    'threeState' => false,
                                ]
                            ]); ?><label class="cbx-label" for="parking_sun">อาทิตย์</label>
                        </div>
                </div>
        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-actions" style="border-top: 1px solid #eee;">
                <div class="row">
                    <div class="col-md-12" style="text-align:right;">
                        <?= Html::a('Close',['index'],['class' => 'btn btn-default']); ?>
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php ActiveForm::end(); ?>
</div>
<?php 
$this->registerJs($this->render('./js/_form_script.js'), View::POS_READY,'body');
?>
