<?php

use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;
use metronic\widgets\portlet\PortletBox;
use yii\widgets\Pjax;
use msoft\widgets\select2\Select2Asset;
use metronic\components\DateConvert;
use msoft\widgets\datatables\DataTablesAsset;

DataTablesAsset::register($this);

Select2Asset::register($this)->css[] = 'css/select2-krajee.css';
RegisterJS::regis(['sweetalert'],$this);
$classmidle = ['class' => 'kv-align-center kv-align-middle','style' => 'color:black;'];

$this->title = 'ลงทะเบียนบัตร';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
    .dt-nowrap{
        white-space: nowrap;
    }
    .dt-licenceplate_no{
        width : 100px;
    }
    .dt-200{
        width : 200px;
    }
');
use yii\data\ArrayDataProvider;
$provider = new ArrayDataProvider([
    'allModels' => [],
    'pagination' => [
        'pageSize' => 100,
    ],
]);
?>
<?= SwalAlert::widget(); ?>
<?= PortletBox::begin([
    'title' => '<span class="caption-subject bold uppercase">' . $this->title . '</span>',
    'icon' => 'fa fa-cube font-dark',
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'tools' => [
    ],
    'actions' => [
       Html::a(Icon::show('plus') .' เพิ่มข้อมูล',['create'],['class' => 'btn btn-success','role' => 'modal-remote'])
    ]
]);
?>
<div class="tb-card-index">
    <?php Pjax::begin([ 'timeout' => 5000, 'id'=> 'index']) ?>
    <?= Datatables::widget([
        'dataProvider' => $provider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'resizableColumns' => false,
        'afterHeader' => [
            [
                'columns' => [
                    ['content' => '','options' => ['class'=>'skip-export','colspan' => 4]],
                    ['content' => '','options' => ['class'=>'skip-export']],
                    ['content' => '','options' => ['class'=>'skip-export','colspan' => 2]],
                    ['content' => '','options' => ['class'=>'skip-export']],
                    ['content' => '','options' => ['class'=>'skip-export','colspan' => 4]],
                ]
            ]
        ],
        'tableOptions' => ['id' => 'example1'],
        'hover' => true,
        'bordered' => false,
        'condensed' => true,
        'striped' => true,
        'responsive' => false,
        'layout' => '{items}',
        'clientOptions' => [
            "ajax" => 'test',
            "lengthMenu" => [[5,10,15,20,-1],[5,10,15,20,"All"]],
            "info" => true,
            "responsive" => true,
            "pageLength" => 20,
            "processing" => true,
            "autoWidth"  => false,
            "columns" => [
                [ "data" => "order" ],
                [ "data" => "licenceplate_no","className"=>"dt-licenceplate_no  text-center"],
                [ "data" => "card_seq" ],
                [ "data" => "card_id" ],
                [ "data" => "card_type_name"  ,"className"=>"dt-200  text-center"],
                [ "data" => "card_owner_name" ,"className"=>"dt-200  text-left"],
                [ "data" => "section" ,"className"=>"dt-200  text-left"],
                [ "data" => "company" ,"className"=>"dt-200  text-left"],
                [ "data" => "action" ,"className"=>"dt-nowrap"]
            ],
            "columnDefs" => [
                [ "sClass" => "text-center", "targets" => [0,1,2,3,4,5,6,7,8] ],
                [ "sClass" => "dt-nowrap", "targets" => [1] ]
            ],
            "initComplete" => new \yii\web\JsExpression("function () {
                initSt2(this,\"select1\",4,\"ประเภท\");
                initSt2(this,\"select2\",7,\"สถาบัน\");
            }")
        ],
        'columns' => [
            [
                'class' => 'msoft\widgets\grid\SerialColumn',
            ],
            [
                'header' => 'ทะเบียนรถ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
            ],
            [
                'header' => 'เลขที่',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
            ],
            [
                'header' => 'หมายเลข',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
            ],
            [
                'header' => 'ประเภท',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
            ],
            [
                'header' => 'ชื่อผู้ถือ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
            ],
            [
                'header' => 'หน่วยงาน',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
            ],
            [
                'header' => 'สถาบัน',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
            ],
            [
                'class' => 'msoft\widgets\ActionColumn',
                'noWrap' => TRUE,
                
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
<?= PortletBox::end(); ?>
<?php 
echo $this->render('modal'); 
$this->registerJsFile(
    '@web/js/dt-s2.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>