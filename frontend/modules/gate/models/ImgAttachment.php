<?php

namespace frontend\modules\gate\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use metronic\components\DateConvert;
use msoft\behaviors\CoreMultiValueBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "img_attachment".
 *
 * @property integer $id
 * @property integer $trans_id
 * @property integer $gate_id
 * @property string $path
 * @property string $name
 * @property integer $createby
 * @property string $createat
 */
class ImgAttachment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'img_attachment';
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createby'],
                ],
                'value' => !empty(Yii::$app->user->id)?Yii::$app->user->id:1,
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createat'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trans_id','gate_id','createby'], 'integer'],
            [['path', 'name'], 'required'],
            [['createat'], 'safe'],
            [['path'], 'string', 'max' => 1024],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trans_id' => 'Trans ID',
            'gate_id' => 'Gate ID',
            'path' => 'Path',
            'name' => 'Name',
            'createby' => 'Createby',
            'createat' => 'Createat',
        ];
    }
}
