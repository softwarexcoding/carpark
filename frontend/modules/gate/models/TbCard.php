<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_card".
 *
 * @property integer $ids
 * @property string $card_seq
 * @property string $card_id
 * @property integer $card_type_id
 * @property integer $card_model_id
 * @property string $card_owner_name
 * @property string $licenceplate_no
 * @property string $card_issuedate
 * @property string $card_expdate
 * @property integer $card_status
 * @property integer $createby
 * @property string $createat
 * @property integer $updateby
 * @property string $updateat
 * @property string $lot_name
 * @property string $lot_floor
 * @property integer $contact_num
 * @property string $section_name
 * @property string $parking_type
 * @property string $parking_date
 * @property string $parking_mon
 * @property string $parking_tue
 * @property string $parking_wen
 * @property string $parking_thu
 * @property string $parking_fri
 * @property string $parking_sat
 * @property string $parking_sun
 */
class TbCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_model_id', 'card_status', 'createby', 'updateby', 'contact_num'], 'integer'],
            [['card_issuedate', 'card_expdate', 'createat', 'updateat','card_type_id'], 'safe'],
            [['card_seq', 'lot_floor'], 'string', 'max' => 50],
            [['card_id', 'card_owner_name', 'licenceplate_no', 'lot_name', 'section_name'], 'string', 'max' => 100],
            [['parking_type'], 'string', 'max' => 10],
            [['parking_date'], 'string', 'max' => 255],
            [['parking_mon', 'parking_tue', 'parking_wen', 'parking_thu', 'parking_fri', 'parking_sat', 'parking_sun'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ids' => 'รหัสรายการ',
            'card_seq' => 'Card Seq',
            'card_id' => 'หมายเลขบัตร',
            'card_type_id' => 'ประเภทบัตร',
            'card_model_id' => 'รหัสรุ่นบัตร',
            'card_owner_name' => 'ชื่อผู้ถือบัตร',
            'licenceplate_no' => 'ทะเบียนรถ',
            'card_issuedate' => 'วันเริ่มใช้บัตร',
            'card_expdate' => 'หมดอายุ',
            'card_status' => 'สถานะบัตร',
            'createby' => 'สร้างโดย',
            'createat' => 'สร้างเมื่อ',
            'updateby' => 'ปรับปรุงโดย',
            'updateat' => 'ปรับปรุงเมื่อ',
            'lot_name' => 'ช่องจอดรถ',
            'lot_floor' => 'ชั้นจอดรถ',
            'contact_num' => 'Contact Num',
            'section_name' => 'Section Name',
            'parking_type' => 'Parking Type',
            'parking_date' => 'Parking Date',
            'parking_mon' => 'Parking Mon',
            'parking_tue' => 'Parking Tue',
            'parking_wen' => 'Parking Wen',
            'parking_thu' => 'Parking Thu',
            'parking_fri' => 'Parking Fri',
            'parking_sat' => 'Parking Sat',
            'parking_sun' => 'Parking Sun',
        ];
    }
    public function getCardtype(){
        return $this->hasOne(TbCardType::className(),['card_type_id'=>'card_type_id']);
    }
}
