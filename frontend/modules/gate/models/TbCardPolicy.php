<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_card_policy".
 *
 * @property integer $card_policy_id
 * @property integer $gate_id
 * @property integer $card_type_id
 */
class TbCardPolicy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_card_policy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gate_id', 'card_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'card_policy_id' => 'รหัสนโยบาย',
            'gate_id' => 'รหัสประตู',
            'card_type_id' => 'รหัสประเภทบัตร',
        ];
    }
}
