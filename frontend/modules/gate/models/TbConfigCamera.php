<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_config_camera".
 *
 * @property integer $id
 * @property integer $gate_id
 * @property string $camera_name
 * @property string $ip
 * @property string $port
 * @property string $username
 * @property string $password
 * @property string $path
 */
class TbConfigCamera extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_config_camera';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gate_id'], 'integer'],
            [['camera_name', 'port'], 'string', 'max' => 10],
            [['ip'], 'string', 'max' => 20],
            [['username', 'password'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gate_id' => 'รหัสประตู',
            'camera_name' => 'ชื่อกล้อง',
            'ip' => 'ไอพี',
            'port' => 'พอร์ต',
            'username' => 'ชื่อผู้ใช้งาน',
            'password' => 'รหัสผ่าน',
        ];
    }
}
