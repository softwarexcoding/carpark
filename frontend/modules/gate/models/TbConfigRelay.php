<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_config_relay".
 *
 * @property integer $id
 * @property integer $gate_id
 * @property string $relay_name
 * @property string $ip
 * @property string $port
 * @property integer $delay
 * @property string $status_on
 * @property string $status_close
 */
class TbConfigRelay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_config_relay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gate_id', 'delay'], 'integer'],
            [['relay_name', 'port', 'status_on', 'status_close'], 'string', 'max' => 10],
            [['ip'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gate_id' => 'รหัสประตู',
            'relay_name' => 'ชื่อรีเลย์',
            'ip' => 'ไอพี',
            'port' => 'Port',
            'delay' => 'เวลาหน่วง',
            'status_on' => 'เปิด',
            'status_close' => 'ปิด',
        ];
    }
}
