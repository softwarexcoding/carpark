<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_config_ui".
 *
 * @property integer $id
 * @property string $member_charge
 * @property string $member_modaldisplay
 * @property string $member_modalautosave
 * @property integer $member_modaltime
 */
class TbConfigUi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_config_ui';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'member_modaltime'], 'integer'],
            [['member_charge', 'member_modaldisplay', 'member_modalautosave'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_charge' => 'คิดเงินบัตรบุคคลาการ card_type_id = 2,3,4 หรือไม่',
            'member_modaldisplay' => 'แสดง modal บัตรจอดบุคคลากร หรือไม่',
            'member_modalautosave' => 'Auto save  บัตรจอดบุคคลากร หรือไม่',
            'member_modaltime' => 'เวลาแสดง modal บัตรจอดบุคคลากร (วินาที)',
        ];
    }
}
