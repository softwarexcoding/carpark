<?php

namespace frontend\modules\gate\models;


use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use metronic\components\DateConvert;
use msoft\behaviors\CoreMultiValueBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "tb_transaction".
 *
 * @property integer $trans_id
 * @property string $card_id
 * @property string $licenceplate_no
 * @property string $gate1_datetime
 * @property string $gate2_datetime
 * @property string $gate3_datetime
 * @property string $total_time
 * @property string $total_min
 * @property string $fee_amt
 * @property integer $disc_type
 * @property integer $disc_by
 * @property string $disc_amt
 * @property string $disc_time
 * @property string $total_amt
 * @property string $total_paid
 * @property string $invoice_no
 * @property integer $trans_status
 * @property integer $createby
 * @property integer $updateby
 * @property string $createat
 * @property string $updateat
 */
class TbTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_transaction';
    }
    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createby', 'updateby'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updateby',
                ],
                'value' => !empty(Yii::$app->user->id)?Yii::$app->user->id:1,
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createat', 'updateat'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateat'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gate1_datetime', 'gate2_datetime', 'gate3_datetime', 'total_time','disc_type', 'createat', 'updateat'], 'safe'],
            [['total_min', 'fee_amt', 'disc_amt', 'disc_time', 'total_amt', 'total_paid'], 'number'],
            [['disc_by', 'trans_status', 'createby', 'updateby'], 'integer'],
            [['card_id'], 'string', 'max' => 100],
            [['licenceplate_no', 'invoice_no'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trans_id' => 'รหัส',
            'card_id' => 'หมายเลขบัตร',
            'licenceplate_no' => 'ทะเบียนรถ',
            'gate1_datetime' => 'เวลาประตู 1',
            'gate2_datetime' => 'เวลาประตู 2',
            'gate3_datetime' => 'เวลาประตู 3',
            'total_time' => 'เวลารวม',
            'total_min' => 'ส่วนต่างเวลา',
            'fee_amt' => 'ค่าบริการ',
            'disc_type' => 'ประเภทส่วนลด',
            'disc_by' => 'ผู้ให้ส่วนลด',
            'disc_amt' => 'มูลค่าส่วนลด',
            'disc_time' => 'เวลาส่วนลดนาที',
            'total_amt' => 'เป็นเงิน',
            'total_paid' => 'จ่ายจริง',
            'invoice_no' => 'หมายเลขนำส่งเงินสด',
            'trans_status' => 'สถานะ',
            'createby' => 'สร้างโดย',
            'updateby' => 'ปรับปรุงโดย',
            'createat' => 'สร้างเมื่อ',
            'updateat' => 'ปรับปรุงเมื่อ',
        ];
    }
    public function getCard(){
        return $this->hasOne(TbCard::className(),['card_id'=>'card_id']);
    }
}
