<?php
use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use metronic\components\DateConvert;
use frontend\modules\gate\models\ImgAttachment;
$date_cal = !empty($modelTrans->gate3_datetime)?$modelTrans->gate3_datetime:date("Y-m-d H:i:s");
$total_time = Yii::$app->db->createCommand("SELECT func_total_time('".$modelTrans->gate1_datetime."','".$date_cal."')")->queryScalar();
$cal_rate = Yii::$app->db->createCommand("SELECT func_cal_rate('".$modelTrans->gate1_datetime."','".$date_cal."')")->queryScalar();
$imgIn = ImgAttachment::findOne(['trans_id'=>$modelTrans->trans_id,'gate_id'=>1]);
$path_imgIn = !empty($imgIn)?'/'.$imgIn->path.$imgIn->name:'/';
$path_imgOut = !empty($modelImg)?'/'.$modelImg->path.$modelImg->name:'/';
?>
<?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL,'id'=>'form_bill']); ?>
<div class="col-sm-11">
    <div class="form-group">
        <label class="col-sm-2 control-label">หมายเลขบัตร</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'card_id', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
            ]);?>
        </div>
        <label class="col-sm-2 control-label">ประเภทบัตร</label>
        <div class="col-sm-4">
            <?= $form->field($modelCard, 'card_type_id', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => $modelCard->cardtype->card_type_name,
            ]);?>
        </div>
    </div>
    <div class="form-group">    
        <label class="col-sm-2 control-label">ชื่อ</label>
        <div class="col-sm-4">
            <?= $form->field($modelCard, 'card_owner_name', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
            ]);?>
        </div>
        <label class="col-sm-2 control-label">ทะเบียนรถ</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'licenceplate_no', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
            ]);?>
        </div>
    </div>
    <div class="form-group"> 
        <label class="col-sm-2 control-label">เข้าประตู1</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'gate1_datetime', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' =>  !empty($modelTrans->gate1_datetime)?DateConvert::mysql2phpDateTime($modelTrans->gate1_datetime):'',
            ]);?>
        </div>
        <label class="col-sm-2 control-label">เข้าประตู2</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'gate2_datetime', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' =>  !empty($modelTrans->gate2_datetime)?DateConvert::mysql2phpDateTime($modelTrans->gate2_datetime):'',
            ]);?>
        </div>
    </div>
    <div class="form-group"> 
        <label class="col-sm-2 control-label">เวลาออก</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'gate3_datetime', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => !empty($modelTrans->gate3_datetime)?DateConvert::mysql2phpDateTime($modelTrans->gate3_datetime):DateConvert::mysql2phpDateTime(date("Y-m-d H:i:s")),
            ]);?>
        </div>
        <label class="col-sm-2 control-label">เวลาจอด</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'total_time', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => $total_time,
            ]);?>
        </div>
    </div>
    <div class="form-group"> 
        <label class="col-sm-2 control-label">ค่าบริการ</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'fee_amt', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    // 'style' => [
                    //     'background-color' => '#5CB85C',
                    // ],
                    'value' => $cal_rate,
            ]);?>
        </div>
        <label class="col-sm-2 control-label">ส่วนลด</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'disc_type', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => ($modelTrans->disc_type==1)?'มีส่วนลด':'ไม่มีส่วนลด',
            ]);?>
        </div>
    </div>
    <div class="form-group"> 
        <label class="col-sm-2 control-label">เป็นเงิน</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'total_amt', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => ($modelTrans->disc_type==1)?'0':$cal_rate,
            ]);?>
        </div>
        <label class="col-sm-2 control-label">ชำระ</label>
        <div class="col-sm-4">
            <?= $form->field($modelTrans, 'total_paid', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => ($modelTrans->disc_type==1)?'0':$cal_rate,
                    'style' => 'font-size:25px',
            ]);?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" style="font-size: 20px;">ประตูเข้า</label>
        <div class="col-sm-4">
            <img style="margin-top: 15px;" src="<?= $path_imgIn ?>" alt="NO_IMGAGE" height="200" width="235">
        </div>
        <label class="col-sm-2 control-label" style="font-size: 20px;">ประตูออก</label>
        <div class="col-sm-4">
            <img style="margin-top: 15px;" src="<?= $path_imgOut ?>" alt="NO_IMGAGE" height="200" width="235">
        <div class="col-sm-4">
    </div>
    <?= Html::hiddenInput('gate3_datetime_hidden',!empty($modelTrans->gate3_datetime)?$modelTrans->gate3_datetime:date("Y-m-d H:i:s"),['id'=>'gate3']); ?>
    <?= Html::hiddenInput('img_ref',!empty($modelImg)?$modelImg->id:null,['id'=>'img_ref']);?>
    <?= Html::hiddenInput('disc_amt',($modelTrans->disc_type==1)?$cal_rate:'0',['id'=>'disc_amt']);?>
    <?= Html::hiddenInput('trans_id',$modelTrans->trans_id,['id'=>'trans_id']); ?>
</div>
<?php ActiveForm::end(); ?>
<div class="col-sm-12 modal-footer">
<?= Html::button('ยกเลิก', ['class' => 'btn btn-default','id'=> 'btnClose', 'data-dismiss' => "modal"]) . Html::a('บันทึกและพิมพ์',false,['class' => 'btn btn-success','id'=>'btnSave','onclick'=>"btnSave();"])?>
</div>
<script type="text/javascript">
$('#tbtransaction-total_paid').on('keyup',function(e){
    e.preventDefault();
    if(e.keyCode==13){
        btnSave();
    }
});
$('#ajaxCrudModal').on('shown.bs.modal', function() {
   $("#tbtransaction-total_paid").focus();
});
var img_ref = $('#img_ref').val();
$('#btnClose').on('click',function(){
    if(img_ref){
        $.ajax({
            url: "delete-img",
            type : "GET",
            data : {img_ref: img_ref},
            success: function(result){

            },error: function (xhr, status, error) {
                Notify.error(error);
                Loading.reload();
            } 
        });
    }
});
function btnSave(){
    if($('#tbtransaction-total_paid').val()){
        Loading.class();
        $('#ajaxCrudModal').modal('hide');
        $.ajax({
            url: "save-bill",
            type : "POST",
            data : $('#form_bill').serialize(),
            success: function(result){
                Save.success();
            },
            error: function (xhr, status, error) {
                Notify.error(error);
                Loading.reload();
            } 

        });
    }else{
        Notify.warning('กรุณาระบุยอดชำระ');
    }
}
</script>