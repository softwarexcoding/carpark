<?php
use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use metronic\components\DateConvert;
?>
<?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL,'id'=>'form_tran']); ?>
<div class="col-sm-11">
    <div class="form-group">
        <label class="col-sm-2 control-label">หมายเลขบัตร</label>
        <div class="col-sm-4">
            <?= $form->field($model, 'card_id', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => $modelCard['card_id'],
            ]);?>
        </div>
        <label class="col-sm-2 control-label">ประเภทบัตร</label>
        <div class="col-sm-4">
            <?= $form->field($modelCard, 'card_type_id', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => $modelCard->cardtype->card_type_name,
            ]);?>
        </div>
    </div>
    <div class="form-group">    
        <label class="col-sm-2 control-label">ชื่อ</label>
        <div class="col-sm-4">
            <?= $form->field($modelCard, 'card_owner_name', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
            ]);?>
        </div>
        <label class="col-sm-2 control-label">ทะเบียนรถ</label>
        <div class="col-sm-4">
            <?= $form->field($model, 'licenceplate_no', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'value' => $modelCard->card_type_id==1?$model->licenceplate_no:$modelCard->licenceplate_no,
                    'style' => 'font-size:25px',
            ]);?>
        </div>
    </div>
    <div class="form-group"> 
        <label class="col-sm-2 control-label">เวลาเข้า</label>
        <div class="col-sm-4">
            <?= $form->field($model, 'gate1_datetime', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => DateConvert::mysql2phpDateTime(date("Y-m-d H:i:s")),
            ]);?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" style="font-size: 20px;">ภาพถ่าย</label>
        <img style="margin-top: 15px;" src="<?= !empty($modelImg)?'/'.$modelImg->path.$modelImg->name:'/'; ?>" alt="NO_IMGAGE" height="200" width="235">
    </div>
    <?= Html::hiddenInput('gate1_datetime_hidden',date("Y-m-d H:i:s"),['id'=>'gate2']); ?>
    <?= Html::hiddenInput('img_ref',!empty($modelImg)?$modelImg->id:null,['id'=>'img_ref']); ?>
</div>
<?php ActiveForm::end(); ?>
<div class="col-sm-12 modal-footer">
<?= Html::button('ยกเลิก', ['class' => 'btn btn-default','id'=> 'btnClose', 'data-dismiss' => "modal"]) . Html::a('บันทึก',false,['class' => 'btn btn-success','id'=>'btnSave','onclick'=>"btnSave();"])?>
</div>
<script type="text/javascript">
$('#tbtransaction-licenceplate_no').on('keyup',function(e){
    e.preventDefault();
    if(e.keyCode==13){
        btnSave();
    }
});
$('#ajaxCrudModal').on('shown.bs.modal', function() {
   $("#tbtransaction-licenceplate_no").focus();
});
var img_ref = $('#img_ref').val();
$('#btnClose').on('click',function(){
    if(img_ref){
        $.ajax({
            url: "delete-img",
            type : "GET",
            data : {img_ref: img_ref},
            success: function(result){

            },error: function (xhr, status, error) {
                Notify.error(error);
                Loading.reload();
            } 
        });
    }
});
function btnSave(){
    //if($('#tbtransaction-licenceplate_no').val()){
        Loading.class();
        $('#ajaxCrudModal').modal('hide');
        $.ajax({
            url: "save",
            type : "POST",
            data : $('#form_tran').serialize(),
            success: function(result){
                Save.success();
            },
            error: function (xhr, status, error) {
                Notify.error(error);
                Loading.reload();
            } 

        });
    // }else{
    //     Notify.warning('กรุณาระบุทะเบียนรถ');
    // }
}
</script>