<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\gate\models\TbTransaction */

$this->title = 'Create Tb Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Tb Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-transaction-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
