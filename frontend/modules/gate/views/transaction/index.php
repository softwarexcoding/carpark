<?php
use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;
use msoft\widgets\datatables\DataTablesAsset;
use yii\helpers\ArrayHelper;
use metronic\components\DateConvert;
use metronic\widgets\portlet\PortletBox;
use frontend\modules\gate\models\TbConfigUi;
$modelConfig =TbConfigUi::findOne(1);
use frontend\assets\IPCamAsset;
IPCamAsset::register($this);
DataTablesAsset::register($this);
foreach ($modelGate->camera as $i=>$data) {
   $this->registerJs("autoLogin('{$data->ip}','{$data->port}','{$data->username}','{$data->password}',{$i});");
}
$this->title = $modelGate['gate_name'];
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
');
$this->registerCss($this->render('/css/custom-style.css'));
?>
<?php $this->registerJsFile(Yii::getAlias('@web') . '/js/socket.io.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?= PortletBox::begin([
    'title' => '<span class="caption-subject bold uppercase">'.$this->title.'</span>',
    'icon' => 'fa fa-car font-dark',
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'tools' => [
    ],
    'actions' => [
       
    ]
]);
?>
<div class="index">
    <div class="row">
        <div id="cam_display" class="col-sm-10 col-sm-offset-1" style="text-align: center;">
            <div id="divPlugin" class="plugin"></div>
        </div>
        <div class="col-sm-12" style="margin-top: 30px;">
            <div class="well" style="background-color:#eee;">
                <div class="form-group">
                    <div class="col-sm-1">CardID</div>
                    <div class="col-sm-4">
                        <?= Html::input('text', 'scancard', '', ['type' => 'text', 'maxlength'=>'10', 'id' => 'scancard', 'value' => '', 'class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Scan CardID','style'=>'margin-top:-10px']) ?>
                    </div>
                    <div class="col-sm-2">
                        <span id="errmsg" style="color: red;"></span>
                    </div> 
                    <div class="col-sm-5" style="margin-top: -15px;text-align: right;">
                        <?= Html::a('เปิดไม้กั้น',false,['class'=> 'btn btn-success','onclick'=>'barrierOpen();']) ?>
                        <?= Html::a('ปิดไม้กั้น',false,['class'=> 'btn btn-danger','onclick'=>'barrierClose();']) ?>
                    </div>   
                </div>
            </div>
            <div id="data_trans" class="tab-pane">
                <table id="dt_trans" class="table table-hover table-bordered table-condensed table-striped" cellspacing="0" width="100%"></table>
            </div>
        </div>
        <div class="form-group" style="text-align:right;">
        <div class="col-sm-12">
            <?= Html::a('Close', [$_SERVER['REQUEST_URI']], ['class' => 'btn btn-default']) ?>
        </div>
    </div>
    </div>
 </div>
<?= PortletBox::end(); ?>
<?php echo $this->render('modal'); ?>

<script type="text/javascript">
    function barrierOpen(){
        $.ajax({
            url: "http://<?= @$modelGate->relay->ip ?>/1234/6/?<?= @$modelGate->relay->status_on ?>"+"&",
            data: {},
            method : 'GET',
            success: function(result){
            },
            error: function(jqXHR,textStatus,errorThrown){
                console.log(errorThrown);
            }
        });
        setTimeout(function() {
          relayOff('<?= @$modelGate->relay->status_on ?>');
        }, <?= @$modelGate->relay->delay?>);
       
    }
    function barrierClose(){
        $.ajax({
            url: "http://<?= @$modelGate->relay->ip ?>/1234/6/?<?= @$modelGate->relay->status_close ?>"+"&",
            data: {},
            method : 'GET',
            success: function(result){
            },
            error: function(jqXHR,textStatus,errorThrown){
                console.log(errorThrown);
            }
        });
        setTimeout(function() {
          relayOff('<?= @$modelGate->relay->status_close ?>');
        }, <?= @$modelGate->relay->delay?>);
    }
    
    function relayClose(code){
        var close_code = parseInt(code, 10)-1;
        if(code=='01'||code=='03'||code=='05'||code=='07'||code=='09'){
            relay_close = "0"+close_code;
        }else{
            relay_close = close_code;
        }
        $.ajax({
            url: "http://<?= @$modelGate->relay->ip ?>/<?= @$modelGate->relay->port ?>/"+relay_close,
            data: {},
            method : 'GET',
            success: function(result){
            },
            error: function(jqXHR,textStatus,errorThrown){
                console.log(errorThrown);
            }
        });
    }
    function relayOff(code){
        var close_code = parseInt(code, 10)-10;
        relay_close = "0"+close_code;
        $.ajax({
             url: "http://<?= @$modelGate->relay->ip ?>/1234/6/?"+relay_close+"&",
            data: {},
            method : 'GET',
            success: function(result){
            },
            error: function(jqXHR,textStatus,errorThrown){
                console.log(errorThrown);
            }
        });
    }
</script>
<?php
$this->registerJs(<<<JS
    var card_id,gate_id = {$modelGate->gate_id};
    localStorage.clear();
    var socket = io('http://' + window.location.hostname + ':3000');
    if(gate_id==1){
        socket.on('request_G1', function (data) {
            if(localStorage.CardID===undefined){
                localStorage.CardID = '000'+data.source;
                card_id = localStorage.CardID;
                console.log(card_id);
                Card.data();
            }
        });
    }else if(gate_id==2){
        socket.on('request_G2', function (data) {
            if(localStorage.CardID===undefined){
                localStorage.CardID = '000'+data.source;
                card_id = localStorage.CardID;
                console.log(card_id);
                Card.data();
            }
        });
    }else if(gate_id==3){
        socket.on('request_G3', function (data) {
            if(localStorage.CardID===undefined){
                localStorage.CardID = '000'+data.source;
                card_id = localStorage.CardID;
                console.log(card_id);
                Card.data();
            }
        });
    }
    $('#scancard').on('keyup',function(e){
        e.preventDefault();
        var cardID = $(this).val();
        if(e.keyCode!=13){
            if(cardID.length==10){
                card_id = cardID;
                Card.data();
            }
        }
    });
    $("#scancard").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errmsg").html("*ตัวเลขเท่านั้น").show().fadeOut("slow");
            return false;
        }
    });

    $('#ajaxCrudModal').on('show.bs.modal', function (e) {
        $('#cam_display').css('z-index','-1');
    });
    $('#ajaxCrudModal').on('hide.bs.modal', function (e) {
        $('#cam_display').css('z-index','1');
        complete();
    });
    
    function complete(clear=''){
        Loading.hide();
        setTimeout(function() {
          $("#scancard").focus();
        }, 500);
        $('#scancard').val('');
        if(!clear){
            setTimeout(function() {
                localStorage.clear(); 
            }, 3000);
        }
    }
    Loading = {
        class : function(){
            $('.page-content').waitMe({
                effect : 'orbit',
                text: 'กำลังโหลดข้อมูล...',
                bg : 'rgba(255,255,255,0.7)',
                color : '#000',
                maxSize : '',
                textPos : 'vertical',
                fontSize : '18px',
                source : ''
            });
        },
        hide : function(){
            $('.page-content').waitMe('hide');
        },
        reload : function(){
            setTimeout(function() {
              location.reload();
            }, 2000); 
        }
    };
    Notify = {
        success : function(massage){
            AppNotify.Show('success','fa fa-check','Success!',massage);
        },
        warning : function(massage){
            AppNotify.Show('warning','fa fa-info-circle','Warning!',massage);
        },
        error : function(massage){
            AppNotify.Show('danger','fa fa-remove','Error!',massage);
        },
    }
    Card = {
        data : function(){
            var self = this;
            Loading.class();
            $.ajax({
                url: "data",
                type : "GET",
                data : {card_id: card_id},
                success: function(result){
                    if(result) {
                        self.expdate();
                    }else{
                        Notify.warning('ไม่พบข้อมูลบัตร');
                        complete();
                    }
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        },
        expdate : function(){
            var self = this;
            $.ajax({
                url: "expdate",
                type : "GET",
                data : {card_id: card_id},
                success: function(result){
                    if(result) {
                        self.policy();
                    }else{
                        Notify.warning('บัตรหมดอายุ');
                        complete();
                    }
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        },
        policy : function(){
            var self = this;
            $.ajax({
                url: "policy",
                type : "GET",
                data : {card_id: card_id,gate_id: gate_id},
                success: function(result){
                    if(result) {
                        if(gate_id==1){
                            self.state();
                        }else if(gate_id==2){
                            capturePic(card_id,gate_id);
                        }else if(gate_id==3){
                            self.park();
                        }
                    }else{
                        Notify.warning('ไม่อนุญาติให้ผ่าน');
                        complete();
                    }
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        },
        park : function(){
            var self = this;
            $.ajax({
                url: "park",
                type : "GET",
                data : {card_id: card_id,gate_id: gate_id},
                success: function(result){
                    if(result) {
                        capturePic(card_id,gate_id); 
                    }else{
                        Notify.warning('ไม่มีข้อมูลการเข้าจอด');
                        complete();
                    }
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        },
        state : function(){
            var self = this;
            $.ajax({
                url: "state",
                type : "GET",
                data : {card_id: card_id,gate_id: gate_id},
                success: function(result){
                    if(result){
                       capturePic(card_id,gate_id);
                    }else{
                        Notify.warning('บัตรอยู่ในสถานะเข้าจอด');
                        complete();
                    }
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        }
    }
    Modal = {
        show : function(result){
            $('#ajaxCrudModal .modal-header').html('<h4 class="modal-title">'+result.title+'</h4>');
            $('#ajaxCrudModal .modal-body').html(result.content);
            $('#ajaxCrudModal').modal('show');
            Loading.hide();
            //complete(true);
        },
        hide : function(img_ref){
            $('#ajaxCrudModal').modal('hide');
            Save.auto(img_ref);
        }
    }
    Save = {
        auto : function(img_ref){
            var self = this;
            $.ajax({
                url: "auto-save",
                type : "POST",
                data : {card_id: card_id,gate_id:gate_id,img_ref:img_ref},
                success: function(result){
                    self.success();
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        },
        checkbill : function(trans_id,img_ref){
            var self = this;
            $.ajax({
                url: "check-bill",
                type : "POST",
                data : {trans_id: trans_id,img_ref:img_ref},
                success: function(result){
                    self.success();
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        },
        success : function(){
            Notify.success('บันทึกข้อมูลเรียบร้อย');
            barrierOpen();
            //ledOn();
            dtFunction.reloadData('#dt_trans');
            complete();
        }
    }
    Display = {
        condition : function(img_ref){
            $.ajax({
                url: "condition",
                type : "GET",
                data : {card_id: card_id,gate_id: gate_id,img_ref:img_ref},
                success: function(result){
                    if(result.type=='visitor'){
                        Modal.show(result);
                    }else if(result.type=='memberOntime'){
                        if('{$modelConfig->member_modaldisplay}'==1){
                            if('{$modelConfig->member_modalautosave}'==1){
                               Modal.show(result);
                               setTimeout(function(){  Modal.hide(img_ref); }, {$modelConfig->member_modaltime}+'000');
                            }else{
                                Save.auto(img_ref);
                            }
                        }else{
                            Save.auto(img_ref);
                        }
                    }else if(result.type=='memberOuttime'){
                        if('{$modelConfig->member_modaldisplay}'==1){
                            Modal.show(result);
                        }else{
                            Save.auto(img_ref);
                        }
                    }
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        },
        bill : function(img_ref){
            $.ajax({
                url: "bill",
                type : "GET",
                data : {card_id: card_id,gate_id: gate_id,img_ref:img_ref},
                success: function(result){
                    if(result.type=='visitor'){
                        Modal.show(result);
                    }else if(result.type=='member'){
                        if('{$modelConfig->member_charge}'==1){
                            Modal.show(result);
                        }else{
                            Save.checkbill(result.trans_id,img_ref);
                        }
                    }else if(result.type=='auto'){
                        Save.checkbill(result.trans_id,img_ref);
                    }
                },error: function (xhr, status, error) {
                    Notify.error(error);
                    Loading.reload();
                } 
            });
        }
    }

//-----------------------------------------------------------------------------------------
dtFunction = {
    initComplete: function (api,columns,el) {
        var self = this;
        $('th').css( "vertical-align", "middle" ),
        // $.each(columns, function( index, value ) {
        //     api.columns(value).every( function () {
        //         var column = this;
        //         $('<p></p>').appendTo($(column.header()));
        //         var select = $('<select style="width: 100%;" class="select2"><option value="">All</option></select>')
        //             .appendTo( $(column.header()) )
        //             .on( 'change', function () {
        //                 var val = $.fn.dataTable.util.escapeRegex(
        //                     $(this).val()
        //                 );
        //                 column.search( val ? '^'+val+'$' : '', true, false ).draw();
        //             } );

        //         column.data().unique().sort().each( function ( d, j ) {
        //             select.append( '<option value="'+d+'">'+d+'</option>' )
        //         } );
        //     } );
        // });
        //self.initSelect2();
        self.hideWaitme(el);
    },
    initSelect2: function(){
        $('select.select2').select2({"allowClear":true,"theme":"bootstrap","width":"100%","placeholder":"All","language":"en-US"});
    },
    initWaitme: function(el){
        $(el).waitMe({
            effect : 'roundBounce',
            text : 'กำลังโหลดข้อมูล...',
            bg : 'rgba(255,255,255,0.7)',
            color : '#000',
            waitTime : -1,
            textPos : 'vertical',
            fontSize : '18px',
        });
    },
    hideWaitme: function(el){
        $(el).waitMe('hide');
    },
    reloadData: function(el){
        var self = this;
        var table = $(el).DataTable();
        table.ajax.reload();
    },
};
var language = {
    "processing": "Processing...",
    "loadingRecords": "Loading...",
    "sProcessing": "กำลังดำเนินการ...",
    "sLengthMenu": "แสดง _MENU_ แถว",
    "sZeroRecords": "ไม่พบข้อมูล",
    "sInfo": "แสดง _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ",
    "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
    "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
    "sInfoPostFix": "",
    "sUrl": "",
    "oPaginate": {
        "sFirst":    "หน้าแรก",
        "sPrevious": "ก่อนหน้า",
        "sNext":     "ถัดไป",
        "sLast":     "หน้าสุดท้าย"
    }
};

var defaultOptions = {
    "dom": '<"pull-left"f><"pull-right"l>t<"pull-left"i>p',
    "order" : false,
    "deferRender": true,
    "processing": true,
    "responsive": true,
    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
};

dtFunction.initWaitme('#data_trans');
$('#dt_trans').DataTable($.extend({},defaultOptions,{
    "ajax": "data-gate?gate_id="+gate_id,
    "pageLength": 10,
    "language": $.extend({}, language, {"search": "ค้นหา : _INPUT_ "}),
    "columns": [
        {   
            "data": "card_id",
            "title": "หมายเลขบัตร",
            "className": 'dt-nowrap dt-center',
            "bSortable": false
        },
        {   
            "data": "card_owner_name",
            "title": "ผู้ถือบัตร",
            "className": 'dt-nowrap dt-head-center',
            "bSortable": false },
        {   
            "data": "licenceplate_no",
            "title": "ทะเบียนรถ",
            "className": 'dt-nowrap dt-head-center',
            "bSortable": false
        },
        {   
            "data": "card_type_name",
            "title": "ประเภทบัตร",
            "className": 'dt-nowrap dt-head-center',
            "bSortable": false 
        },
        {   
            "data": "gate1_datetime",
            "title": "เวลาเข้าประตู1",
            "className": 'dt-nowrap dt-center',
            "bSortable": false 
        },
        {   
            "data": "gate2_datetime",
            "title": "เวลาเข้าประตู2",
            "className": 'dt-nowrap dt-center',
            "bSortable": false 
        },
        {   
            "data": "gate3_datetime",
            "title": "เวลาออกประตู1",
            "className": 'dt-nowrap dt-center',
            "bSortable": false 
        },
        {   
            "data": "trans_status",
            "title": "สถานะ",
            "className": 'dt-nowrap dt-center',
            "bSortable": false 
        },
        {   
            "data": "action",
            "title": "ดำเนินการ",
            "className": 'dt-nowrap dt-center',
            "bSortable": false
        }
        
    ],
    "columnDefs": [
        {
            "targets": [5],
            "visible": (gate_id==2||gate_id==3)?true:false,
        },
        {
            "targets": [6],
            "visible": (gate_id==3)?true:false,
        }
    ],
    "initComplete": function () { dtFunction.initComplete(this.api(),[],'#data_trans')  },
    // "fnRowCallback": function( row, data, index ) {
    //     var api = this.api();
    //     dtFunction.fnRowCallback(api, row, data, index, '#count-tab1');
    //     if(data.SRStatus === 'กำลังดำเนินการ'){
    //         $(row).addClass( "danger" );
    //     }
    // },
}));    
JS
);?>
<script type="text/javascript">
    function btnDelete(id,gate_id, el){
       swal({
          title: 'ยืนยันคำสั่ง?',
          text: '',
          type: 'error',
          showCancelButton: true,
          confirmButtonText: 'ยืนยัน',
          cancelButtonText: 'ยกเลิก'
        }).then((result) => {
            if (result) {
                $.ajax({
                    url: "delete",
                    type : "GET",
                    data : {id: id,gate_id:gate_id},
                    success: function(result){
                        dtFunction.reloadData(el);
                    },error: function (xhr, status, error) {
                        Notify.error(error);
                        Loading.reload();
                    } 
                });
            }
        })
}
</script>   