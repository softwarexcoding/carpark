<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use msoft\helpers\RegisterJS;

RegisterJS::regis(['ajaxcrud'],$this);
?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
])?>
<?php Modal::end(); ?>