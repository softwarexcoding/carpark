<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\gate\models\TbTransaction */

$this->title = 'Update Tb Transaction: ' . $model->trans_id;
$this->params['breadcrumbs'][] = ['label' => 'Tb Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->trans_id, 'url' => ['view', 'id' => $model->trans_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tb-transaction-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
