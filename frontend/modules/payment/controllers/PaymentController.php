<?php

namespace frontend\modules\payment\controllers;

use Yii;
use frontend\modules\payment\models\TbReceipt;
use frontend\modules\payment\models\TbInvoice;
use frontend\modules\payment\models\TbShift;
use frontend\modules\payment\models\TbConfigReport;
use frontend\modules\payment\models\TbReceiptSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use msoft\helpers\Html;
use yii\filters\AccessControl;
use yii\db\Query;
use msoft\widgets\Icon;
use msoft\mpdf\Pdf;
use yii\helpers\Url;
use yii\helpers\Json;
/**
 * PaymentController implements the CRUD actions for TbReceipt model.
 */
class PaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TbReceipt models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }
    public function actionQuery(){
        $query = (new Query())
        ->select([
            'tb_receipt.*',
            'tb_transaction2.gate3_datetime',
            'tb_transaction2.licenceplate_no',
        ])
        ->from('tb_receipt')
        ->leftJoin('tb_transaction2','tb_transaction2.trans_id = tb_receipt.trans_id')
        ->WHERE('isnull(tb_receipt.inv_num)')
        ->all();
        $dataArray = [];
        foreach ($query as $key => $value) {
            $column = [];
            $column['order'] = $key+1;
            $column['rec_num'] = $value['rec_num'];
            $column['licenceplate_no'] = isset($value['licenceplate_no'])?$value['licenceplate_no']: '-';
            $column['gate3_datetime'] = isset($value['gate3_datetime'])?\metronic\components\DateConvert::convertToLogicalDateOnly($value['gate3_datetime']):'-';
            $column['fee_amt'] = $value['fee_amt'];
            $column['disc_amt'] = $value['disc_amt'];
            $column['cardloss_amt'] = $value['cardloss_amt'];
            $column['total_paid'] = $value['total_paid'];
            $column['checkbox'] = Html::checkbox('selection[]',false,['class'=>'kv-row-checkbox','value' => $value['rec_num']]);
            $dataArray[] = $column;
        }

        echo \yii\helpers\Json::encode(['data' => $dataArray]);
    }

    public function actionHistory()
    {
        
        return $this->render('history', [
        ]);
    }
    public function actionQueryHistory(){
        $query = (new Query())
        ->select([
            'tb_invoice.inv_num',
            'tb_invoice.inv_date',
            'user.username AS createby',
            'SUM(tb_receipt.fee_amt) AS fee_amt',
            'SUM(tb_receipt.disc_amt) AS disc_amt',
            'SUM(tb_receipt.cardloss_amt) AS cardloss_amt',
            'SUM(tb_receipt.total_paid) AS total_paid',
        ])
        ->from('tb_invoice')
        ->leftJoin('tb_receipt','tb_invoice.inv_num = tb_receipt.inv_num')
        ->leftjoin('user','tb_invoice.createby = user.id')
        ->groupby('tb_invoice.inv_num')
        ->all();
        $dataArray = [];
        foreach ($query as $key => $value) {
            $column = [];
            $column['order'] = $key+1;
            $column['inv_num'] = $value['inv_num'];
            $column['createby'] = $value['createby'];
            $column['inv_date'] = \metronic\components\DateConvert::mysql2phpDateTime($value['inv_date']);
            $column['fee_amt'] = $value['fee_amt'];
            $column['disc_amt'] = $value['disc_amt'];
            $column['cardloss_amt'] = $value['cardloss_amt'];
            $column['total_paid'] = $value['total_paid'];
            $column['action'] = 
            Html::a(Icon::show('print', [], Icon::BSG),['/payment/payment/print','id' => $value['inv_num']],['class' => 'btn btn-xs btn-primary tooltips','data-pjax' => 0, 'target' => '_blank']).
            Html::aDelete(Icon::show('trash', [], Icon::BSG),false,[
                'data-url' => \yii\helpers\Url::to(['/payment/payment/delete-invoice','id' => $value['inv_num']]),
                'pjaxreload' => '#history',
                'class' => 'btn btn-xs btn-danger tooltips'
            ]);
            $dataArray[] = $column;
        }
        echo \yii\helpers\Json::encode(['data' => $dataArray]);
    }

    /**
     * Displays a single TbReceipt model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "TbReceipt #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TbReceipt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TbReceipt();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new TbReceipt",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new TbReceipt",
                    'content'=>'<span class="text-success">Create TbReceipt success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new TbReceipt",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->rec_num]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing TbReceipt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update TbReceipt #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "TbReceipt #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update TbReceipt #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->rec_num]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing TbReceipt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        \Yii::$app->getSession()->setFlash('success', 'Deleted!');
        return $this->redirect(['index']);
    }
     public function actionDeleteInvoice($id)
    {
        $model =  $this->findModelInvoice($id);
        $receipt = TbReceipt::findAll(['inv_num' => $id]);
        foreach ($receipt as $value) {
            $customer = TbReceipt::findOne($value->rec_num);
            $customer->inv_num = null;
            $customer->save();
        }
        $model->delete();
        \Yii::$app->getSession()->setFlash('success', 'Deleted!');
        return $this->redirect(['history']);
    }

    /**
     * Finds the TbReceipt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbReceipt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbReceipt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModelInvoice($id)
    {
        if (($model = TbInvoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionPrintPayment() {
        
        $request = Yii::$app->request;
        if($request->isGet){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $title = 'บันทึกการนำส่งเงินสด';
            return [
                    'title'=> $title ,
                    'content'=>$this->renderAjax('_modal_report', [
                    ]),
                    'footer'=> ''
                    //Html::button('Close',['class'=>'btn btn-default pull-rigth','data-dismiss'=>"modal"]).Html::button(Icon::show('save').'บันทึกนำส่งเงินสด', ['type' => 'button',  'class' => 'btn btn-success ', 'id' => 'save-payment','target' => '_blank'])
                ];    
        }else{
            return $this->render('_modal_report', [
                 'id' => $keys,
            ]);
        }
    }
    public function actionSavepayment()
    {
        $request = Yii::$app->request;
        $model = new TbInvoice();
        $model->inv_date = date('Y-m-d H:i:s');
        $model->createby = Yii::$app->user->getId();
        $model->shift_id = $request->post('shift_id');
        $model->inv_status = '2';
        if($model->save()){
            TbReceipt::updateAll(['inv_num' => $model->inv_num], ['IN', 'rec_num', $request->post('keys')]);
            //return $this->redirect('findidprint');
            return Json::encode(Url::to(['/payment/payment/print-slip','invid' => $model->inv_num]));
        }else{
            return false; 
        }
        
    }
    public function actionFindidprint(){
        $inv_num = TbInvoice::find()->max('inv_num');
        return $this->redirect(['print', 'id' => $inv_num]);
    }
    public function actionPrint($id){
        $header = 'รายงานนำส่งเงิน';
        $subheader = 'ค่าบริการจอดรถยนต์';
        $sql = "SELECT
                tb_invoice.shift_id AS shift_id,
                tb_invoice.inv_num AS inv_num,
                tb_invoice.inv_date AS inv_date,
                Sum(tb_receipt.fee_amt) AS fee_amt,
                Sum(tb_receipt.disc_amt) AS disc_amt,
                Sum(tb_receipt.cardloss_amt) AS cardloss_amt,
                Sum(tb_receipt.total_paid) AS total_paid,
                tb_invoice.createby ,
                user.username AS createby
                FROM
                tb_invoice
                LEFT JOIN tb_receipt ON tb_invoice.inv_num = tb_receipt.inv_num
                LEFT JOIN user ON tb_invoice.createby = user.id
                WHERE
                tb_invoice.inv_num = $id
                GROUP BY tb_invoice.inv_num";
        $sub_sql = Yii::$app->db->createCommand($sql)->queryAll();
        $pdf = new Pdf(['mode' => Pdf::MODE_UTF8,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => $header . '.pdf',
            'format' => [80, 160], #60
            'content' => $this->renderPartial('slip', [
                'query' => $sub_sql[0],
                'id' => '1',
                'type' => 'content',
                'header'=>$header,
                'subheader' =>$subheader,
            ]),
            'marginTop' => '35',
            //'marginBottom' => '30',
            'marginLeft' => '5',
            'marginRight' => '5',
            'marginFooter' => '5',
            'marginHeader' => '5',
            'options' => [
                'defaultheaderline' => 0,
                'defaultfooterline' => 0,
                'title' => $header . '.pdf',

            ],
            'methods' => [
                'SetHeader' => $this->renderPartial('slip', [
                    'query' => $sub_sql[0],
                    'id' => '1',
                    'type' => 'header',
                    'header'=>$header,
                    'subheader' =>$subheader,
                ]),
                'SetFooter' => $this->renderPartial('slip', [
                    'query' => $sub_sql[0],
                    'id' => '1',
                    'type' => 'footer',
                    'header'=>$header,
                    'subheader' =>$subheader,
                ]),
        ]]);

        return $pdf->render();
    }

    public function actionPrintSlip($invid){
        $rows = (new \yii\db\Query())
        ->select([
            'tb_invoice.*', 
            '(SELECT DATE_FORMAT(DATE_ADD(tb_invoice.inv_date, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\')) as inv_date',
            'Sum( tb_receipt.fee_amt ) AS fee_amt',
            'Sum( tb_receipt.disc_amt ) AS disc_amt',
            'Sum( tb_receipt.cardloss_amt ) AS cardloss_amt',
            'Sum( tb_receipt.total_paid ) AS total_paid',
            'tb_invoice.createby',
            '`user`.username AS user_createby'
        ])
        ->from('tb_invoice')
        ->leftJoin('tb_receipt','tb_invoice.inv_num = tb_receipt.inv_num')
        ->leftJoin('`user`','tb_invoice.createby = `user`.id')
        ->where(['tb_invoice.inv_num' => $invid])
        ->groupBy('tb_invoice.inv_num')
        ->one();
        return $this->renderAjax('slip_report',[
            'rows' => $rows
        ]);
    }

}
