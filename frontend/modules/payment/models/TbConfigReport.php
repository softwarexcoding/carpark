<?php

namespace frontend\modules\payment\models;

use Yii;

/**
 * This is the model class for table "tb_config_report".
 *
 * @property int $id
 * @property int $gate13_timediff_hr เกณฑ์เวลา ค้างคืน(ชั่วโมง)
 * @property int $gate12_timediff_min เกณฑ์เวลา จอดไม่ตรงตามพื้นที่(นาที)
 * @property string $parking_name ชื่อลานจอด
 * @property string $company_name ชื่อหน่วยงาน
 * @property string $rec_comment หมายเหตุ-ข้อความ
 * @property int $parkinglotqty_visitor จำนวนที่จอดรถผู้รับบริการ
 * @property int $parkinglotqty_member จำนวนที่จอดรถเจ้าหน้าที่
 */
class TbConfigReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_config_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'gate13_timediff_hr', 'gate12_timediff_min', 'parkinglotqty_visitor', 'parkinglotqty_member'], 'integer'],
            [['parking_name', 'company_name'], 'string', 'max' => 100],
            [['rec_comment'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gate13_timediff_hr' => 'เกณฑ์เวลา ค้างคืน(ชั่วโมง)',
            'gate12_timediff_min' => 'เกณฑ์เวลา จอดไม่ตรงตามพื้นที่(นาที)',
            'parking_name' => 'ชื่อลานจอด',
            'company_name' => 'ชื่อหน่วยงาน',
            'rec_comment' => 'หมายเหตุ-ข้อความ',
            'parkinglotqty_visitor' => 'จำนวนที่จอดรถผู้รับบริการ',
            'parkinglotqty_member' => 'จำนวนที่จอดรถเจ้าหน้าที่',
        ];
    }
}
