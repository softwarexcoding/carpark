<?php

namespace frontend\modules\payment\models;

use Yii;

/**
 * This is the model class for table "tb_disc_section".
 *
 * @property int $disc_sec_id
 * @property string $disc_sec_name
 */
class TbDiscSection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_disc_section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['disc_sec_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'disc_sec_id' => 'Disc Sec ID',
            'disc_sec_name' => 'Disc Sec Name',
        ];
    }
}
