<?php

namespace frontend\modules\payment\models;

use Yii;

/**
 * This is the model class for table "tb_invoice".
 *
 * @property int $inv_num
 * @property string $inv_date วันที่เวลา
 * @property int $createby ผู้ดำเนินการ(user)
 * @property int $inv_status
 */
class TbInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inv_date'], 'safe'],
            [['createby', 'inv_status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inv_num' => 'Inv Num',
            'inv_date' => 'วันที่เวลา',
            'createby' => 'ผู้ดำเนินการ(user)',
            'inv_status' => 'Inv Status',
        ];
    }
}
