<?php

namespace frontend\modules\payment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\payment\models\TbReceipt;

/**
 * TbReceiptSearch represents the model behind the search form about `frontend\modules\payment\models\TbReceipt`.
 */
class TbReceiptSearch extends TbReceipt
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rec_num', 'trans_id', 'inv_num', 'print_status'], 'integer'],
            [['fee_amt', 'disc_amt', 'cardloss_amt', 'total_paid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbReceipt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rec_num' => $this->rec_num,
            'trans_id' => $this->trans_id,
            'fee_amt' => $this->fee_amt,
            'disc_amt' => $this->disc_amt,
            'cardloss_amt' => $this->cardloss_amt,
            'total_paid' => $this->total_paid,
            'inv_num' => $this->inv_num,
            'print_status' => $this->print_status,
        ]);

        return $dataProvider;
    }
}
