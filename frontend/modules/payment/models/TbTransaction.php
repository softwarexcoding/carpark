<?php

namespace frontend\modules\payment\models;

use Yii;

/**
 * This is the model class for table "tb_transaction".
 *
 * @property int $trans_id รหัส
 * @property string $card_id หมายเลขบัตร
 * @property string $licenceplate_no ทะเบียนรถ
 * @property string $gate1_datetime เวลาประตู 1
 * @property string $gate2_datetime เวลาประตู 2
 * @property string $gate3_datetime เวลาประตู 3
 * @property string $total_time เวลารวม
 * @property string $total_min ส่วนต่างเวลา
 * @property string $fee_amt ค่าบริการ
 * @property int $disc_type ประเภทส่วนลด
 * @property int $disc_by ผู้ให้ส่วนลด
 * @property string $disc_amt มูลค่าส่วนลด
 * @property string $disc_time เวลาส่วนลดนาที
 * @property string $total_amt เป็นเงิน
 * @property string $total_paid จ่ายจริง
 * @property string $invoice_no หมายเลขนำส่งเงินสด
 * @property int $trans_status สถานะ
 * @property int $createby สร้างโดย
 * @property int $updateby ปรับปรุงโดย
 * @property string $createat สร้างเมื่อ
 * @property string $updateat ปรับปรุงเมื่อ
 */
class TbTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gate1_datetime', 'gate2_datetime', 'gate3_datetime', 'total_time', 'createat', 'updateat'], 'safe'],
            [['total_min', 'fee_amt', 'disc_amt', 'disc_time', 'total_amt', 'total_paid'], 'number'],
            [['disc_type', 'disc_by', 'trans_status', 'createby', 'updateby'], 'integer'],
            [['card_id'], 'string', 'max' => 100],
            [['licenceplate_no', 'invoice_no'], 'string', 'max' => 50],
            [['card_id', 'trans_status'], 'unique', 'targetAttribute' => ['card_id', 'trans_status']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trans_id' => 'รหัส',
            'card_id' => 'หมายเลขบัตร',
            'licenceplate_no' => 'ทะเบียนรถ',
            'gate1_datetime' => 'เวลาประตู 1',
            'gate2_datetime' => 'เวลาประตู 2',
            'gate3_datetime' => 'เวลาประตู 3',
            'total_time' => 'เวลารวม',
            'total_min' => 'ส่วนต่างเวลา',
            'fee_amt' => 'ค่าบริการ',
            'disc_type' => 'ประเภทส่วนลด',
            'disc_by' => 'ผู้ให้ส่วนลด',
            'disc_amt' => 'มูลค่าส่วนลด',
            'disc_time' => 'เวลาส่วนลดนาที',
            'total_amt' => 'เป็นเงิน',
            'total_paid' => 'จ่ายจริง',
            'invoice_no' => 'หมายเลขนำส่งเงินสด',
            'trans_status' => 'สถานะ',
            'createby' => 'สร้างโดย',
            'updateby' => 'ปรับปรุงโดย',
            'createat' => 'สร้างเมื่อ',
            'updateat' => 'ปรับปรุงเมื่อ',
        ];
    }
}
