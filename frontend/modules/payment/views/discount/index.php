<?php
use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;
use metronic\widgets\portlet\PortletBox;
use yii\helpers\ArrayHelper;
use msoft\widgets\Select2;
use yii\helpers\Json;
use yii\widgets\Pjax;
use frontend\modules\payment\models\TbDiscSection;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payment\models\TbTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
RegisterJS::regis(['sweetalert'],$this);

$this->title = 'บันทึกส่วนลด';
$this->params['breadcrumbs'][] = $this->title;
$classmidle = ['class' => 'kv-align-center kv-align-middle','style' => 'color:black'];
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
');
use yii\data\ArrayDataProvider;
$provider = new ArrayDataProvider([
    'allModels' => [],
    'pagination' => [
        'pageSize' => 100,
    ],
]);
?>
<?= SwalAlert::widget(); ?>
<?= PortletBox::begin([
    'title' => '<span class="caption-subject bold uppercase">' . $this->title . '</span>',
    'icon' => 'fa fa-cube font-dark',
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'tools' => [
    ],
    'actions' => [
       
    ]
]);
?>
<div class="tb-transaction-index">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="well">
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon" style="background-color:#f1f4f7";>แผนก :</span>
                            <?php if(!empty(Yii::$app->user->identity->profile->section_id)){ ?>
                                <?= Select2::widget([
                                    'name' => 'discsection',
                                    'data' => ArrayHelper::map(TbDiscSection::find()->where(['section_id' => Yii::$app->user->identity->profile->section_id])->all(), 'disc_sec_id', 'disc_sec_name'),
                                    'id' => 'discsection',
                                    'options' => [
                                        //'placeholder' => 'เลือกแผนก ...',
                                        'value' => ArrayHelper::map(TbDiscSection::find()->where(['section_id' => Yii::$app->user->identity->profile->section_id])->all(), 'disc_sec_id', 'disc_sec_name'),
                                        'multiple' => false
                                    ],
                                ])?>
                            <?php }else{ ?>
                                <?= Select2::widget([
                                    'name' => 'discsection',
                                    'data' => ArrayHelper::map(TbDiscSection::find()->all(), 'disc_sec_id', 'disc_sec_name'),
                                    'id' => 'discsection',
                                    'options' => [
                                        'placeholder' => 'เลือกแผนก ...',

                                        'multiple' => false
                                    ],
                                ])?>
                            <?php } ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon" style="background-color:#f1f4f7";>หมายเลขบัตร :</span>
                            <?= Html::input('text', 'scancard', '', [
                                'type' => 'text', 
                                'maxlength'=>'10', 
                                'id' => 'scancard', 
                                'value' => '', 
                                'class' => 'form-control', 
                                'autofocus' => 'autofocus', 
                                'placeholder' => 'Scan CardID'
                            ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::begin([ 'timeout' => 5000, 'id'=> 'index']) ?>
    <?= Datatables::widget([
    'dataProvider' => $provider,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'afterHeader' => [
        [
            'columns' => [
                ['content' => '','options' => ['class' => 'skip-export','colspan' => 4]],
                ['content' => '','options' => ['class' => 'skip-export',]],
                ['content' => '','options' => ['class' => 'skip-export',]],
                ['content' => '','options' => ['class' => 'skip-export',]],
            ]
        ]
    ],
    'tableOptions' => ['id' => 'discount'],
    'hover' => true,
    'bordered' => false,
    'condensed' => true,
    'striped' => true,
    'responsive' => false,
    'layout' => '{items}',
    'export' => [
        'label' => 'รายงาน',
        'target' => '_blank',
        'showConfirmAlert' => false
    ],
    'exportConfig' => [
        GridView::EXCEL => [
            'label' => Yii::t('app', 'Excel'),
            'icon' => 'file-excel-o',
            'iconOptions' => ['class' => 'text-success'],
            'showHeader' => true,
            'showPageSummary' => true,
            'showFooter' => true,
            'showCaption' => true,
            'filename' => Yii::t('app', 'grid-export'),
            'alertMsg' => Yii::t('app', 'The EXCEL export file will be generated for download.'),
            'options' => ['title' => Yii::t('app', 'Microsoft Excel 95+')],
            'mime' => 'application/vnd.ms-excel',
            'config' => [
                'worksheet' => Yii::t('app', 'ExportWorksheet'),
                'cssFile' => '',
            ],
        ],
        //GridView::CSV => ['label' => 'Save as CSV'],
        GridView::PDF => [
            
            'label' => Yii::t('app', 'PDF'),
            'icon' => 'file-pdf-o',
            'iconOptions' => ['class' => 'text-danger'],
            'showHeader' => true,
            'showPageSummary' => true,
            'showFooter' => true,
            'showCaption' => true,
            'filename' => Yii::t('app', 'grid-export'),
            'alertMsg' => Yii::t('app', 'The PDF export file will be generated for download.'),
            'options' => [
                'title' => Yii::t('app', 'Portable Document Format'),
            ],
            'mime' => 'application/pdf',
            'config' => [
                'mode' => 'UTF-8',
                'format' => 'A4',
                'destination' => 'I',
                'marginTop' => 20,
                'marginBottom' => 20,
                'cssInline' => 'td{font-size:22pt;}'.
                    'th{font-size:24pt;text-align:center;}',
                'methods' => [
                    'SetHeader' => '<table width="100%">
                                        <tr>
                                            <td class="text-right" width="50%" style="font-size:18pt;">
                                                สถานะการจอดรถข้ามคืน
                                            </td>
                                        </tr>
                                    </table>',
                    'SetFooter' => '<table width="100%">
                                        <tr>
                                            <td class="text-left" width="50%" style="font-size:13pt;">
                                                ระบบจัดการลานจอดรถยนต์ โรงพยาบาลราชวิถี อาคารเฉลิมพระเกียรติ
                                            </td>
                                            <td class="text-right" width="50%" style="font-size:13pt;">
                                            ' . 'Print:' .date('d/m/Y').'
                                            </td>
                                        </tr>
                                    </table>',
                ],
                'options' => [
                    'title' => 'สถานะการจอดรถข้ามคืน.PDF',
                    'defaultheaderline' => 0,
                    'defaultfooterline' => 0,
                ],
                'contentBefore' => '',
                'contentAfter' => '',
            ],
        ],
    ],
    'clientOptions' => [
        "data" => new \yii\web\JsExpression(Json::encode($query)),
        "lengthMenu" => [[5,10,15,20,-1],[5,10,15,20,"All"]],
        "pageLength" => 20,
        "processing" => true,
        "columns" => [
            [ "data" => "id" ],
            [ "data" => "card_id" ],
            [ "data" => "licenceplate_no" ],
            [ "data" => "card_owner_name" ],
            [ "data" => "section_name" ],
            [ "data" => "card_type_name" ],
            [ "data" => "disc_by" ],
        ],
        "columnDefs" => [
            [ "sClass" => "text-center", "targets" => [0,1,2,4,6] ],
            [ "sClass" => "dt-nowrap", "targets" => [] ]
        ],
        "initComplete" => new \yii\web\JsExpression("function (settings) {
            initSt2(this,\"select71\",4,\"หน่วยงาน\");
            initSt2(this,\"select72\",5,\"ประเภท\");

            var t = $('#discount').DataTable();
            t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();
        }")
    ],
    'columns' => [
        [
            'class' => 'msoft\widgets\grid\SerialColumn',
            'width' => '5%',
        ],
        
        [
            'header' => 'หมายเลขบัตร',
            'headerOptions' => $classmidle,
            'hAlign' => 'center',
            'width' => '10%',
        ],
        [
            'header' => 'ทะเบียนรถ',
            'headerOptions' => $classmidle,
            'hAlign' => 'center',
            'width' => '10%',
        ],
        [
            'header' => 'ชื่อผู้ถือบัตร',
            'headerOptions' => $classmidle,
            'hAlign' => 'center',
            'width' => '20%',
        ],
        [
            'header' => 'หน่วยงาน',
            'headerOptions' => $classmidle,
            'hAlign' => 'center',
            'width' => '10%',
        ],
        [
            'header' => 'ประเภทบัตร',
            'headerOptions' => $classmidle,
            'hAlign' => 'center',
            'width' => '20%',
        ],
        [
            'header' => 'แผนก',
            'headerOptions' => $classmidle,
            'hAlign' => 'center',
            'width' => '15%',
        ],
    ],
]); ?>
<?php Pjax::end() ?>
</div>
<?= PortletBox::end(); ?>

<?php echo $this->render('modal'); ?>
<?php 
$this->registerJsFile(
    '@web/js/dt-s2.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJs(<<<JS
    $('#scancard').on('keyup',function(e){
        e.preventDefault();
        var cardID = $(this).val();
        if(e.keyCode!=13){
            if(cardID.length==10){
                card_id = cardID;
                Card.data(e);
            }
        }
      
    });
    $("#scancard").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errmsg").html("*ตัวเลขเท่านั้น").show().fadeOut("slow");
            return false;
        }
    });

    $('#ajaxCrudModal').on('show.bs.modal', function (e) {
        $('#cam_display').css('z-index','-1');
    });
    $('#ajaxCrudModal').on('hide.bs.modal', function (e) {
        $('#cam_display').css('z-index','1');
        localStorage.clear();
        complete();
    });

    function complete(){
        Loading.hide();
        setTimeout(function() {
          $("#scancard").focus();
        }, 500);
        $('#scancard').val('');
    }
    Loading = {
        class : function(){
            $('.page-content').waitMe({
                effect : 'orbit',
                text: 'กำลังโหลดข้อมูล...',
                bg : 'rgba(255,255,255,0.7)',
                color : '#000',
                maxSize : '',
                textPos : 'vertical',
                fontSize : '18px',
                source : ''
            });
        },
        hide : function(){
            $('.page-content').waitMe('hide');
        }
    };
    Notify = {
        success : function(massage){
            AppNotify.Show('success','fa fa-check','Success!',massage);
        },
        warning : function(massage){
            AppNotify.Show('warning','fa fa-info-circle','Warning!',massage);
        },
        error : function(massage){
            AppNotify.Show('danger','fa fa-remove','Error!',massage);
        },
    }
    Card = {
        data : function(e){
            $('html').bind('keypress', function(e)
                {
                if(e.keyCode == 13)
                    {
                    return false;
                    }
                });
            var discby = $('#discsection').val();
            var self = this;
            if( discby == ''){
                Notify.warning('แผนกว่าง');
                $('#scancard').val('');
            }else{
                Loading.class();
                    $.get(
                    'data',
                    {
                        card_id: card_id , disc_by : discby
                    },
                    function (result)
                    {   
                        if(result) {
                            swal({
                                title: "บันทึกส่วนลด?",
                                text: "ยืนยันการบันทึกข้อมูลส่วนลด",
                                icon: "warning",
                                buttons: true,
                            }).then((willDelete) => {
                                Loading.hide();
                                if (willDelete) {
                                    Loading.class();
                                    $.get(
                                    'data',
                                    {
                                        card_id: card_id , disc_by : discby
                                    },
                                        function (result)
                                        {   
                                            if(result) {
                                                Notify.success('บันทึกข้อมูลเรียบร้อย');
                                                localStorage.clear();
                                                $.pjax.reload({container: '#index'});
                                                complete();
                                            }else{
                                                Notify.warning('ไม่พบข้อมูลบัตร');
                                                localStorage.clear();
                                                complete();
                                            }
                                        }
                                    )
                                } 
                            });
                        }else{
                            Notify.warning('ไม่พบข้อมูลบัตร');
                            localStorage.clear();
                            complete();
                        }
                    }
                )
            }
        }
    }
JS
);
?>