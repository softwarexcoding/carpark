<?php

use yii\helpers\Html;
use msoft\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payment\models\TbTransaction */

$this->title = $model->trans_id;
$this->params['breadcrumbs'][] = ['label' => 'Tb Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-transaction-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'trans_id',
            'card_id',
            'licenceplate_no',
            'gate1_datetime',
            'gate2_datetime',
            'gate3_datetime',
            'total_time',
            'total_min',
            'fee_amt',
            'disc_type',
            'disc_by',
            'disc_amt',
            'disc_time',
            'total_amt',
            'total_paid',
            'invoice_no',
            'trans_status',
            'createby',
            'updateby',
            'createat',
            'updateat',
        ],
    ]) ?>

</div>
