<?php

use yii\helpers\Html;
use yii\helpers\Json;
use msoft\widgets\ActiveForm;
use msoft\widgets\Select2;
use msoft\widgets\DateRangePicker;
use frontend\modules\report\components\ReportQuery;
use yii\helpers\ArrayHelper;
use frontend\modules\payment\models\TbShift;
use yii\web\View;
$shift = TbShift::find()->all();
$this->registerJs('var shift = '.Json::encode(ArrayHelper::index($shift, null, 'shift_id')).' ' ,View::POS_HEAD);
?>
<style type="text/css">
.swal2-container{
    z-index : 99999;
}
</style>
<?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL,'id'=>'form_report']); ?>  
    <div class="form-group" style="margin-top: 15px;">    
        <?= Html::label('ช่วงเวลา (กะ)', 'ช่วงเวลา', ['class' => 'col-sm-2 control-label no-padding-right']) ?>
        <div class="col-sm-2">
            <?= Select2::widget([
                'name' => 'shift_id',
                'data' => ArrayHelper::map($shift, 'shift_id', 'shift_id'),
                'id' => 'shift_id',
                'options' => [
                    'placeholder' => 'เลือกช่วงเวลา ...',
                    'multiple' => false,

                ],
                 'pluginOptions' => ['allowClear' => true],
                'pluginEvents' => [
                            "change" => "function() { Project.Test($(this).val()); }",
                ],
            ])?>
        </div>
            <?= Html::label('ช่วงเวลา', 'ช่วงเวลา', ['class' => 'col-sm-2 control-label no-padding-right']) ?>
        <div class="col-sm-2">
            <?= Html::input('text', 'shift_begin','',['class' => 'form-control']) ?>
        </div>
            <?= Html::label('ช่วงเวลา', 'ช่วงเวลา', ['class' => 'col-sm-2 control-label no-padding-right']) ?>
        <div class="col-sm-2">
             <?= Html::input('text', 'shift_end','',['class' => 'form-control']) ?>
        </div>
        
        
    </div>
    <br>
    <div class="form-group" >  
        <div class="col-sm-12" style="text-align:right">
            <?= Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]) ?>
            <?= Html::submitButton('บันทึกนำส่งเงินสด',['class'=>'btn btn-success']) ?>
        </div>
    
</div>
<?php ActiveForm::end(); ?>
<?php 
$this->registerJs($this->render('./js/_form_script.js'), View::POS_READY,'body');
?>

