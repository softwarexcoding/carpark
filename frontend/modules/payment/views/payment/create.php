<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payment\models\TbReceipt */

$this->title = 'Create Tb Receipt';
$this->params['breadcrumbs'][] = ['label' => 'Tb Receipts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-receipt-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
