<?php

use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;
use metronic\widgets\portlet\PortletBox;
use yii\data\ArrayDataProvider;
use yii\grid\GridViewAsset;
use yii\widgets\Pjax;
use msoft\widgets\datatables\DataTablesAsset;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payment\models\TbReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
RegisterJS::regis(['sweetalert'],$this);
GridViewAsset::register($this);
DataTablesAsset::register($this);

$this->title = 'นำส่งเงินสด';
$this->params['breadcrumbs'][] = $this->title;
$classmidle = ['class' => 'kv-align-center kv-align-middle','style' => 'color:black'];
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
');
$provider = new ArrayDataProvider([
    'allModels' => [],
    'pagination' => [
        'pageSize' => 100,
    ],
]);
?>
<?= SwalAlert::widget(); ?>
<div class="tab-content">
<?php echo $this->render('_tab'); 
$this->registerJs('$("#tab_A").addClass("active");');
?>
<?= PortletBox::begin([
    'title' => '<span class="caption-subject bold uppercase">' . $this->title . '</span>',
    'icon' => 'fa fa-cube font-dark',
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'tools' => [
    ],
    'actions' => [
        Html::a(Icon::show('save').' นำส่งเงินสด',['print-payment'],['class' => 'btn btn-success','role' => 'modal-remote'])
    ]
]);
?>
    <?php Pjax::begin([ 'timeout' => 5000, 'id'=> 'index']) ?>
    <table id="example1" class="kv-grid-table table table-hover table-striped table-condensed kv-table-wrap dataTable no-footer dtr-inline" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>เลขที่ใบเสร็จ</th>
                <th>ทะเบียนรถ</th>
                <th>วันที่</th>
                <th>ค่าบริการ</th>
                <th>ส่วนลด</th>
                <th>ค่าปรับบัตรหาย</th>
                <th>เป็นเงิน</th>
                <th><?= Html::checkbox('checkall',false,['value' => 0])?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="2">รวมทั้งสิ้น รายการ</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
    <?php /*echo Datatables::widget([
        'id' => 'payment_datatable',
        'dataProvider' => $provider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'tableOptions' => ['id' => 'example1'],
        'hover' => true,
        'bordered' => false,
        'condensed' => true,
        'export' => false,
        'striped' => true,
        'responsive' => false,
        'layout' => '{items}',
        'clientOptions' => [
            "ajax" => 'query',
            "lengthMenu" => [[5,10,15,20,-1],[5,10,15,20,"All"]],
            "info" => true,
            "responsive" => true,
            "pageLength" => 20,
            "processing" => true,
            "columns" => [
                [ "data" => "order" ],
                [ "data" => "rec_num" ],
                [ "data" => "licenceplate_no"],
                [ "data" => "gate3_datetime" ],
                [ "data" => "fee_amt" ],
                [ "data" => "disc_amt" ],
                [ "data" => "cardloss_amt" ],
                [ "data" => "total_paid" ],
                [ "data" => "checkbox","bSortable"=>false]
            ],
            "columnDefs" => [
                [ "sClass" => "text-center", "targets" => [0,1,2,3,4,5,6,7,8] ],
                [ "sClass" => "dt-nowrap", "targets" => [] ]
            ],

            'fnRowCallback' => new \yii\web\JsExpression("function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $(nRow).attr( 'data-key' ,aData.rec_num);
                return nRow;
            } "),
            "footerCallback"=> new \yii\web\JsExpression("function (row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
                total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                $( api.column( 4 ).footer() ).html(
                    '$'+pageTotal +' ( $'+ total +' total)'
                );
            }")
        ],
        'columns' => [
            [
                'class' => 'msoft\widgets\grid\SerialColumn',
                'width' => '5%',
            ],
            [
                'header' => 'เลขที่ใบเสร็จ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'ทะเบียนรถ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'วันที่',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'ค่าบริการ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'header' => 'ส่วนลด',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'header' => 'ค่าปรับบัตรหาย',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'header' => 'เป็นเงิน',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'class' => 'msoft\widgets\grid\CheckboxColumn',
                'noWrap' => TRUE,
                'width' => '5%',
            ],
        ],
    ]); */ ?>
    <?php Pjax::end() ?>
</div>
<?= PortletBox::end(); ?>

<?php echo $this->render('modal'); ?>
<?php $this->registerJs(<<<JS
    Notify = {
        success : function(massage){
            AppNotify.Show('success','fa fa-check','Success!',massage);
        },
        warning : function(massage){
            AppNotify.Show('warning','fa fa-info-circle','Warning!',massage);
        },
        error : function(massage){
            AppNotify.Show('danger','fa fa-remove','Error!',massage);
        },
    }
    Loading = {
        class : function(){
            $('.page-content').waitMe({
                effect : 'orbit',
                text: 'กำลังโหลดข้อมูล...',
                bg : 'rgba(255,255,255,0.7)',
                color : '#000',
                maxSize : '',
                textPos : 'vertical',
                fontSize : '18px',
                source : ''
            });
        },
        hide : function(){
            $('.page-content').waitMe('hide');
        }
    };

   var example1 = $('#example1').DataTable({
    "lengthMenu":[
        [10,20,50,100,-1],[10,20,50,100,"All"],
        [5,10,15,20,-1],[5,10,15,20,"All"]
    ],
    "info":true,
    "responsive":true,
    "pageLength":-1,
    "language":{
        "search":"ค้นหา: _INPUT_ <span id=\"export-example1\"></span>",
        "sProcessing":"กำลังดำเนินการ...",
        "sLengthMenu":"แสดง _MENU_ แถว",
        "sZeroRecords":"ไม่พบข้อมูล",
        "sInfo":"แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
        "sInfoEmpty":"แสดง 0 ถึง 0 จาก 0 แถว",
        "sInfoFiltered":"(กรองข้อมูล _MAX_ ทุกแถว)",
        "sInfoPostFix":"",
        "oPaginate":{
            "sFirst":"หน้าแรก",
            "sPrevious":"ก่อนหน้า",
            "sNext":"ถัดไป",
            "sLast":"หน้าสุดท้าย"}
        },
        "ajax":"query",
        "processing":true,
        "columns":[
            {"data":"order"},
            {"data":"rec_num"},
            {"data":"licenceplate_no"},
            {"data":"gate3_datetime"},
            {"data":"fee_amt"},
            {"data":"disc_amt"},
            {"data":"cardloss_amt"},
            {"data":"total_paid"},
            {"data":"checkbox","bSortable":false}
        ],
        "columnDefs":[
            {"sClass":"text-center","targets":[0,1,2,3,4,5,6,7,8]},
            {"sClass":"dt-nowrap","targets":[]}
        ],
        "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            $(nRow).attr( 'data-key' ,aData.rec_num);
            return nRow;
        } ,
        "footerCallback":function (row, data, start, end, display ) {
            var api = this.api(), data;
            var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
            };
            $.each([ 4, 5, 6 ,7 ], function( index, value ) {
                total = api.column( value ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                pageTotal = api.column( value, { page: 'current'} ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                
                $( api.column( value ).footer() ).html(
                    total.toFixed(0)
                );
            });
            $( api.column( 0 ).footer() ).html(
                'รวมทั้งสิ้น '+api.data().count()+' รายการ'
            );
        }
    });
    jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
        return this.flatten().reduce( function ( a, b ) {
            if ( typeof a === 'string' ) {
                a = a.replace(/[^\d.-]/g, '') * 1;
            }
            if ( typeof b === 'string' ) {
                b = b.replace(/[^\d.-]/g, '') * 1;
            }
     
            return a + b;
        }, 0 );
    } );

    $('input[name="checkall"]').on('click',function(){
        
        if($(this).is(':checked') ){
            $('input[name="selection[]"]').prop('checked', true);
           
        }else{
            $('input[name="selection[]"]').prop('checked', false);
        }
    });

JS
);
?>