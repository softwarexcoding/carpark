<?php 
use frontend\modules\payment\models\TbConfigReport;
use frontend\modules\payment\models\TbShift;
use frontend\modules\payment\models\TbReceipt;
use yii\helpers\ArrayHelper;
$result = ArrayHelper::getColumn($query,'inv_num');
?>
<?php if ($type == 'header') : ?>
    <?php $name = TbConfigReport::find()->where(['id' => $id])->one();?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="font-size:18pt;text-align:center; ">
                <strong><?php echo $header; ?></strong>
            </td>
        </tr>
        <tr>
            <td style="font-size:16pt;text-align:center; ">
                <strong><?php echo $subheader; ?></strong>
            </td>
        </tr>
        <tr>
            <td style="font-size:14pt;text-align:center; "><?php echo $name->parking_name; ?></td>
        </tr>
        <tr>
            <td style="font-size:14pt;text-align:center; "><?php echo $name->company_name; ?></td>
        </tr>
        <br>
    </table>
<?php endif; ?>
<?php if ($type == 'content') : ?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <?php $shift = TbShift::find()->where(['shift_id' => $query['shift_id']])->one();?>
        <tr>
            <td style="font-size:16pt;text-align:left; " colspan="4">
                <strong>ช่วงเวลาที่ (กะ) : <?php echo $query['shift_id']; ?></strong>
            </td>
        </tr>
        <tr>
            <td style="font-size:16pt;text-align:left; " colspan="4">
                <strong>วันที่ : <?php echo \metronic\components\DateConvert::convertToLogicalDateOnly($query['inv_date']); ?></strong>
            </td>
        </tr>
        <tr>
            <td width="15%"  style="font-size:16pt;text-align:left;">
                <strong>เวลา : </strong>
            </td>
            <td width="35%" style="font-size:16pt;text-align:left;">
                <strong><?php echo $shift['shiff_begin']; ?></strong>
            </td>
            <td width="15%"  style="font-size:16pt;text-align:left;">
                <strong>ถึง : </strong>
            </td>
            <td width="35%"  style="font-size:16pt;text-align:left;">
                <strong><?php echo $shift['shiff_end'];  ?></strong>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top: 1px solid #D8D8D8;" >
        <tr>
            <td width="40%" style="font-size:16pt;text-align:left; ">
                เลขที่ :
            </td>
            <td width="10%" style="font-size:16pt;text-align:left; "></td>
            <td width="50%" style="font-size:16pt;text-align:left" colspan="2">
                <?php echo $query['inv_num']; ?>
            </td>
        </tr>
        <tr>
            <td width="40%" style="font-size:16pt;text-align:left; ">
                วัน - เวลา :
            </td>
            <td width="10%" style="font-size:16pt;text-align:left; "></td>
            <td width="50%" style="font-size:16pt;text-align:left" colspan="2">
                <?php echo \metronic\components\DateConvert::mysql2phpDateTime($query['inv_date']); ?>
            </td>
        </tr>
        <tr>
            <td width="40%" style="font-size:16pt;text-align:left; ">
                ค่าบริการจอดรถ :
            </td>
            <td width="10%" style="font-size:16pt;text-align:left; "></td>
            <td width="30%" style="font-size:16pt;text-align:left" >
                <?php echo isset($query['fee_amt'])?$query['fee_amt']:'-'; ?>
            </td>
            <td width="20%" style="font-size:16pt;text-align:left; ">บาท</td>
        </tr>
        <tr>
            <td width="40%" style="font-size:16pt;text-align:left; ">
                ส่วนลด :
            </td>
            <td width="10%" style="font-size:16pt;text-align:left; "></td>
            <td width="30%" style="font-size:16pt;text-align:left">
                <?php echo isset($query['disc_amt'])?$query['disc_amt']:'-'; ?>
            </td>
            <td width="20%" style="font-size:16pt;text-align:left; ">บาท</td>
        </tr>
        <tr>
            <td width="40%" style="font-size:16pt;text-align:left; ">
                ค่าปรับบัตรหาย :
            </td>
            <td width="10%" style="font-size:16pt;text-align:left; "></td>
            <td width="30%" style="font-size:16pt;text-align:left">
                <?php echo isset($query['cardloss_amt'])?$query['cardloss_amt']:'-'; ?>
            </td>
            <td width="20%" style="font-size:16pt;text-align:left; ">บาท</td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top: 1px solid #D8D8D8;border-bottom: 1px solid #D8D8D8;">
        <tr>
            <td style="font-size:16pt;text-align:left; ">
                <?php $name = TbReceipt::find()->where(['inv_num' => $query['inv_num']])->all();?>
                <strong>จำนวนรถ <?php echo count($name); ?> คัน</strong>
            </td>
            <td style="font-size:16pt;text-align:right; ">
                <strong>รวมเป็นเงิน <?php echo $query['total_paid']; ?> บาท</strong>
            </td>
        </tr>
    </table>
<?php endif; ?>
<?php if ($type == 'footer') : ?>
    <?php $name = TbConfigReport::find()->where(['id' => $id])->one();?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="font-size:14pt;text-align:center; ">
                ลงชื่อ.................................
            </td>
        </tr>
        <tr>
            <td style="font-size:14pt;text-align:center; ">
                <strong>( <?php echo $query['createby']; ?> )</strong>
            </td>
        </tr>
        <tr>
            <td style="font-size:14pt;text-align:center; ">
                <strong>ผู้นำส่งเงินสด</strong>
            </td>
        </tr>
    </table>
    <br><br>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="font-size:14pt;text-align:center; ">
                ลงชื่อ.................................
            </td>
        </tr>
        
        <tr>
            <td style="font-size:14pt;text-align:center; ">
                <strong>เจ้าหน้าที่การเงิน</strong>
            </td>
        </tr>
    </table>
<?php endif; ?>



