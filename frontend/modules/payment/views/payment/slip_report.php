<?php
use yii\bootstrap\BootstrapAsset;
use yii\web\JqueryAsset;
use yii\helpers\Html;

BootstrapAsset::register($this);
JqueryAsset::register($this);
$this->registerCssFile("@web/css/80mm.css", [
    'depends' => [BootstrapAsset::className()],
]);
$this->registerCssFile("https://fonts.googleapis.com/css?family=Prompt",[
    'depends' => [BootstrapAsset::className()],
]);
$this->registerJs(<<<JS
    // window.print();
    // window.onafterprint = function(){
    //     window.close();
    // }
    var content = $('.x_content');
    $.each([ 52, 97 ], function( index, value ) {
      $('#clone').after(content.clone());
    });
JS
);
$formatter = \Yii::$app->formatter;

$this->title = 'ใบนำส่งเงินสด';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- 80mm -->
<center>
    <div class="x_content">
      <div class="row" style="width: 80mm;margin: auto;">
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 1cm 21px 20px 21px;">
            <div class="col-xs-12" style="padding: 0;">
                <h4 class="color" style="margin-top: 0px;margin-bottom: 0px;text-align: center;"><b style="font-weight: bold;"><?= Html::encode('ใบนำส่งเงินสด'); ?></b></h4>
                <h5 class="color" style="margin-top: 0px;margin-bottom: 5px;text-align: center;"><b><?= Html::encode('ลานจอดรถยนต์อาคารเฉลิมพระเกียรติ'); ?></b></h5>
                <h5 class="color" style="margin-top: 0px;margin-bottom: 0px;text-align: center;"><b><?= Html::encode('โรงพยาบาลราชวิถี'); ?></b></h5>
                <!-- <h6 class="color" style="margin-top: 4px;margin-bottom: 0px;text-align: center;"><b>Trat</b></h6>
                <h6 class="color" style="margin-top: 4px;margin-bottom: 8px;text-align: center;"><b>0805650351</b></h6> -->
            </div>
            <!-- <div class="col-xs-12" style="padding: 3px 0px 10px 0px;;text-align: left;">
                <h6 style="margin: 4px 1px;" class="color"><b>คำสั่งซื้อที่</b>  :  <b style="font-size: 13px;">#58</b> <b>วันที่</b> :  <b style="font-size: 12px;">31-01-2018 21:28</b></h6>
                <h6 style="margin: 4px 1px;" class="color"><b>ผู้ขาย</b>  :  <b style="font-size: 13px;">ไอดีสำหรับทดลอง</b> <b>การชำระ</b> :  <b style="font-size: 13px;">เงินสด</b></h6>
                <h6 style="margin: 4px 1px;" class="color"><b>ผู้ซื้อ</b>  :  <b style="font-size: 13px;">ลูกค้าทั่วไป</b> <b>เบอร์</b> :  <b style="font-size: 12px;">ไม่ได้ระบุ</b></h6>
            </div> -->
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 5px 20px 0px 20px;">
            <div class="col-xs-12" style="text-align: left;padding: 0;">
                <div class="col-xs-12" style="padding: 4px 0px 3px 0px;border-top: dashed 1px #404040;">
                	<div class="col-xs-6" style="padding: 1px;">
                		<h6 class="color" style="margin: 0px;"><b style="font-size: 14px;">เลขที่ : <?= $rows['inv_num']; ?></b></h6>
                	</div>
                </div>
                                                            
            	<div class="col-xs-12" style="padding: 15px 0px 18px 0px;border-top: dashed 1px #404040;border-bottom: dashed 1px #404040;">
            	   <div class="col-xs-5" style="padding: 3px 1px 3px 1px;">
                		<h6 class="color" style="margin: 0px;text-align: right;"><b style="font-size: 14px;">วัน/เวลา</b></h6>
                	</div>
                	<div class="col-xs-7" style="padding: 3px 1px 3px 1px;">
                		<h6 class="color" style="margin: 0px;text-align: right;"><b style="font-size: 14px;"> <?= $rows['inv_date']; ?></b></h6>
                	</div>
                	<div class="col-xs-5" style="padding: 3px 1px 3px 1px;">
                		<h6 class="color" style="margin: 0px;text-align: right;"><b style="font-size: 14px;">ค่าบริการจอดรถ</b></h6>
                	</div>
                	<div class="col-xs-7" style="padding: 3px 1px 3px 1px;">
                		<h6 class="color" style="margin: 0px;text-align: right;"><b style="font-size: 14px;"> <?= $formatter->format($rows['disc_amt'], ['decimal', 2]); ?> บาท</b></h6>
                	</div>
                    <div class="col-xs-5" style="padding: 3px 1px 3px 1px;">
                		<h6 class="color" style="margin: 0px;text-align: right;"><b style="font-size: 14px;">ส่วนลด</b></h6>
                	</div>
                	<div class="col-xs-7" style="padding: 3px 1px 3px 1px;">
                		<h6 class="color" style="margin: 0px;text-align: right;"><b style="font-size: 14px;"> <?= $formatter->format($rows['fee_amt'], ['decimal', 2]); ?> บาท</b></h6>
                	</div>
                	<div class="col-xs-5" style="padding: 3px 1px 3px 1px;">
                		<h6 class="color" style="margin: 0px;text-align: right;"><b style="font-size: 14px;">ค่าปรับบัตรหาย</b></h6>
                	</div>
                	<div class="col-xs-7" style="padding: 3px 1px 3px 1px;">
                		<h6 class="color" style="margin: 0px;text-align: right;"><b style="font-size: 14px;"> <?= $formatter->format($rows['cardloss_amt'], ['decimal', 2]); ?> บาท</b></h6>
                	</div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 5px 20px 0px 20px;">
            <div class="col-xs-12" style="text-align: right;padding: 0;">
                <div class="col-xs-11" style="padding: 1px;text-align: right;">
                    <h6 class="color" style="margin: 0px;font-size: 13px;"><b style="font-weight: bold;">รวมเป็นเงิน <?= $formatter->format($rows['total_paid'], ['decimal', 2]); ?></b></h6>
                </div>
                <div class="col-xs-1" style="padding: 1px;text-align: right;">
                    <h6 class="color" style="margin: 0px;font-size: 13px;font-weight: bold;"><b style="font-weight: bold;">บาท</b></h6>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 60px 0px 0px 0px;">
        	<h4 class="color" style="margin-top: 0px;margin-bottom: 0px;text-align: center;"><b>โรงพยาบาลราชวิถี</b></h4>
            <h6 class="color" style="margin-top: 4px;margin-bottom: 0px;text-align: center;"><b>ขอขอบคุณลูกค้าทุกท่านที่มาใช้บริการ</b></h6>
            <h5 style="margin: 10px 0 30px 0px;;text-align: center;" class="color"><b>ลงชื่อ</b></h5>
            <div class="col-xs-12" style="padding-bottom: 10px;">
                <div style="border-bottom: dashed 1px #404040;"></div>
            </div>
            <h5 style="margin-bottom: 30;text-align: center;" class="color"><b>ผู้นำส่งเงินสด</b></h5>
        </div>
      </div>
    </div>
    <div id="clone"></div>
</center>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>