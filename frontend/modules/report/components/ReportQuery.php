<?php

namespace frontend\modules\report\components;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use metronic\user\models\Profile;
use yii\helpers\ArrayHelper;
use frontend\modules\report\models\TbTransaction;
use frontend\modules\report\models\TbTransaction2;
use frontend\modules\card\models\TbCard;
use frontend\modules\card\models\TbCardType;

class ReportQuery {
    public static function getReportNightstate($card_id,$start_date,$end_date) 
    {
        $timediff13_hr = Yii::$app->db->createCommand('SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
        $time13format = "$timediff13_hr:00:00";
        $sql = 'SELECT
                tb_card.card_owner_name         AS card_owner_name,
                tb_transaction2.card_id          AS card_id,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate1_datetime,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate2_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate2_datetime,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate3_datetime,
                DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS now,
                timediff( tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime) AS T13_hr,
                    IF ( 
                        isnull
                            ( tb_transaction2.gate2_datetime ),
                        "Y", "N" ) AS test,
                    IF (
                        isnull(tb_transaction2.gate2_datetime ),
                    ceiling(
                        ( time_to_sec(
                                timediff( now(), tb_transaction2.gate1_datetime)) / 60
                        )
                    ),
                    ceiling(
                        ( time_to_sec(
                                timediff( tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)) / 60
                        )
                    )
                    ) AS timediff12_min,
                    ceiling(
                        (
                            time_to_sec(
                                timediff(now(),tb_transaction2.gate1_datetime)) / 3600
                        )
                    ) AS timediff13_hr,
                tb_transaction2.trans_status AS trans_status
            FROM
                (
                    (
                        tb_transaction2
                        JOIN tb_card ON (
                            ( tb_card.card_id = tb_transaction2.card_id
                            )
                        )
                    )
                    JOIN tb_card_type ON (
                        (
                            tb_card_type.card_type_id = tb_card.card_type_id
                        )
                    )
                )
            WHERE tb_transaction2.gate1_datetime >= :start_date AND tb_transaction2.gate1_datetime <= :end_date AND tb_transaction2.trans_status = 2 AND tb_transaction2.card_id = :card_id AND (
                timediff(
                    tb_transaction2.gate3_datetime,
                    tb_transaction2.gate1_datetime
                ) > "'.$time13format.'"
            )';
        $result = Yii::$app->db->createCommand($sql)
                ->bindParam(':card_id', $card_id)
                ->bindParam(':start_date', $start_date)
                ->bindParam(':end_date', $end_date)
                ->query();            
        return $result->readAll();
    }
    public static function getReportNonestate($card_id,$start_date,$end_date) 
    {
        $timediff12_min = Yii::$app->db->createCommand('SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
        $sql = 'SELECT
                tb_card.card_owner_name         AS card_owner_name,
                tb_transaction2.card_id          AS card_id,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate1_datetime,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate2_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate2_datetime,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate3_datetime,
                DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS now,
                IF (
                    isnull(tb_transaction2.gate2_datetime),
                    timediff( tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime),
                    timediff( tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)
                ) AS T13_hr,
                    IF ( 
                isnull
                    ( tb_transaction2.gate2_datetime ),
                "Y", "N" ) AS test,

                IF (
                    isnull(tb_transaction2.gate2_datetime),
                    ceiling(
                            ( time_to_sec(
                                    timediff( tb_transaction2.gate3_datetime, tb_transaction2.gate1_datetime)) / 60
                            )
                        ),
                        ceiling(
                            ( time_to_sec(
                                    timediff( tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)) / 60
                            )
                        )   
                ) AS timediff12_min,
                tb_transaction2.trans_status AS trans_status
            FROM
                (
                    (
                        tb_transaction2
                        JOIN tb_card ON (
                            ( tb_card.card_id = tb_transaction2.card_id
                            )
                        )
                    )
                    JOIN tb_card_type ON (
                        (
                            tb_card_type.card_type_id = tb_card.card_type_id
                        )
                    )
                )
            WHERE tb_transaction2.gate1_datetime >= :start_date AND tb_transaction2.gate1_datetime <= :end_date AND tb_transaction2.trans_status = 2 AND tb_card_type.card_type_id IN (1,2) AND tb_transaction2.card_id = :card_id
            HAVING (timediff12_min >= '.$timediff12_min.')';
        $result = Yii::$app->db->createCommand($sql)
                ->bindParam(':card_id', $card_id)
                ->bindParam(':start_date', $start_date)
                ->bindParam(':end_date', $end_date)
                ->query();            
        return $result->readAll();
    }
    public static function getCard_owner_name($card_id) 
    {
        $model =  TbCard::findOne(['card_id'=>$card_id]);
        return $model;
    }
    public static function getCard_type($card_type_id) 
    {
        $model =  TbCardType::findOne(['card_type_id'=>$card_type_id]);
        return !$model?'':$model['card_type_name'];
    }

    public static function getNameuser($user_id) 
    {
        $model = Profile::find()->select(['CONCAT(user_fname_th," ",user_lname_th) AS Username', 'user_id'])->where(['user_id' => $user_id])->asArray()->one();
        return $model['Username'];
    }
    public static function getConvertHrsToDay($time) 
    {
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time);
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        $dtF = new \DateTime("@0");
        $dtT = new \DateTime("@$time_seconds");
        if($dtF->diff($dtT)->format('%a') == 0){
            return $dtF->diff($dtT)->format('%H:%i:%s');
        }else{
            return $dtF->diff($dtT)->format('%a วัน %H:%i:%s');
        }
        
    }
    public static function getqueryIN_OUT(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_card.card_type_id',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_card.card_type_id',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryMember(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryVisitor(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_transaction.gate1_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (1)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_transaction2.gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (1)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryOut(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (1,2,3,4)")
            ->all();
        return $rows;
        //print_r($rows);
    }
    public static function getqueryIn(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (1,2,3,4)")
            ->all();
        return $rows;
        //print_r($rows);
    }
    public static function getqueryNow_member(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_member2(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_member34(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (3)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (3)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_Doctor(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (4)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (4)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_Gate2(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_Gate2_2(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_Gate2_34(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (3)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (3)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_Gate2_Doctor(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (4)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (4)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNone(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate2_datetime` is null")
            ->andWHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate2_datetime` is null")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNone2(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate2_datetime` is null")
            ->andWHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate2_datetime` is null")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNone34(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate2_datetime` is null")
            ->andWHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (3)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate2_datetime` is null")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (3)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNoneDoctor(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (4)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate2_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (4)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_doctorout(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_transaction2.gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (4)")
            ->all();
        return $rows;
        //print_r($rows);
    }
    public static function getqueryNow_memberout(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2,3,4)")
            ->all();
        return $rows;
        //print_r($rows);
    }
    public static function getqueryNow_memberout2(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (2)")
            ->all();
        return $rows;
        //print_r($rows);
    }
    public static function getqueryNow_memberout34(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_card.section_name',
                'tb_transaction2.gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (3,4)")
            ->all();
        return $rows;
        //print_r($rows);
    }
    public static function getqueryNow_visitor(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_transaction.gate1_datetime AS gate1_datetime',
                'tb_transaction.gate2_datetime',
                'tb_transaction.gate3_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (1)");
        $rows2 = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_transaction2.gate1_datetime AS gate1_datetime',
                'tb_transaction2.gate2_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (1)");
        $rows->union($rows2);
        return $rows->all();
    }
    public static function getqueryNow_visitorout(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction2.trans_id',
                'tb_transaction2.card_id',
                'tb_transaction2.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_transaction2.gate1_datetime',
                'tb_transaction2.gate3_datetime'
                ])
            ->from('tb_transaction2')
            ->leftJoin('tb_card','tb_transaction2.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction2`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWHERE("`tb_transaction2`.`gate3_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (1)")
            ->all();
        return $rows;
        //print_r($rows);
    }
    public static function getqueryNow_visitorin(){
        $rows = (new \yii\db\Query())
            ->select([
                'tb_transaction.trans_id',
                'tb_transaction.card_id',
                'tb_transaction.licenceplate_no',
                'tb_card.card_owner_name',
                'tb_transaction.gate1_datetime'
                ])
            ->from('tb_transaction')
            ->leftJoin('tb_card','tb_transaction.card_id = tb_card.card_id')
            ->WHERE("`tb_transaction`.`gate1_datetime` BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')")
            ->andWhere("tb_card.card_type_id IN (1)")
            ->all();
        return $rows;
        //print_r($rows);
    }
}
