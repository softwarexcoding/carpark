<?php

namespace frontend\modules\report\controllers;

use Yii;
use frontend\modules\report\models\TbTransaction;
use frontend\modules\report\models\TbTransactionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use msoft\helpers\Html;
use yii\filters\AccessControl;
use yii\db\Query;
use msoft\widgets\Icon;
use frontend\modules\report\components\ReportQuery;
use msoft\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
/**
 * DashboardController implements the CRUD actions for TbTransaction model.
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TbTransaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionGetData()
    {
    Yii::$app->response->format = Response::FORMAT_JSON;
    

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // รถเข้าชั้น1 ปัจจุบัน
        $queryIN =  Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction
            WHERE
            tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')')->queryAll();
         $queryBalance_member =  Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction
            LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
            WHERE
            tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
            AND tb_card.card_type_id IN (1,2,3,4)')->queryAll();
        $queryOut =  Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction2
            LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
            WHERE
            tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
            AND
            tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
            AND tb_card.card_type_id IN (1,2,3,4)')->queryAll();
        // รถเข้าชั้น1 ทั้งหมด
        $queryIN_OUT =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                    tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (1,2,3,4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                    tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (1,2,3,4)')->queryAll();
        $queryIN_OUT234 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                    tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2,3,4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                    tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2,3,4)')->queryAll();
        $queryIN_OUT1 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                    tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (1)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                    tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (1)')->queryAll();
        $queryNone_23 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_transaction.gate2_datetime IS NULL
                AND tb_card.card_type_id IN (2, 3)
            UNION 
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                    tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW() ,\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_transaction2.gate2_datetime IS NULL
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2, 3)')->queryAll();
        // รถเข้าชั้น 1 ของบุคลากร
        $queryNow_member =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2, 3, 4)
            UNION 
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                    tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW() ,\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2, 3, 4)')->queryAll();
        // รถเข้าชั้น 1 ของบุคลากรมีที่จอด
        $queryNow_member2 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2)')->queryAll();
        // รถเข้าชั้น 1 ของบุคลากรไม่มีที่จอด
        $queryNow_member34 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                    tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (3)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                    tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (3)')->queryAll();
        $queryNow_Doctor =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                    tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                    tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (4)')->queryAll();
        // รถเข้าชั้น 1 ของผู้รับบริการ
        $queryNow_visitor =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\') 
                AND tb_card.card_type_id IN (1)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\') 
                AND 
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (1)')->queryAll();
        // รถเข้าชั้น 4B ของบุคลากร
        $queryNow_Gate2 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                tb_transaction.gate2_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2,3,4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate2_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2,3,4)')->queryAll();
        // รถเข้าชั้น 4B ของบุคลากรมีที่จอด
        $queryNow_Gate2_2 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                tb_transaction.gate2_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate2_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2)')->queryAll();
        // รถเข้าชั้น 4B ของบุคลากรไม่มีที่จอด
        $queryNow_Gate2_34 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                tb_transaction.gate2_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (3)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate2_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (3)')->queryAll();
        $queryNow_Gate2_Doctor =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE
                tb_transaction.gate2_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate2_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND
                tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\')
                    AND DATE_FORMAT(NOW(), \'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (4)')->queryAll();
        // รถออกของบุคลากร
        $queryNow_Gate3_member =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2, 3, 4)')->queryAll();
        $queryNow_Gate3_member2 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (2)')->queryAll();
        $queryNow_Gate3_member34 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (3)')->queryAll();
        $queryNow_Gate3_Doctor =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE
                tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
                AND tb_card.card_type_id IN (4)')->queryAll();
        // รถออกของผู้รับบริการ
        $queryNow_Gate3_visitor =  Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction2
            LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
            WHERE
            tb_transaction2.gate3_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
            AND tb_transaction2.gate1_datetime BETWEEN DATE_FORMAT(NOW(),\'%Y-%m-%d 00:00:00\') 
                    AND DATE_FORMAT(NOW(),\'%Y-%m-%d 23:59:59\')
            AND tb_card.card_type_id IN (1)')->queryAll();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $timediff12_min = Yii::$app->db->createCommand('
            SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
        $Lot_visitor = Yii::$app->db->createCommand('
            SELECT tb_config_report.parkinglotqty_visitor FROM tb_config_report')->queryScalar();
        $Lot_member = Yii::$app->db->createCommand('
            SELECT tb_config_report.parkinglotqty_member FROM tb_config_report')->queryScalar();
        $timediff13_hr = Yii::$app->db->createCommand('
            SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
            $time13format = "$timediff13_hr:00:00";
        //จำนวนรถที่จอดอยู่ทั้งหมด
        $queryAll = Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction
            LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
            WHERE tb_card.card_type_id IN (1,2,3,4)')->queryAll();
        //จำนวนรถของบุคลากร
        $queryMember = Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction
            LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
            WHERE tb_card.card_type_id IN (2,3,4)')->queryAll();
        //จำนวนรถของผู้มาใช้บริการ
        $queryVisitor = Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction
            LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
            WHERE tb_card.card_type_id = 1')->queryAll();
        //จำนวนรถผู้มารับบริการจอดค้างคืน
        $queryNightstate_1 = Yii::$app->db->createCommand('
            SELECT tb_transaction.card_id FROM tb_transaction
            LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
            WHERE
                timediff(
                    now(),
                    tb_transaction.gate1_datetime
                ) > "24:00:00" AND
            tb_card.card_type_id IN (1)')
            ->queryAll();
        //จำนวนรถบุคลากรจอดค้างคืน
        $queryNightstate_234 = Yii::$app->db->createCommand('SELECT
            tb_transaction.card_id
            FROM
            tb_transaction
            LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
            WHERE
            timediff(
                    now(),
                    tb_transaction.gate1_datetime
                ) > "'.$time13format.'" 
                AND tb_card.card_type_id IN (2,3,4)')->queryAll();
        $queryNonestate = Yii::$app->db->createCommand('SELECT
    *
FROM
    tb_transaction
LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
WHERE  tb_card.card_type_id IN (2,3)
AND tb_transaction.gate2_datetime is null')->queryAll();
        // $Nightstate = ArrayHelper::index($queryNightstate, null, 'card_type_id');
        // $Nonestate  = ArrayHelper::index($queryNonestate, null, 'card_type_id');
        return [
            #row1
            //'queryNow'                  => count($queryNow),
            'queryNow_member'           => count($queryNow_member),
            'queryNow_member34'         => count($queryNow_member34),
            'queryNow_member2'          => count($queryNow_member2),
            'queryNow_Doctor'           => count($queryNow_Doctor),
            'queryNow_visitor'          => count($queryNow_visitor),
            'queryNow_Gate2_2'          => count($queryNow_Gate2_2),
            'queryNow_Gate2_34'         => count($queryNow_Gate2_34),
            'queryNow_Gate2_Doctor'     => count($queryNow_Gate2_Doctor),
            'queryNow_Gate3_member'     => count($queryNow_Gate3_member),
            'queryNow_Gate3_member2'    => count($queryNow_Gate3_member2),
            'queryNow_Gate3_member34'   => count($queryNow_Gate3_member34),
            'queryNow_Gate3_Doctor'     => count($queryNow_Gate3_Doctor),
            'queryNow_Gate3_visitor'    => count($queryNow_Gate3_visitor),
            'queryIN_OUT'               => count($queryIN_OUT),
            'queryNow_Gate2'            => count($queryNow_Gate2),
            'queryBalance_member'       => count($queryBalance_member),
            'queryBalance_visitor'      => (count($queryNow_visitor) - count($queryNow_Gate3_visitor)),
            'queryNone'                 => (count($queryNone_23) + count($queryNow_Gate2_Doctor)) ,
            'queryNone_2'               => (count($queryNow_member2) - count($queryNow_Gate2_2)),
            'queryNone_34'              => (count($queryNow_member34) - count($queryNow_Gate2_34)),
            'queryNone_Doctor'          => count($queryNow_Gate2_Doctor),
            'queryOut'                  => count($queryOut),
            'queryIN_OUT234'            => count($queryIN_OUT234),
            'queryIN_OUT1'              => count($queryIN_OUT1),
            'lot_null'                  => (($Lot_visitor + $Lot_member)-(count($queryNightstate_1)+count($queryNightstate_234)))-count($queryBalance_member),
            'lot'                       => $Lot_visitor + $Lot_member,
            #row1
            'queryAll'                  => count($queryAll),
            'queryVisitor'              => count($queryVisitor),
            'queryMember'               => count($queryMember),
            'queryNightstate_1'         => count($queryNightstate_1),
            'queryNightstate_234'       => count($queryNightstate_234),
            'Lot_visitor'               => $Lot_visitor,
            'Lot_member'                => $Lot_member,
            'querySpace_visitor'        => $Lot_visitor - (count($queryVisitor)),
            'querySpace_member'         => $Lot_member - (count($queryMember)),
            'queryNonestate'            => count($queryNonestate),
        ];
    }
    public function actionStatus()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
            $queryStatus = Yii::$app->db->createCommand('
            SELECT  \' \'                           AS id,
                    tb_transaction.trans_id         AS trans_id,
                    tb_transaction.card_id          AS card_id,
                    tb_transaction.licenceplate_no  AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time
            FROM    tb_transaction
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE
                tb_card.card_type_id IN (1,2,3,4)
            AND DATE_FORMAT(
                tb_transaction.gate1_datetime,
                \'%Y-%m-%d\'
            ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            ORDER BY 
                tb_transaction.gate1_datetime DESC')
            ->queryAll();
        }else{
            $queryStatus = Yii::$app->db->createCommand('
                SELECT  \' \'                           AS id,
                        tb_transaction.trans_id         AS trans_id,
                        tb_transaction.card_id          AS card_id,
                        tb_transaction.licenceplate_no  AS licenceplate_no,
                        tb_card.card_owner_name         AS card_owner_name,
                        tb_card.section_name            AS section_name,
                        tb_card_type.card_type_id       AS card_type_id,
                        tb_card_type.card_type_name     AS card_type_name,
                        DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                        DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time
                FROM    tb_transaction
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE
                    tb_card.card_type_id IN (1,2,3,4)
                AND DATE_FORMAT(
                tb_transaction.gate1_datetime,
                \'%Y-%m-%d\'
            ) = DATE(NOW())
                ORDER BY 
                    tb_transaction.gate1_datetime DESC')
                ->queryAll();
        }
        return $this->render('_detail_status', [
            'queryStatus' => $queryStatus,
        ]);
    }
    
    public function actionStatusHistory()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
            $queryHistory = Yii::$app->db->createCommand('
            SELECT  \' \'                           AS id,
                    tb_transaction2.trans_id         AS trans_id,
                    tb_transaction2.card_id          AS card_id,
                    tb_transaction2.licenceplate_no  AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate3_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate3_time
            FROM    tb_transaction2
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE
                tb_card.card_type_id IN (1,2,3,4)
            AND DATE_FORMAT(
                tb_transaction2.gate1_datetime,
                \'%Y-%m-%d\'
            ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            ORDER BY 
                tb_transaction2.gate1_datetime DESC')
            ->queryAll();
        }else{
            $queryHistory = Yii::$app->db->createCommand('
            SELECT  \' \'                           AS id,
                    tb_transaction2.trans_id         AS trans_id,
                    tb_transaction2.card_id          AS card_id,
                    tb_transaction2.licenceplate_no  AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate3_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate3_time
            FROM    tb_transaction2
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE
                tb_card.card_type_id IN (1,2,3,4)
            AND DATE_FORMAT(
                tb_transaction2.gate1_datetime,
                \'%Y-%m-%d\'
            ) = DATE(NOW())
            
            ORDER BY 
                tb_transaction2.gate1_datetime DESC')
            ->queryAll();
        }
        return $this->render('_detail_history', [
            'queryHistory' => $queryHistory,
        ]);
    }
    public function actionStatusOut()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
            $queryStatus = Yii::$app->db->createCommand('
            SELECT  \' \'                           AS id,
                    tb_transaction2.trans_id        AS trans_id,
                    tb_transaction2.card_id         AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate3_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate3_time
            FROM    tb_transaction2
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE
                tb_card.card_type_id IN (1,2,3,4)
            AND DATE_FORMAT(
                tb_transaction2.gate3_datetime,
                \'%Y-%m-%d\'
            ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            ORDER BY tb_transaction2.gate3_datetime DESC')
            ->queryAll();
        }else{
            $queryStatus = Yii::$app->db->createCommand('
            SELECT  \' \'                           AS id,
                    tb_transaction2.trans_id        AS trans_id,
                    tb_transaction2.card_id         AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate3_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate3_time
            FROM    tb_transaction2
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE
                tb_card.card_type_id IN (1,2,3,4)
            AND DATE_FORMAT(
                tb_transaction2.gate3_datetime,
                \'%Y-%m-%d\'
            ) = DATE(NOW())
            ORDER BY tb_transaction2.gate3_datetime DESC')
            ->queryAll();
        }
        return $this->render('_detail_out', [
            'queryStatus' => $queryStatus,
        ]);
    }
    public function actionStatusNightstate()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
            $timediff13_hr = Yii::$app->db->createCommand('
            SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
            $time13format = "$timediff13_hr:00:00";
            $queryNightstate = Yii::$app->db->createCommand('
            SELECT  \' \'                           AS id,
                    tb_transaction.trans_id         AS trans_id,
                    tb_transaction.card_id          AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    CONCAT((floor(ceiling( time_to_sec(timediff(now(),tb_transaction.gate1_datetime)) / 3600) / 24)), " วัน  ",(floor((ceiling( time_to_sec(timediff(now(),tb_transaction.gate1_datetime)) / 3600) % 24) )), " ชั่วโมง") as timediff13_hr
            FROM tb_transaction
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE tb_card.card_type_id IN (1,2,3,4) 
            AND DATE_FORMAT(
                tb_transaction.gate1_datetime,
                \'%Y-%m-%d\'
            ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            AND timediff(now(),tb_transaction.gate1_datetime) > "'.$time13format.'" ')->queryAll();
        }else{
            $timediff13_hr = Yii::$app->db->createCommand('
            SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
            $time13format = "$timediff13_hr:00:00";
            $queryNightstate = Yii::$app->db->createCommand('
            SELECT  \' \'                           AS id,
                    tb_transaction.trans_id         AS trans_id,
                    tb_transaction.card_id          AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    CONCAT((floor(ceiling( time_to_sec(timediff(now(),tb_transaction.gate1_datetime)) / 3600) / 24)), " วัน  ",(floor((ceiling( time_to_sec(timediff(now(),tb_transaction.gate1_datetime)) / 3600) % 24) )), " ชั่วโมง") as timediff13_hr
            FROM tb_transaction
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE tb_card.card_type_id IN (1,2,3,4) 
            AND DATE_FORMAT(
                tb_transaction.gate1_datetime,
                \'%Y-%m-%d\'
            ) = DATE(NOW())
            AND timediff(now(),tb_transaction.gate1_datetime) > "'.$time13format.'" ')->queryAll();
        }
        return $this->render('_detail_nightstate', [
            'queryNightstate' => $queryNightstate,
        ]);
    }
    public function actionStatusNonestate()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
            $timediff12_min = Yii::$app->db->createCommand('
            SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
        $queryNonestate = Yii::$app->db->createCommand('
                SELECT \' \'                            AS id,
                        tb_transaction.trans_id         AS trans_id,
                        tb_transaction.card_id          AS card_id,
                        tb_card.licenceplate_no         AS licenceplate_no,
                        tb_card.card_owner_name         AS card_owner_name,
                        tb_card.section_name            AS section_name,
                        tb_card_type.card_type_id       AS card_type_id,
                        tb_card_type.card_type_name     AS card_type_name,
                        DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                        DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    CASE WHEN tb_transaction.gate2_datetime IS NULL THEN
                    CASE WHEN tb_transaction.gate3_datetime IS NULL THEN
                            timediff(NOW(),tb_transaction.gate1_datetime)
                        ELSE
                            timediff(tb_transaction.gate3_datetime,tb_transaction.gate1_datetime)               
                            END
                        ELSE
                            timediff(tb_transaction.gate2_datetime,tb_transaction.gate1_datetime)
                            END 
                    AS timediff12_min
                FROM tb_transaction
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE tb_card_type.card_type_id IN (2,3) 
                AND tb_transaction.gate2_datetime is null
                AND DATE_FORMAT(
                    tb_transaction.gate1_datetime,
                    \'%Y-%m-%d\'
                ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            UNION
                SELECT
                    \' \'                           AS id,
                    tb_transaction2.trans_id        AS trans_id,
                    tb_transaction2.card_id         AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    CASE WHEN tb_transaction2.gate2_datetime IS NULL THEN
                    CASE WHEN tb_transaction2.gate3_datetime IS NULL THEN
                            timediff(NOW(),tb_transaction2.gate1_datetime)
                        ELSE
                            timediff(tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime)               
                            END
                        ELSE
                            timediff(tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)
                            END 
                    AS timediff12_min
                FROM tb_transaction2
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE tb_card_type.card_type_id IN (2,3) AND tb_transaction2.gate2_datetime is null
                AND DATE_FORMAT(
                    tb_transaction2.gate1_datetime,
                    \'%Y-%m-%d\'
                ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            HAVING timediff12_min >= '.$timediff12_min.'')->queryAll();
        }else{
            $timediff12_min = Yii::$app->db->createCommand('
            SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
            $queryNonestate = Yii::$app->db->createCommand('
                SELECT \' \'                            AS id,
                        tb_transaction.trans_id         AS trans_id,
                        tb_transaction.card_id          AS card_id,
                        tb_card.licenceplate_no         AS licenceplate_no,
                        tb_card.card_owner_name         AS card_owner_name,
                        tb_card.section_name            AS section_name,
                        tb_card_type.card_type_id       AS card_type_id,
                        tb_card_type.card_type_name     AS card_type_name,
                        DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                        DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    CASE WHEN tb_transaction.gate2_datetime IS NULL THEN
                    CASE WHEN tb_transaction.gate3_datetime IS NULL THEN
                            timediff(NOW(),tb_transaction.gate1_datetime)
                        ELSE
                            timediff(tb_transaction.gate3_datetime,tb_transaction.gate1_datetime)               
                            END
                        ELSE
                            timediff(tb_transaction.gate2_datetime,tb_transaction.gate1_datetime)
                            END 
                    AS timediff12_min
                FROM tb_transaction
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE tb_card_type.card_type_id IN (2,3) AND tb_transaction.gate2_datetime is null
                AND DATE_FORMAT(
                    tb_transaction.gate1_datetime,
                    \'%Y-%m-%d\'
                ) = DATE(NOW())
            UNION
                SELECT
                    \' \'                           AS id,
                    tb_transaction2.trans_id        AS trans_id,
                    tb_transaction2.card_id         AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    CASE WHEN tb_transaction2.gate2_datetime IS NULL THEN
                    CASE WHEN tb_transaction2.gate3_datetime IS NULL THEN
                            timediff(NOW(),tb_transaction2.gate1_datetime)
                        ELSE
                            timediff(tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime)               
                            END
                        ELSE
                            timediff(tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)
                            END 
                    AS timediff12_min
                FROM tb_transaction2
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE tb_card_type.card_type_id IN (2,3) AND tb_transaction2.gate2_datetime is null
                AND DATE_FORMAT(
                    tb_transaction2.gate1_datetime,
                    \'%Y-%m-%d\'
                ) = DATE(NOW())
            HAVING timediff12_min >= '.$timediff12_min.'')->queryAll();
        }
        return $this->render('_detail_nonestate', [
            'queryNonestate'  => $queryNonestate,
        ]);
    }

    public function actionStatusCondition2()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        $timediff12_min = Yii::$app->db->createCommand('
            SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
        $timediff13_hr = Yii::$app->db->createCommand('
            SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
        $time13format = "$timediff13_hr:00:00";
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
            $queryCondition2 = Yii::$app->db->createCommand('
                SELECT
                    \'\' AS id,
                    q1.trans_id,
                    q1.card_id,
                    q1.licenceplate_no,
                    q1.card_owner_name,
                    q1.section_name,
                    q1.card_type_id,
                    q1.card_type_name,
                    q1.gate1_date,
                    q1.gate1_time
                FROM
                    (
                        SELECT
                            tb_transaction.trans_id AS trans_id,
                            tb_transaction.card_id AS card_id,
                            tb_card.licenceplate_no AS licenceplate_no,
                            tb_card.card_owner_name AS card_owner_name,
                            tb_card.section_name AS section_name,
                            tb_card_type.card_type_id AS card_type_id,
                            tb_card_type.card_type_name AS card_type_name,
                            DATE_FORMAT(
                                DATE_ADD(
                                    tb_transaction.gate1_datetime,
                                    INTERVAL 543 YEAR
                                ) ,\'%d/%m/%Y\') AS gate1_date,
                 DATE_FORMAT(
                        DATE_ADD(
                            tb_transaction.gate1_datetime,
                            INTERVAL 543 YEAR
                        ),\'%H:%i:%s\') AS gate1_time,
                        CASE WHEN tb_transaction.gate2_datetime IS NULL THEN
                        CASE WHEN tb_transaction.gate3_datetime IS NULL THEN
                                        timediff(NOW(),tb_transaction.gate1_datetime)
                                ELSE
                                        timediff(tb_transaction.gate3_datetime,tb_transaction.gate1_datetime)               
                                        END
                                ELSE
                                        timediff(tb_transaction.gate2_datetime,tb_transaction.gate1_datetime)
                                        END 
                        AS timediff12_min
                    FROM tb_transaction
                    LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
                    LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                    WHERE tb_card_type.card_type_id IN (2,3) 
                    AND tb_transaction.gate2_datetime is null
                    AND DATE_FORMAT(
                            tb_transaction.gate1_datetime,
                            \'%Y-%m-%d\'
                    ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                UNION
                    SELECT
                            tb_transaction2.trans_id        AS trans_id,
                            tb_transaction2.card_id         AS card_id,
                            tb_card.licenceplate_no         AS licenceplate_no,
                            tb_card.card_owner_name         AS card_owner_name,
                            tb_card.section_name            AS section_name,
                            tb_card_type.card_type_id       AS card_type_id,
                            tb_card_type.card_type_name     AS card_type_name,
                            DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                            DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                            CASE WHEN tb_transaction2.gate2_datetime IS NULL THEN
                            CASE WHEN tb_transaction2.gate3_datetime IS NULL THEN
                                            timediff(NOW(),tb_transaction2.gate1_datetime)
                                    ELSE
                                            timediff(tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime)               
                                            END
                                    ELSE
                                            timediff(tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)
                                            END 
                            AS timediff12_min
                    FROM tb_transaction2
                    LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
                    LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                    WHERE tb_card_type.card_type_id IN (2,3) AND tb_transaction2.gate2_datetime is null
                    AND DATE_FORMAT(
                            tb_transaction2.gate1_datetime,
                            \'%Y-%m-%d\'
                    ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                HAVING timediff12_min >= '.$timediff12_min.'
                ) q1
                INNER JOIN 
                (
                SELECT 
                tb_transaction.trans_id AS trans_id,
                tb_transaction.card_id AS card_id,
                tb_card.licenceplate_no AS licenceplate_no,
                tb_card.card_owner_name AS card_owner_name,
                tb_card.section_name AS section_name,
                tb_card_type.card_type_id AS card_type_id,
                tb_card_type.card_type_name AS card_type_name,
                DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                CONCAT((floor(ceiling( time_to_sec(timediff(now(),tb_transaction.gate1_datetime)) / 3600) / 24)), " วัน  ",(floor((ceiling( time_to_sec(timediff(now(),tb_transaction.gate1_datetime)) / 3600) % 24) )), " ชั่วโมง") as timediff13_hr
                FROM tb_transaction
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE tb_card.card_type_id IN (1,2,3,4) 
                AND DATE_FORMAT(
                        tb_transaction.gate1_datetime,
                        \'%Y-%m-%d\'
                ) BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND timediff(now(),tb_transaction.gate1_datetime) > "'.$time13format.'" 
                ) q2 ON q1.trans_id = q2.trans_id')->queryAll();
        }else{
            $queryCondition2 = Yii::$app->db->createCommand('
                SELECT
                \'\' AS id,
                q1.trans_id,
                q1.card_id,
                q1.licenceplate_no,
                q1.card_owner_name,
                q1.section_name,
                q1.card_type_id,
                q1.card_type_name,
                q1.gate1_date,
                q1.gate1_time
            FROM
                (
                    SELECT
                        tb_transaction.trans_id AS trans_id,
                        tb_transaction.card_id AS card_id,
                        tb_card.licenceplate_no AS licenceplate_no,
                        tb_card.card_owner_name AS card_owner_name,
                        tb_card.section_name AS section_name,
                        tb_card_type.card_type_id AS card_type_id,
                        tb_card_type.card_type_name AS card_type_name,
                        DATE_FORMAT(
                            DATE_ADD(
                                tb_transaction.gate1_datetime,
                                INTERVAL 543 YEAR
                            ),
                            \'%d/%m/%Y\'
                ) AS gate1_date,
                DATE_FORMAT(
                    DATE_ADD(
                        tb_transaction.gate1_datetime,
                        INTERVAL 543 YEAR
                    ) ,\'%H:%i:%s\'
                )AS gate1_time, 

            CASE WHEN tb_transaction.gate2_datetime IS NULL THEN
            CASE WHEN tb_transaction.gate3_datetime IS NULL THEN
                            timediff(NOW(),tb_transaction.gate1_datetime)
                    ELSE
                            timediff(tb_transaction.gate3_datetime,tb_transaction.gate1_datetime)               
                            END
                    ELSE
                            timediff(tb_transaction.gate2_datetime,tb_transaction.gate1_datetime)
                            END 
            AS timediff12_min
            FROM tb_transaction
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE tb_card_type.card_type_id IN (2,3) AND tb_transaction.gate2_datetime is null
            AND DATE_FORMAT(
                    tb_transaction.gate1_datetime,
                    \'%Y-%m-%d\'
            ) = DATE(NOW())
            UNION
            SELECT
                    tb_transaction2.trans_id        AS trans_id,
                    tb_transaction2.card_id         AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    CASE WHEN tb_transaction2.gate2_datetime IS NULL THEN
                    CASE WHEN tb_transaction2.gate3_datetime IS NULL THEN
                                    timediff(NOW(),tb_transaction2.gate1_datetime)
                            ELSE
                                    timediff(tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime)               
                                    END
                            ELSE
                                    timediff(tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)
                                    END 
                    AS timediff12_min
            FROM tb_transaction2
            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
            WHERE tb_card_type.card_type_id IN (2,3) AND tb_transaction2.gate2_datetime is null
            AND DATE_FORMAT(
                    tb_transaction2.gate1_datetime,
                    \'%Y-%m-%d\'
            ) = DATE(NOW())
            HAVING timediff12_min >= '.$timediff12_min.') q1
            INNER JOIN 
            (
                SELECT 
                    tb_transaction.trans_id         AS trans_id,
                    tb_transaction.card_id          AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                    DATE_FORMAT(DATE_ADD(tb_transaction.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                    CONCAT((floor(ceiling( time_to_sec(timediff(now(),tb_transaction.gate1_datetime)) / 3600) / 24)), " วัน  ",(floor((ceiling( time_to_sec(timediff(now(),tb_transaction.gate1_datetime)) / 3600) % 24) )), " ชั่วโมง") as timediff13_hr
                FROM tb_transaction
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE tb_card.card_type_id IN (1,2,3,4) 
                AND DATE_FORMAT(
                        tb_transaction.gate1_datetime,
                        \'%Y-%m-%d\'
                ) = DATE(NOW())
                AND timediff(now(),tb_transaction.gate1_datetime) > "'.$time13format.'" 
            ) q2 ON q1.trans_id = q2.trans_id')->queryAll();
        }
        return $this->render('_detail_condition2', [
            'queryCondition2'  => $queryCondition2,
        ]);
    }
    public function actionReportNightstate()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
                $timediff13_hr = Yii::$app->db->createCommand('
                SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
                $time13format = "$timediff13_hr:00:00";
                //print_r($time13format);
                $queryReportNightsate = Yii::$app->db->createCommand('
                SELECT  \' \'                            AS id,
                        tb_transaction2.trans_id         AS trans_id,
                        tb_transaction2.card_id          AS card_id,
                        tb_card.licenceplate_no          AS licenceplate_no,
                        tb_card.card_owner_name          AS card_owner_name,
                        tb_card.section_name             AS section_name,
                        tb_card_type.card_type_id        AS card_type_id,
                        tb_card_type.card_type_name      AS card_type_name,
                        DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate1_datetime,
                        COUNT(tb_transaction2.trans_id) AS On_tran
                FROM tb_transaction2
                LEFT JOIN tb_card ON (tb_card.card_id = tb_transaction2.card_id )
                LEFT JOIN tb_card_type ON (tb_card_type.card_type_id = tb_card.card_type_id )
                WHERE timediff(tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime) > "'.$time13format.'"
                AND DATE_FORMAT(tb_transaction2.gate1_datetime,\'%Y-%m-%d\') BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                GROUP BY tb_transaction2.card_id
                ORDER BY tb_transaction2.gate1_datetime')->queryAll();
            }else{
                $timediff13_hr = Yii::$app->db->createCommand('
                SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
                $time13format = "$timediff13_hr:00:00";
                //print_r($time13format);
                $queryReportNightsate = Yii::$app->db->createCommand('
                SELECT  \' \'                            AS id,
                        tb_transaction2.trans_id         AS trans_id,
                        tb_transaction2.card_id          AS card_id,
                        tb_card.licenceplate_no          AS licenceplate_no,
                        tb_card.card_owner_name          AS card_owner_name,
                        tb_card.section_name             AS section_name,
                        tb_card_type.card_type_id        AS card_type_id,
                        tb_card_type.card_type_name      AS card_type_name,
                        DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate1_datetime,
                        COUNT(tb_transaction2.trans_id) AS On_tran
                FROM tb_transaction2
                LEFT JOIN tb_card ON (tb_card.card_id = tb_transaction2.card_id )
                LEFT JOIN tb_card_type ON (tb_card_type.card_type_id = tb_card.card_type_id )
                WHERE timediff(tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime) > "'.$time13format.'"
                AND DATE_FORMAT(tb_transaction2.gate1_datetime,\'%Y-%m-%d\') = DATE(NOW())
                GROUP BY tb_transaction2.card_id
                ORDER BY tb_transaction2.gate1_datetime')->queryAll();
            }
        return $this->render('_report_nightstate', [
            'queryReportNightsate'  => $queryReportNightsate,
        ]);
    }
    public function actionReportNonestate()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
            $timediff12_min = Yii::$app->db->createCommand('
                SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
            $queryReportNonestate = Yii::$app->db->createCommand('
                SELECT i.*,COUNT(i.trans_id) AS On_tran FROM (SELECT
                    \' \'                           AS id,
                    tb_transaction2.trans_id        AS trans_id,
                    tb_transaction2.card_id         AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate1_datetime,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate2_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate2_datetime,
                    DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS now,
                    IF (
                        isnull(
                            tb_transaction2.gate2_datetime
                        ),
                        ceiling(
                            (
                                time_to_sec(
                                    timediff(
                                        tb_transaction2.gate3_datetime,
                                        tb_transaction2.gate1_datetime
                                    )
                                ) / 60
                            )
                        ),
                        ceiling(
                            (
                                time_to_sec(
                                    timediff(
                                        tb_transaction2.gate2_datetime,
                                        tb_transaction2.gate1_datetime
                                    )
                                ) / 60
                            )
                        )
                    ) AS timediff12_min,
                    tb_transaction2.trans_status     AS trans_status
                FROM tb_transaction2
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE tb_card_type.card_type_id IN (2, 3)
                AND DATE_FORMAT( tb_transaction2.gate1_datetime,\'%Y-%m-%d\') BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            )i
            WHERE i.timediff12_min >= '.$timediff12_min.'
            GROUP BY i.card_id')->queryAll();
        }else{
            $timediff12_min = Yii::$app->db->createCommand('
                SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
            $queryReportNonestate = Yii::$app->db->createCommand('
                SELECT i.*,COUNT(i.trans_id) AS On_tran FROM (SELECT
                    \' \'                           AS id,
                    tb_transaction2.trans_id        AS trans_id,
                    tb_transaction2.card_id         AS card_id,
                    tb_card.licenceplate_no         AS licenceplate_no,
                    tb_card.card_owner_name         AS card_owner_name,
                    tb_card.section_name            AS section_name,
                    tb_card_type.card_type_id       AS card_type_id,
                    tb_card_type.card_type_name     AS card_type_name,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate1_datetime,
                    DATE_FORMAT(DATE_ADD(tb_transaction2.gate2_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate2_datetime,
                    DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS now,
                    IF (
                        isnull(
                            tb_transaction2.gate2_datetime
                        ),
                        ceiling(
                            (
                                time_to_sec(
                                    timediff(
                                        tb_transaction2.gate3_datetime,
                                        tb_transaction2.gate1_datetime
                                    )
                                ) / 60
                            )
                        ),
                        ceiling(
                            (
                                time_to_sec(
                                    timediff(
                                        tb_transaction2.gate2_datetime,
                                        tb_transaction2.gate1_datetime
                                    )
                                ) / 60
                            )
                        )
                    ) AS timediff12_min,
                    tb_transaction2.trans_status     AS trans_status
                FROM tb_transaction2
                LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
                LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                WHERE tb_card_type.card_type_id IN (2, 3)
                AND DATE_FORMAT( tb_transaction2.gate1_datetime,\'%Y-%m-%d\') = DATE(NOW())  
            )i
            WHERE i.timediff12_min >= '.$timediff12_min.'
            GROUP BY i.card_id')->queryAll();
        }
        return $this->render('_report_nonestate', [
            'queryReportNonestate'  => $queryReportNonestate,
        ]);
    }
    public function actionReportCondition2()
    {
        $start_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('from_date'));
        $end_date = \metronic\components\DateConvert::convertDate(Yii::$app->request->post('to_date'));
        $timediff12_min = Yii::$app->db->createCommand('
                SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
        $timediff13_hr = Yii::$app->db->createCommand('
                SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
        $time13format = "$timediff13_hr:00:00";
        if((isset($_POST['from_date']) && isset($_POST['to_date']))){
           $queryReportCondition2 = Yii::$app->db->createCommand('SELECT
                \'\' AS id,
                q1.trans_id,
                q1.card_id,
                q1.licenceplate_no,
                q1.card_owner_name,
                q1.section_name,
                q1.card_type_id,
                q1.card_type_name
            FROM
                (
                    SELECT
                        i.*, COUNT(i.trans_id) AS On_tran
                    FROM
                        (
                            SELECT
                                tb_transaction2.trans_id AS trans_id,
                                tb_transaction2.card_id AS card_id,
                                tb_card.licenceplate_no AS licenceplate_no,
                                tb_card.card_owner_name AS card_owner_name,
                                tb_card.section_name AS section_name,
                                tb_card_type.card_type_id AS card_type_id,
                                tb_card_type.card_type_name AS card_type_name,

                            IF (
                                isnull(
                                    tb_transaction2.gate2_datetime
                                ),
                                ceiling(
                                    (
                                        time_to_sec(
                                            timediff(
                                                tb_transaction2.gate3_datetime,
                                                tb_transaction2.gate1_datetime
                                            )
                                        ) / 60
                                    )
                                ),
                                ceiling(
                                    (
                                        time_to_sec(
                                            timediff(
                                                tb_transaction2.gate2_datetime,
                                                tb_transaction2.gate1_datetime
                                            )
                                        ) / 60
                                    )
                                )
                            ) AS timediff12_min,
                            tb_transaction2.trans_status AS trans_status
                        FROM
                            tb_transaction2
                        LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
                        LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                        WHERE
                            tb_card_type.card_type_id IN (2, 3)
                        AND DATE_FORMAT(
                            tb_transaction2.gate1_datetime ,\'%Y-%m-%d\') BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                        )i
                        WHERE i.timediff12_min >= '.$timediff12_min.'
                        GROUP BY i.card_id
                ) q1
            INNER JOIN 
                (
                    SELECT
                                    tb_transaction2.trans_id         AS trans_id,
                                    tb_transaction2.card_id          AS card_id,
                                    tb_card.licenceplate_no          AS licenceplate_no,
                                    tb_card.card_owner_name          AS card_owner_name,
                                    tb_card.section_name             AS section_name,
                                    tb_card_type.card_type_id        AS card_type_id,
                                    tb_card_type.card_type_name      AS card_type_name,
                    COUNT(tb_transaction2.trans_id) AS On_tran
                            FROM tb_transaction2
                            LEFT JOIN tb_card ON (tb_card.card_id = tb_transaction2.card_id )
                            LEFT JOIN tb_card_type ON (tb_card_type.card_type_id = tb_card.card_type_id )
                            WHERE timediff(tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime) > "'.$time13format.'"
                            AND DATE_FORMAT(tb_transaction2.gate1_datetime,\'%Y-%m-%d\') BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                            GROUP BY tb_transaction2.card_id
                            ORDER BY tb_transaction2.gate1_datetime
            ) q2 ON q1.trans_id = q2.trans_id')->queryAll();
        }else{
            $queryReportCondition2 = Yii::$app->db->createCommand('SELECT
                \'\' AS id,
                q1.trans_id,
                q1.card_id,
                q1.licenceplate_no,
                q1.card_owner_name,
                q1.section_name,
                q1.card_type_id,
                q1.card_type_name
            FROM
                (
                    SELECT
                        i.*, COUNT(i.trans_id) AS On_tran
                    FROM
                        (
                            SELECT
                                tb_transaction2.trans_id AS trans_id,
                                tb_transaction2.card_id AS card_id,
                                tb_card.licenceplate_no AS licenceplate_no,
                                tb_card.card_owner_name AS card_owner_name,
                                tb_card.section_name AS section_name,
                                tb_card_type.card_type_id AS card_type_id,
                                tb_card_type.card_type_name AS card_type_name,

                            IF (
                                isnull(
                                    tb_transaction2.gate2_datetime
                                ),
                                ceiling(
                                    (
                                        time_to_sec(
                                            timediff(
                                                tb_transaction2.gate3_datetime,
                                                tb_transaction2.gate1_datetime
                                            )
                                        ) / 60
                                    )
                                ),
                                ceiling(
                                    (
                                        time_to_sec(
                                            timediff(
                                                tb_transaction2.gate2_datetime,
                                                tb_transaction2.gate1_datetime
                                            )
                                        ) / 60
                                    )
                                )
                            ) AS timediff12_min
                            FROM
                                tb_transaction2
                            LEFT JOIN tb_card ON tb_card.card_id = tb_transaction2.card_id
                            LEFT JOIN tb_card_type ON tb_card_type.card_type_id = tb_card.card_type_id
                            WHERE
                                tb_card_type.card_type_id IN (2, 3)
                            AND DATE_FORMAT(
                                tb_transaction2.gate1_datetime,
                                \'%Y-%m-%d\'
                            ) = DATE(NOW())
                        ) i
                    WHERE
                        i.timediff12_min >= '.$timediff12_min.'
                    GROUP BY
                        i.card_id
                ) q1
            INNER JOIN (
                SELECT
                    tb_transaction2.trans_id AS trans_id,
                    tb_transaction2.card_id AS card_id,
                    tb_card.licenceplate_no AS licenceplate_no,
                    tb_card.card_owner_name AS card_owner_name,
                    tb_card.section_name AS section_name,
                    tb_card_type.card_type_id AS card_type_id,
                    tb_card_type.card_type_name AS card_type_name,
                    DATE_FORMAT(
                        DATE_ADD(
                            tb_transaction2.gate1_datetime,
                            INTERVAL 543 YEAR
                        ),
                        \'%d/%m/%Y %H:%i:%s\'
                    ) AS gate1_datetime,
                    COUNT(tb_transaction2.trans_id) AS On_tran
                FROM
                    tb_transaction2
                LEFT JOIN tb_card ON (
                    tb_card.card_id = tb_transaction2.card_id
                )
                LEFT JOIN tb_card_type ON (
                    tb_card_type.card_type_id = tb_card.card_type_id
                )
                WHERE
                    timediff(
                        tb_transaction2.gate3_datetime,
                        tb_transaction2.gate1_datetime
                    ) > "'.$time13format.'"
                AND DATE_FORMAT(
                    tb_transaction2.gate1_datetime,
                    \'%Y-%m-%d\'
                ) = DATE(NOW())
                GROUP BY
                    tb_transaction2.card_id
                ORDER BY
                    tb_transaction2.gate1_datetime
            ) q2 ON q1.trans_id = q2.trans_id')->queryAll();
        }
        return $this->render('_report_condition2', [
            'queryReportCondition2'  => $queryReportCondition2,
        ]);
    }
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "TbTransaction #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TbTransaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TbTransaction();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new TbTransaction",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new TbTransaction",
                    'content'=>'<span class="text-success">Create TbTransaction success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new TbTransaction",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->trans_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing TbTransaction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update TbTransaction #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "TbTransaction #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update TbTransaction #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->trans_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing TbTransaction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        \Yii::$app->getSession()->setFlash('success', 'Deleted!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the TbTransaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbTransaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbTransaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionDetailDashboard(){
        $query = (new Query())
        ->select([
            'tb_transaction.*',
            'tb_card.*',
            'tb_card_type.*'
        ])
        ->from('tb_transaction')
        ->leftJoin('tb_card','tb_card.card_id = tb_transaction.card_id')
        ->leftJoin('tb_card_type','tb_card_type.card_type_id = tb_card.card_type_id')
        ->all();
        $dataArray = [];
        foreach ($query as $key => $value) {
            $column = [];
            $column['order'] = $key+1;
            $column['card_id'] = $value['card_id'];
            $column['licenceplate_no'] = $value['licenceplate_no'];
            $column['card_owner_name'] = $value['card_owner_name'];
            $column['section_name'] = $value['section_name'];
            $column['card_type_name'] = $value['card_type_name'];
            $column['gate1_datetime'] = $value['gate1_datetime'];
            $column['gate2_datetime'] = $value['gate2_datetime'];
            $column['card_status'] = $value['card_status'];
            $dataArray[] = $column;
        }

        echo \yii\helpers\Json::encode(['data' => $dataArray]);
    }


     public function actionReturnExpand($card_id){
        $timediff13_hr = Yii::$app->db->createCommand('SELECT tb_config_report.gate13_timediff_hr FROM tb_config_report')->queryScalar();
        $time13format = "$timediff13_hr:00:00";
        $queryReportNightsate = Yii::$app->db->createCommand('SELECT
                tb_card.card_owner_name         AS card_owner_name,
                tb_transaction2.card_id          AS card_id,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate1_date,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate1_time,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y\') AS gate3_date,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%H:%i:%s\') AS gate3_time,
                DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS now,
                timediff( tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime) AS T13_hr,
                tb_transaction2.trans_status AS trans_status
            FROM
                (
                    (
                        tb_transaction2
                        JOIN tb_card ON (
                            ( tb_card.card_id = tb_transaction2.card_id
                            )
                        )
                    )
                    JOIN tb_card_type ON (
                        (
                            tb_card_type.card_type_id = tb_card.card_type_id
                        )
                    )
                )
            WHERE
                (
                    (
                        tb_transaction2.trans_status = 2
                    )
                    AND (
                        timediff(
                            tb_transaction2.gate3_datetime,
                            tb_transaction2.gate1_datetime
                        ) > "'.$time13format.'"
                    )
                    AND (tb_transaction2.card_id = '.$card_id.')
            )')
            ->queryAll();
        $request = Yii::$app->request;      
        if($request->isAjax){
            return $this->renderAjax('detail_report', [
                'queryReportNightsate'  => $queryReportNightsate,
                'card_id' => $card_id,
            ]);
        }
     }
     public function actionReturnExpandNonestate($card_id)
     {
        $timediff12_min = Yii::$app->db->createCommand('SELECT tb_config_report.gate12_timediff_min FROM tb_config_report')->queryScalar();
        $queryReportNonestate = Yii::$app->db->createCommand('SELECT
                tb_transaction2.card_id          AS card_id,
                tb_card.card_owner_name         AS card_owner_name,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate1_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate1_datetime,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate2_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate2_datetime,
                DATE_FORMAT(DATE_ADD(tb_transaction2.gate3_datetime, INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS gate3_datetime,
                DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 543 YEAR),\'%d/%m/%Y %H:%i:%s\') AS now,
                IF (
                    isnull(tb_transaction2.gate2_datetime),
                    timediff( tb_transaction2.gate3_datetime,tb_transaction2.gate1_datetime),
                    timediff( tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)
                ) AS T13_hr,
                IF (
                    isnull(tb_transaction2.gate2_datetime),
                    ceiling(
                            ( time_to_sec(
                                    timediff( tb_transaction2.gate3_datetime, tb_transaction2.gate1_datetime)) / 60
                            )
                        ),
                        ceiling(
                            ( time_to_sec(
                                    timediff( tb_transaction2.gate2_datetime,tb_transaction2.gate1_datetime)) / 60
                            )
                        )   
                ) AS timediff12_min,
                tb_transaction2.trans_status AS trans_status
            FROM
                (
                    (
                        tb_transaction2
                        JOIN tb_card ON (
                            (
                                tb_card.card_id = tb_transaction2.card_id
                            )
                        )
                    )
                )
            WHERE
                (
                    (
                        tb_card.card_type_id IN (2, 3)
                    )
                    AND (
                    tb_transaction2.card_id = '.$card_id.'
                    )
                )
            HAVING (timediff12_min >= '.$timediff12_min.')')
            ->queryAll();
        $request = Yii::$app->request;      
        if($request->isAjax){
            return $this->renderAjax('detail_report_nonestate', [
                'queryReportNonestate'  => $queryReportNonestate,
                'card_id' => $card_id,
            ]);
        }
     }
     public function actionPrintreport($type,$card_id) {
        $request = Yii::$app->request;
        if($request->isGet){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($type=='nightstate'){
                $title = 'รายงานสถิติการจอดรถค้างคืน';
            }elseif($type=='nonestate'){
                $title = 'รายงานสถิติการจอดรถไม่ตรงตามพื้นที่จอด';
            }elseif($type=='balance'){
                $title = 'รายงานยอดคงเหลือสินค้า';
            }
            return [
                    'title'=> $title. '#' .$card_id,
                    'content'=>$this->renderAjax('_modal_report', [
                        'type' => $type,
                        'card_id' => $card_id
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-rigth','data-dismiss'=>"modal"]).Html::button('print',['class'=>'btn btn-success pull-rigth','onclick'=>'print();'])
                ];    
        }else{
            return $this->render('_modal_report', [
                
            ]);
        }
    }
    function actionPrintPdf($type,$card_id=null,$date_range=null) {
        if($type=='nightstate'){
            $data = $this->spritDate($date_range);
            $start_date = $data['start_date'];
            $end_date = $data['end_date'];
            $name = 'รายงานสถิติการจอดรถค้างคืน';
            $model = ReportQuery::getReportNightstate($card_id,$start_date.' 00:00:00',$end_date.' 23:59:59');
        }elseif($type=='nonestate'){
            $data = $this->spritDate($date_range);
            $start_date = $data['start_date'];
            $end_date = $data['end_date'];
            $name = 'รายงานสถิติการจอดรถไม่ตรงตามพื้นที่จอด';
            $model = ReportQuery::getReportNonestate($card_id,$start_date.' 00:00:00',$end_date.' 23:59:59');
        }elseif($type=='balance'){
            $model = ReportQuery::getReportBalance($stk_id);
            $start_date = null;
            $end_date = null;
        }
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => $name.'.pdf',
            'content' => $this->renderPartial('_report', [
                'model' =>   $model,
                'type' => $type,
                'card_id' => $card_id,
                'start_date' => $start_date,
                'end_date' => $end_date,
            ]),
            'marginTop' => '15',
            'marginBottom' => '15',
            'marginLeft' => '20',
            'marginRight' => '15',
            'options' => [
                'title' => $name.'.pdf',
                'defaultheaderline' => 0,
                'defaultfooterline' => 0,
            ],
            'methods' => [
                   'SetHeader'=> 'หน้า {PAGENO} / {nbpg}',
                   'SetFooter' => $this->footerReport(),
            ]
        ]);
        return $pdf->render();
    }
    private function footerReport($size=12) {
        $footer = '<table width="100%" style"border-top: 1px solid black;">
                    <tr>
                        <td class="text-left" width="50%" style="font-size:' . $size . 'pt;">ระบบจัดการลานจอดรถยนต์ อาคารเฉลิมพระเกียรติ โรงพยาบาลราชวิถี 
                        </td>
                        <td class="text-right" width="50%" style="font-size:' . $size . 'pt;">
                        ' . 'Print:' .date('d/m/Y').'
                        </td>
                    </tr>
                  </table>';
        return $footer;
    }
    private function spritDate($date)
    {
        $data = explode(' ', $date);
        return $date = [
            'start_date' => $this->convertThaiToMysqlDate($data[0]),
            'end_date' => $this->convertThaiToMysqlDate($data[2]),
        ];
    }
    private function convertThaiToMysqlDate($date=null) {
        //แปลงวันที่ลง mysql
        if(!empty($date)){
            $arr = explode("/", $date);
            if($arr[2]>'2000'&&$arr[2]<'2500'){
                $y = $arr[2];
            }else if($arr[2]>'2500'){
                $y = ($arr[2] - 543);
            }
            if($arr[1]<10){
               $m = '0'.intval($arr[1]); 
            }else{
               $m = $arr[1]; 
            }
            if($arr[0]<10){
               $d = '0'.intval($arr[0]); 
            }else{
               $d = $arr[0]; 
            }
            return "$y-$m-$d";
        }else{
            return "0000-00-00";
        }
        
    }
    function actionPrintquery($id) {
        if($id == '1'){
            $name = 'รายงานสถิติการจอดรถ';
            $content = '_report_InOut';
            $model = ReportQuery::getqueryIN_OUT();
        }else if($id == '2'){
            $name = 'รายงานสถิติการจอดรถบุคลากร';
            $model = ReportQuery::getqueryMember();
            $content = '_report_member';
        }else if($id == '3'){
            $name = 'รายงานสถิติการจอดรถผู้มารับบริการ';
            $model = ReportQuery::getqueryVisitor();
            $content = '_report_visitor';
        }else if($id == '4'){
            $name = 'รายงานรถออกจากพื้นที่จอด';
            $model = ReportQuery::getqueryOut();
            $content = '_report_out';
        }else if($id == '5'){
            $name = 'รายงานสถิติการจอดรถบุคลากร';
            $model = ReportQuery::getqueryIn();
            $content = '_report_in';
        }else if($id == '6'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรที่ผ่านชั้น 1';
            $model = ReportQuery::getqueryNow_member();
            $content = '_report_nowmember';
        }else if($id == '7'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรที่มีช่องจอดประจำที่ผ่านชั้น 1';
            $model = ReportQuery::getqueryNow_member2();
            $content = '_report_nowmember2';
        }else if($id == '8'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรที่ไม่มีช่องจอดประจำที่ผ่านชั้น 1';
            $model = ReportQuery::getqueryNow_member34();
            $content = '_report_nowmember34';
        }else if($id == '9'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรที่ผ่านชั้น 4B';
            $model = ReportQuery::getqueryNow_Gate2();
            $content = '_report_gate2';
        }else if($id == '10'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรที่มีช่องจอดประจำที่ผ่านชั้น 4B';
            $model = ReportQuery::getqueryNow_Gate2_2();
            $content = '_report_gate2_2';
        }else if($id == '11'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรที่ไม่มีช่องจอดประจำที่ผ่านชั้น 4B';
            $model = ReportQuery::getqueryNow_Gate2_34();
            $content = '_report_gate2_34';
        }else if($id == '12'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรจอดไม่ตรงพื้นที่';
            $model = ReportQuery::getqueryNone();
            $content = '_report_none';
        }else if($id == '13'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรมีช่องจอดประจำจอดไม่ตรงพื้นที่';
            $model = ReportQuery::getqueryNone2();
            $content = '_report_none2';
        }else if($id == '14'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรไม่มีช่องจอดประจำจอดไม่ตรงพื้นที่';
            $model = ReportQuery::getqueryNone34();
            $content = '_report_none34';
        }else if($id == '15'){
            $name = 'รายงานรถบุคลากรออกจากพื้นที่จอด';
            $model = ReportQuery::getqueryNow_memberout();
            $content = '_report_memberout';
        }else if($id == '16'){
            $name = 'รายงานรถบุคลากรมีช่องจอดประจำออกจากพื้นที่จอด';
            $model = ReportQuery::getqueryNow_memberout2();
            $content = '_report_memberout2';
        }else if($id == '17'){
            $name = 'รายงานรถบุคลากรไม่มีช่องจอดประจำออกจากพื้นที่จอด';
            $model = ReportQuery::getqueryNow_memberout34();
            $content = '_report_memberout34';
        }else if($id == '18'){
            $name = 'รายงานแสดงข้อมูลรถผู้มารับบริการเข้าจอดในพื้นที่จอด';
            $model = ReportQuery::getqueryNow_visitor();
            $content = '_report_nowvisitor';
        }else if($id == '19'){
            $name = 'รายงานแสดงข้อมูลรถผู้มารับบริการออกจากพื้นที่จอด';
            $model = ReportQuery::getqueryNow_visitorout();
            $content = '_report_visitorout';
        }else if($id == '20'){
            $name = 'รายงานแสดงข้อมูลรถผู้มารับบริการที่ยังจอดในพื้นที่จอด';
            $model = ReportQuery::getqueryNow_visitorin();
            $content = '_report_visitorin';
        }else if($id == '21'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรทางการแพทย์(พื้นที่สีเหลือง)  ที่ผ่านชั้น 1';
            $model = ReportQuery::getqueryNow_Doctor();
            $content = '_report_nowdoctor';
        }else if($id == '22'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรทางการแพทย์(พื้นที่สีเหลือง)  ที่ผ่านชั้น 4B';
            $model = ReportQuery::getqueryNow_Gate2_Doctor();
            $content = '_report_gate2_doctor';
        }else if($id == '23'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรทางการแพทย์(พื้นที่สีเหลือง) จอดไม่ตรงพื้นที่';
            $model = ReportQuery::getqueryNoneDoctor();
            $content = '_report_nonedoctor';
        }else if($id == '24'){
            $name = 'รายงานแสดงข้อมูลรถบุคลากรทางการแพทย์(พื้นที่สีเหลือง) ออกจากพื้นที่จอด';
            $model = ReportQuery::getqueryNow_doctorout();
            $content = '_report_doctorout';
        }else if($id == '0_1'){
            $name = 'รายงานสรุปการใช้พื้นที่จอดรถ';
            $model = ReportQuery::getqueryNow_visitorin();
            $content = '_report_visitorin';
        }
        $model = $model;
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => $name.'.pdf',
            'content' => $this->renderPartial($content, [
                'model' =>  $model,
            ]),
            'marginTop' => '15',
            'marginBottom' => '20',
            'marginLeft' => '10',
            'marginRight' => '10',
            'options' => [
                'title' => $name.'.pdf',
                'defaultheaderline' => 0,
                'defaultfooterline' => 0,
            ],
            'methods' => [
                   'SetHeader'=> 'หน้า {PAGENO} / {nbpg}',
                   'SetFooter' => [
                    '<table border="0" width="100%" style="border-top: 1px solid black;">'
                    . '<tr>'
                    . '<td style="text-align:left;font-size:12pt;">ระบบจัดการลานจอดรถยนต์ อาคารเฉลิมพระเกียรติ โรงพยาบาลราชวิถี</td>'
                    . '<td style="text-align:right;font-size:12pt;">Print:' .date('d/m/Y'). '</td>'
                    . '</tr>'
                    . '</table>'
                ],
            ]
        ]);
        return $pdf->render();
    }
    public function actionPrintreportBydate() {
        $request = Yii::$app->request;
        if($request->isGet){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $title = 'รายงานสรุปการใช้พื้นที่จอดรถ';
            return [
                    'title'=> $title,
                    'content'=>$this->renderAjax('_modal_report_bydate', [
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-rigth','data-dismiss'=>"modal"]).Html::button('print',['class'=>'btn btn-success pull-rigth','onclick'=>'print();'])
                ];    
        }else{
            return $this->render('_modal_report_bydate', [
                
            ]);
        }
    }
    public function actionPrintighchart_member() {
        $request = Yii::$app->request;
        if($request->isGet){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $title = 'สถิติการจอดรถยนต์ บุคลากร';
            return [
                    'title'=> $title,
                    'content'=>$this->renderAjax('_modal_highchart', [
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-rigth','data-dismiss'=>"modal"]).Html::button('print',['class'=>'btn btn-success pull-rigth','onclick'=>'print();'])
                ];    
        }else{
            return $this->render('_modal_highchart', [
                
            ]);
        }
    }
    public function actionPrintighchart_visitor() {
        $request = Yii::$app->request;
        if($request->isGet){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $title = 'สถิติการจอดรถยนต์ ผู้มารับบริการ';
            return [
                    'title'=> $title,
                    'content'=>$this->renderAjax('_modal_highchart', [
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-rigth','data-dismiss'=>"modal"]).Html::button('print',['class'=>'btn btn-success pull-rigth','onclick'=>'print();'])
                ];    
        }else{
            return $this->render('_modal_highchart', [
                
            ]);
        }
    }
    function actionPrintAll($date_range=null) {
        $data = $this->spritDate($date_range);
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        //print_r($start_date.' 00:00:00');
        $model = $this->actionGetDatareport($start_date,$end_date);
        $name = 'รายงานสรุปการใช้พื้นที่จอดรถ';
        $content = '_report_all';
        //$model = ReportQuery::getqueryIN_OUT();
        $model = $model;
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => $name.'.pdf',
            'content' => $this->renderPartial($content, [
                'model' =>  $model,
                'start_date' => $start_date,
                'end_date' => $end_date,
            ]),
            'marginTop' => '10',
            'marginBottom' => '10',
            'marginLeft' => '10',
            'marginRight' => '10',
            'options' => [
                'title' => $name.'.pdf',
                'defaultheaderline' => 0,
                'defaultfooterline' => 0,
            ],
            'methods' => [
                   //'SetHeader'=> 'หน้า {PAGENO} / {nbpg}',
                   'SetFooter' => $this->footerReport(),
            ]
        ]);
        return $pdf->render();
    }
    public function actionGetDatareport($start_date,$end_date)
    {
    Yii::$app->response->format = Response::FORMAT_JSON;
    $start_date = $start_date.' 00:00:00';
    $end_date = $end_date.' 23:59:59';
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //รถเข้าชั้น1 ปัจจุบัน
        $queryIN =  Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction
            WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"')->queryAll();
         $queryBalance_member =  Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction
            LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
            WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            AND tb_card.card_type_id IN (1,2,3,4)')->queryAll();
        $queryOut =  Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction2
            LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
            WHERE tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            AND tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            AND tb_card.card_type_id IN (1,2,3,4)')->queryAll();
        // รถเข้าชั้น1 ทั้งหมด
        $queryIN_OUT =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (1,2,3,4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (1,2,3,4)')->queryAll();
        $queryIN_OUT234 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2,3,4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2,3,4)')->queryAll();
        $queryIN_OUT1 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (1)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (1)')->queryAll();
        $queryNone_23 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction.gate2_datetime IS NULL
                AND tb_card.card_type_id IN (2, 3)
            UNION 
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate2_datetime IS NULL
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2, 3)')->queryAll();
        // รถเข้าชั้น 1 ของบุคลากร
        $queryNow_member =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2, 3, 4)
            UNION 
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2, 3, 4)')->queryAll();
        // รถเข้าชั้น 1 ของบุคลากรมีที่จอด
        $queryNow_member2 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2)')->queryAll();
        // รถเข้าชั้น 1 ของบุคลากรไม่มีที่จอด
        $queryNow_member34 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (3)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (3)')->queryAll();
        $queryNow_Doctor =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (4)')->queryAll();
        // รถเข้าชั้น 1 ของผู้รับบริการ
        $queryNow_visitor =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (1)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (1)')->queryAll();
        // รถเข้าชั้น 4B ของบุคลากร
        $queryNow_Gate2 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate2_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2,3)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate2_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2,3)')->queryAll();
        // รถเข้าชั้น 4B ของบุคลากรมีที่จอด
        $queryNow_Gate2_2 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate2_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate2_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2)')->queryAll();
        // รถเข้าชั้น 4B ของบุคลากรไม่มีที่จอด
        $queryNow_Gate2_34 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate2_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (3)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate2_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (3)')->queryAll();
        $queryNow_Gate2_Doctor =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction
                LEFT JOIN tb_card ON tb_transaction.card_id = tb_card.card_id
                WHERE tb_transaction.gate2_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_transaction.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (4)
            UNION
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate2_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                AND tb_card.card_type_id IN (4)')->queryAll();
        // รถออกของบุคลากร
        $queryNow_Gate3_member =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2, 3, 4)')->queryAll();
        $queryNow_Gate3_member2 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_card.card_type_id IN (2)')->queryAll();
        $queryNow_Gate3_member3 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_card.card_type_id IN (3)')->queryAll();
        $queryNow_Gate3_member4 =  Yii::$app->db->createCommand('
                SELECT * FROM tb_transaction2
                LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
                WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'"  AND "'.$end_date.'"
                AND tb_card.card_type_id IN (4)')->queryAll();
        // รถออกของผู้รับบริการ
        $queryNow_Gate3_visitor =  Yii::$app->db->createCommand('
            SELECT * FROM tb_transaction2
            LEFT JOIN tb_card ON tb_transaction2.card_id = tb_card.card_id
            WHERE tb_transaction2.gate3_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            AND tb_transaction2.gate1_datetime BETWEEN "'.$start_date.'" AND "'.$end_date.'"
            AND tb_card.card_type_id IN (1)')->queryAll();    
        return [
            #row1
            //'queryNow'                  => count($queryNow),
            'queryNow_member'           => count($queryNow_member),
            'queryNow_member34'         => count($queryNow_member34),
            'queryNow_member2'          => count($queryNow_member2),
            'queryNow_visitor'          => count($queryNow_visitor),
            'queryNow_Gate2_2'          => count($queryNow_Gate2_2),
            'queryNow_Gate2_34'         => count($queryNow_Gate2_34),
            'queryNow_Gate2_Doctor'     => count($queryNow_Gate2_Doctor),
            'queryNow_Gate3_member'     => count($queryNow_Gate3_member),
            'queryNow_Gate3_member2'    => count($queryNow_Gate3_member2),
            'queryNow_Gate3_member3'    => count($queryNow_Gate3_member3),
            'queryNow_Gate3_member4'    => count($queryNow_Gate3_member4),
            'queryNow_Gate3_visitor'    => count($queryNow_Gate3_visitor),
            'queryNow_Doctor'           => count($queryNow_Doctor),
            'queryIN_OUT'               => count($queryIN_OUT),
            'queryNow_Gate2'            => count($queryNow_Gate2),
            'queryBalance_member'       => count($queryBalance_member),
            'queryBalance_visitor'      => (count($queryNow_visitor) - count($queryNow_Gate3_visitor)),
            'queryNone'                 => (count($queryNone_23) + count($queryNow_Gate2_Doctor)),
            'queryNone_2'               => (count($queryNow_member2) - count($queryNow_Gate2_2)),
            'queryNone_34'              => (count($queryNow_member34) - count($queryNow_Gate2_34)),
            'queryOut'                  => count($queryOut),
            'queryIN_OUT234'            => count($queryIN_OUT234),
            'queryIN_OUT1'              => count($queryIN_OUT1),
        ];
    }
}
