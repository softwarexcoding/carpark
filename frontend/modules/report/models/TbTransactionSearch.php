<?php

namespace frontend\modules\report\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\report\models\TbTransaction;

/**
 * TbTransactionSearch represents the model behind the search form about `frontend\modules\report\models\TbTransaction`.
 */
class TbTransactionSearch extends TbTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trans_id', 'disc_type', 'trans_status', 'createby', 'updateby'], 'integer'],
            [['card_id', 'licenceplate_no', 'gate1_datetime', 'gate2_datetime', 'gate3_datetime', 'invoice_no', 'createat', 'updateat', 'total_time'], 'safe'],
            [['total_min', 'fee_amt', 'disc_amt', 'disc_time', 'total_amt', 'total_paid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'trans_id' => $this->trans_id,
            'gate1_datetime' => $this->gate1_datetime,
            'gate2_datetime' => $this->gate2_datetime,
            'gate3_datetime' => $this->gate3_datetime,
            'total_min' => $this->total_min,
            'fee_amt' => $this->fee_amt,
            'disc_type' => $this->disc_type,
            'disc_amt' => $this->disc_amt,
            'disc_time' => $this->disc_time,
            'total_amt' => $this->total_amt,
            'total_paid' => $this->total_paid,
            'trans_status' => $this->trans_status,
            'createby' => $this->createby,
            'updateby' => $this->updateby,
            'createat' => $this->createat,
            'updateat' => $this->updateat,
            'total_time' => $this->total_time,
        ]);

        $query->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'licenceplate_no', $this->licenceplate_no])
            ->andFilterWhere(['like', 'invoice_no', $this->invoice_no]);

        return $dataProvider;
    }
}
