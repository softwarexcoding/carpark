<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use msoft\widgets\Select2;
use msoft\widgets\DateRangePicker;
use frontend\modules\report\components\ReportQuery;
?>
<?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL,'id'=>'form_report']); ?>  
    <div class="form-group" style="margin-top: 15px;">    
        <?= Html::label('ช่วงเวลา', 'ช่วงเวลา', ['class' => 'col-sm-2 control-label no-padding-right']) ?>
        <div class="col-sm-9">
            <?php
            echo '<div class="drp-container">';
            echo Select2::widget([
                //'model' => $model,
                'attribute' => 'state_2',
                'data' => [
                        'all' => "ไม่มีเงื่อนไข", 
                        'day' => "เงื่อนไขตามวัน", 
                        'date' => "เงื่อนไขตามวันที่",
                    ],
                'name' => 'ss',
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            echo '</div>';
            ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
	function print(){
	   var myWindow = window.open("print-all?date_range="+$('#date_range').val(),"", "top=100,left=200,width=" + (screen.width - '400') + ",height=300,right=auto");
        myWindow.window.print();
	}
</script>
