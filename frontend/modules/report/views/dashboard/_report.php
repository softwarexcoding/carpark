<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\modules\report\components\ReportQuery;
if($type=='nightstate'){
    $title = 'รายงานสถิติจอดรถค้างคืน';
    $header = ReportQuery::getCard_owner_name($card_id);
}elseif($type=='nonestate'){
    $title = 'รายงานสถิติการจอดรถไม่ตรงตามพื้นที่จอด';
    $header = ReportQuery::getCard_owner_name($card_id);
}elseif($type=='balance'){
    $title = 'รายงานยอดคงเหลือสินค้า';
    $date = date('d/m/Y');
}


$result = ArrayHelper::index($model,null,'stk_id');
//******Setting style culumn pdf****** 
$font_heading =  'style="text-align:center;font-size:24pt"';
$font_nomal = 'style="text-align:center;font-size:16pt"';
$column_th_style ='style="text-align:center;font-size:16pt;border-top: 1px solid black;border-bottom: 1px solid black;padding-top: 10px;"';
$column_th_style_rigth ='style="text-align:right;font-size:16pt;border-top: 1px solid black;border-bottom: 1px solid black;padding-top: 10px;"';
$column_td_style_center = 'style="font-size:16pt;text-align:center;vertical-align: top;"';
$column_td_style_right = 'style="font-size:16pt;text-align: right;vertical-align: top;"';
$column_td_style_left = 'style="font-size:16pt;vertical-align: top;"';
//******Setting style culumn pdf****** 
$i = 0;
?>
<table width="100%">
    <tr>
        <td <?= $font_nomal ?> width="33%">
            <?= Html::img('@frontend/modules/report/views/dashboard/images/logo_print.png', ['alt' => 'My logo','height' => '100px']) ?>
        </td>
        <td <?= $font_nomal ?> width="33%">
        </td>
        <td <?= $font_heading ?> width="33%">
            <strong><?= $title; ?></strong>
        </td>
    <tr>
</table>
<table border="0"  width="100%">
    <tr >
        <td <?= $font_nomal ?> style="text-align:left;">    
            <strong><?= Html::label('ชื่อผู้ถือบัตร : ') ?></strong><?= $header->card_owner_name ?>
        </td>
    <tr> 
</table>
<table border="0"  width="100%">
    <tr >
        <td <?= $font_nomal ?> style="text-align:left;">    
            <strong><?= Html::label('หมายเลขบัตร : ') ?></strong><?= $header->card_id ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <strong><?= Html::label('ทะเบียนรถ : ') ?></strong><?= $header->licenceplate_no?>
        </td>
    <tr> 
</table>
<table border="0"  width="100%">
    <tr width="50%">
        
        <td <?= $font_nomal ?> style="text-align:left;">    
            <strong><?= Html::label('หน่วยงาน : ') ?></strong><?= $header->section_name ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <strong><?= Html::label('ประเภทบัตร : ') ?></strong><?= ReportQuery::getCard_type($header->card_type_id) ?>
        </td>
    <tr> 
</table>
<br>
<table border="0"  width="100%">
    <tr width="50%">
        <td <?= $font_nomal ?> style="text-align:right;">    
                <strong><?= Html::label('จากวันที่ ') ?></strong><?= Yii::$app->formatter->asDate($start_date, 'dd/MM/yyyy') ?>
                <strong><?= Html::label('ถึงวันที่ ') ?></strong><?= Yii::$app->formatter->asDate($end_date, 'dd/MM/yyyy') ?>
        </td>
    <tr> 
</table>
<br>
<table border="0" cellpadding="0" cellspacing="0"  width="100%"> 
    <thead>
        <tr>
            <th <?= $column_th_style ?> width="10%">
                <strong>#</strong>
            </th>
            <th <?= $column_th_style ?> width="30%">
                <strong>เวลาเข้า</strong>
            </th>
            <th <?= $column_th_style ?> width="30%">
                <strong>เวลาออก</strong>
            </th>
            <th <?= $column_th_style ?> width="30%">
                <strong>ระยะเวลาจอด</strong>
            </th>
        </tr>
    </thead>
        <?php foreach ($model as $row) : ?>
        <tr>    
            <td  <?= $column_td_style_center?> >
                <?= $i+1; ?>
            </td>
            <td  <?= $column_td_style_center?> >
                <?= $row['gate1_datetime'] ?>
            </td>
            <td  <?= $column_td_style_center ?> >
                <?= $row['gate3_datetime'] ?>
            </td>
            <td  <?= $column_td_style_center?>>
                <?= $row['T13_hr'] ?>
            </td>
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?> 
</table>
<table width="100%" border="0" style="border-top: 1px solid black;border-bottom: 1px solid black;">
    <tr>
        <td width="57%" style="font-size:16pt;">
        	<strong>รวมทั้งสิ้น <?= $i; ?> รายการ</strong>
        </td>
        <td>
        </td>
        <td  style="padding-left:30px">
        </td>
    </tr>
</table>

