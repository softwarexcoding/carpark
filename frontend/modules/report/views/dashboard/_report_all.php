<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\modules\report\components\ReportQuery;
$title = 'รายงานสรุปการใช้งานพื้นที่จอดรถ';


$result = ArrayHelper::index($model,null,'stk_id');
//******Setting style culumn pdf****** 
$font_heading =  'style="text-align:center;font-size:20pt"';
$font_nomal = 'style="text-align:center;font-size:16pt"';
$column_th_style ='style="text-align:center;font-size:16pt;border-top: 1px solid black;border-bottom: 1px solid black;padding-top: 5px;padding-bottom: 5px;"';
$column_th_style_rigth ='style="text-align:right;font-size:16pt;border-top: 1px solid black;border-bottom: 1px solid black;padding-top: 10px;"';
$column_td_style_center = 'style="font-size:16pt;text-align:center;vertical-align: top;"';
$column_td_style_right = 'style="font-size:16pt;text-align: right;vertical-align: top;"';
$column_td_style_left = 'style="font-size:16pt;vertical-align: top;"';
//******Setting style culumn pdf****** 
$i = 0;
?>
<table width="100%" border="0" >
    <tr>
        <td style="text-align:center;font-size:16pt" width="20%">
            <?= Html::img('@frontend/modules/report/views/dashboard/images/logo/logo_print.png', ['alt' => 'My logo','height' => '80px']) ?>
        </td>
        <td style="text-align:right;font-size:16pt" width="80%">  
            <strong><?= $title; ?></strong>
            <br>  
            <strong><?= Html::label('วันที่') ?></strong>
                <?= \metronic\components\DateConvert::convertToLogicalDateOnly($start_date); ?>
            <strong><?= Html::label(' ถึง ') ?></strong>
                <?= \metronic\components\DateConvert::convertToLogicalDateOnly($end_date); ?>
        </td>
    </tr>
</table>
<table border="0"  width="100%" style="border-top: 1px solid black;">
    <tr width="100%">
        <td style="text-align:center;font-size:18pt;" colspan="3">     
            <strong><?= Html::label('สถิติการจอดรถสูงสุด ') ?></strong>
        </td>
    </tr>
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถเข้าจอดทั้งหมด ') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryIN_OUT']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%">
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรทั้งหมด') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryIN_OUT234']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถผู้มารับบริการทั้งหมด') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryIN_OUT1']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
</table>
<table border="0"  width="100%" >
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถออกจากพื้นที่จอดทั้งหมด ') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryOut']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%"> 
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถที่ยังอยู่ในพื้นที่จอด ') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryBalance_member']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%"  style="border-top: 1px solid black;">
    <tr width="100%">
        <td style="text-align:center;font-size:18pt;" colspan="3">     
            <strong><?= Html::label('สถิติการจอดแยกตามประเภทของบุคลากร ') ?></strong>
        </td>
    </tr>
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถบุคลากรผ่านประตูชั้น 1 ทั้งหมด ') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryNow_member']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%">
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%"></td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรมีพื้นที่จอดประจำ') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_member2']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรไม่มีพื้นที่จอดประจำ') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_member34']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรทางการแพทย์') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_Doctor']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
</table>
<table border="0"  width="100%"  style="border-top: 1px solid gray;">
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถบุคลากรผ่านประตูชั้น 4B ทั้งหมด ') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryNow_Gate2']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%">
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรมีพื้นที่จอดประจำ') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_Gate2_2']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรไม่มีพื้นที่จอดประจำ') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_Gate2_34']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรทางการแพทย์') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_Gate2_Doctor']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
</table> 
<table border="0"  width="100%" style="border-top: 1px solid gray;">
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถบุคลากรจอดไม่ตรงตามพื้นที่') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryNone']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%">
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรมีพื้นที่จอดประจำ') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNone_2']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรไม่มีพื้นที่จอดประจำ') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNone_34']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%">    
                
            </td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรทางการแพทย์') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_Gate2_Doctor']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
</table>
<table border="0"  width="100%"  style="border-top: 1px solid gray;">
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถบุคลากรออกจากพื้นที่จอดรถ') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryNow_Gate3_member']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%">
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%"></td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรมีพื้นที่จอดประจำ') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_Gate3_member2']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%"></td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรไม่มีพื้นที่จอดประจำ') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_Gate3_member3']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
        <tr width="100%">
            <td <?= $font_nomal ?> style="text-align:left;width: 10%"></td>
            <td <?= $font_nomal ?> style="text-align:left;width: 60%">    
                <?= Html::label('จำนวนรถบุคลากรทางการแพทย์') ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= $model['queryNow_Gate3_member4']; ?>
            </td>
            <td <?= $font_nomal ?> style="text-align:right;">    
                <?= Html::label('คัน') ?>
            </td>
        </tr>
</table>
<table border="0"  width="100%"  style="border-top: 1px solid black;">
    <tr width="100%" >
        <td style="text-align:center;font-size:18pt;" colspan="3">    
            <strong><?= Html::label('สถิติการจอดของผู้มารับบริการ ') ?></strong>
        </td>
    </tr>
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถผู้รับบริการเข้าพื้นที่จอดรถทั้งหมด') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryNow_visitor']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%">
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถผู้รับบริการออกจากพื้นที่จอดรถทั้งหมด') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryNow_Gate3_visitor']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>
<table border="0"  width="100%" style="border-bottom: 1px solid black;">
    <tr width="100%">
        <td <?= $font_nomal ?> style="text-align:left;width: 70%">    
            <strong><?= Html::label('จำนวนรถผู้รับบริการยังอยู่ในพื้นที่จอดรถทั้งหมด') ?></strong>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= $model['queryBalance_visitor']; ?>
        </td>
        <td <?= $font_nomal ?> style="text-align:right;">    
            <?= Html::label('คัน') ?>
        </td>
    </tr> 
</table>

