<?php
use yii\data\ArrayDataProvider;
use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;
use yii\helpers\Json;
use metronic\widgets\portlet\PortletBox;
use yii\widgets\Pjax;
use msoft\widgets\select2\Select2Asset;
use msoft\widgets\DateRangePicker;
use msoft\mpdf\Pdf;
use msoft\widgets\CheckboxX;
use msoft\widgets\ActiveForm;
use yii\helpers\Url;
use metronic\components\DateConvert;
Select2Asset::register($this)->css[] = 'css/select2-krajee.css';
RegisterJS::regis(['sweetalert'],$this);
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
    .table-scrollable{
        overflow-x: hidden !important;
    }
');
$this->registerJs('$("#tab_D").addClass("active");');
$this->title = 'รายงานสถิติการจอดรถค้างคืน';
$this->params['breadcrumbs'][] = $this->title;
$classmidle = ['class' => 'kv-align-center kv-align-middle','style' => 'color:black'];
$provider = new ArrayDataProvider([
    'allModels' => [],
    'pagination' => [
        'pageSize' => 100,
    ],
]);
$this->registerCss("
    td.details-control {
        background: url('/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('/images/details_close.png') no-repeat center center;
    }
");
$value_from = isset($_POST['from_date'])?DateConvert::convertToLogical2(DateConvert::convertDate($_POST['from_date'])):Yii::$app->formatter->asDate('now', 'php:d/m/'.date('Y'));
$value_to = isset($_POST['to_date'])?DateConvert::convertToLogical2(DateConvert::convertDate($_POST['to_date'])):Yii::$app->formatter->asDate('now', 'php:d/m/'.date('Y'));
?>
<?= SwalAlert::widget(); ?>

<div class="tab-content">
<?php echo $this->render('_tab'); ?>
            <div class="portlet light bordered">
            <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cube font-dark"></i>
                <span class="caption-subject bold uppercase"><?= $this->title ?></span>
                <span class="caption-helper"></span>
            </div>
            <div class="actions" style="width: 50%;">
                    <div class="form-group">
                        <div class="col-sm-12" style="text-align: right;">
                            <?= CheckboxX::widget([
                                'name'=> '1',
                                'options'=>['id'=> '1',],
                                'pluginOptions'=>['size'=>'md','threeState' => false,],
                                'pluginEvents' => [
                                    "change" => "function() { Checkboxx.S_ByUser($(this).val()); }",
                                ],
                            ]),Html::encode('ผู้มารับบริการ'),

                            CheckboxX::widget([
                                'name'=> '2',
                                'options'=>['id'=> '2',],
                                'pluginOptions'=>['size'=>'md','threeState' => false,],
                                'pluginEvents' => [
                                    "change" => "function() { Checkboxx.S_ByDoctor($(this).val()); }",
                                ],
                            ]),Html::encode('บุคลากร')?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'action' => Url::to(['report-nightstate']),
                'method' => 'POST',
                'id' => 'wait',
            ]); ?>
            <div class="form-group">
                <div class="col-sm-2" style="text-align:right;margin-top: 5px;">แสดงข้อมูลของวันที่</div>
                <div class="col-sm-4" style="text-align: left;">
                <?php
                    echo '<div class="drp-container">';
                    echo DateRangePicker::widget([
                        'name' => 'date_range_2',
                        //'presetDropdown' => true,
                        'hideInput' => true,
                        'startAttribute' => 'from_date',
                        'endAttribute' => 'to_date',
                        'startInputOptions' => ['value' => $value_from],
                        'endInputOptions' => ['value' => $value_to],
                        'pluginOptions' => [
                            'initRangeExpr' => true,
                            'ranges' => [
                                "วันนี้" => ["moment().startOf('day')", "moment()"],
                                "เมื่อวาน" => ["moment().subtract(1, 'day').startOf('day')", "moment().subtract(1, 'day').endOf('day')"],
                                "เดือนนี้" => ["moment().startOf('month')", "moment().endOf('month')"],
                                "เดือนที่แล้ว" => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                                "ปีที่แล้ว" => ["moment().subtract(1, 'year').startOf('year')", "moment().subtract(1, 'year').endOf('year')"],
                                "ปีนี้" => ["moment().startOf('year')", "moment().endOf('year')"],
                            ],
                            'locale' => [
                                'format' => 'D/M/Y',
                                'separator' => ' ถึง ',
                            ],
                        ]
                    ]);
                    echo '</div>';
                    ?>
                </div>
                <?= Html::submitButton(Icon::show('search').'ค้นหา', ['class' => 'btn btn-primary']) ?> 
            </div>
            <?php ActiveForm::end(); ?>
            <br><br><br>
            <div class="portlet-body">
            <?= Datatables::widget([
                'dataProvider' => $provider,
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'afterHeader' => [
                    ['columns' => [
                        ['content' => '','options' => ['class' => 'skip-export','colspan' => 4]],
                        ['content' => '','options' => ['class' => 'skip-export',]],
                        ['content' => '','options' => ['class' => 'skip-export',]],
                        ['content' => '','options' => ['class' => 'skip-export','colspan' => 4]],
                    ]]
                ],
                'tableOptions' => ['id' => 'reportnightstate'],
                'hover' => false,
                'bordered' => false,
                'condensed' => true,
                'striped' => true,
                'responsive' => false,
                'export' => [
                    'label' => 'รายงาน',
                    'target' => '_blank',
                    'showConfirmAlert' => false
                ],
                'layout' => '{items}',
                'exportConfig' => [
                    GridView::EXCEL => [
                        'label' => Yii::t('app', 'Excel'),
                        'icon' => 'file-excel-o',
                        'iconOptions' => ['class' => 'text-success'],
                        'showHeader' => true,
                        'showPageSummary' => true,
                        'showFooter' => true,
                        'showCaption' => true,
                        'filename' => Yii::t('app', 'grid-export'),
                        'alertMsg' => Yii::t('app', 'The EXCEL export file will be generated for download.'),
                        'options' => ['title' => Yii::t('app', 'Microsoft Excel 95+')],
                        'mime' => 'application/vnd.ms-excel',
                        'config' => [
                            'worksheet' => Yii::t('app', 'ExportWorksheet'),
                            'cssFile' => '',
                        ],
                    ],
                    //GridView::CSV => ['label' => 'Save as CSV'],
                    GridView::PDF => [
                        
                        'label' => Yii::t('app', 'PDF'),
                        'icon' => 'file-pdf-o',
                        'iconOptions' => ['class' => 'text-danger'],
                        'showHeader' => true,
                        'showPageSummary' => true,
                        'showFooter' => true,
                        'showCaption' => true,
                        'filename' => Yii::t('app', 'grid-export'),
                        'alertMsg' => Yii::t('app', 'The PDF export file will be generated for download.'),
                        'options' => [
                            'title' => Yii::t('app', 'Portable Document Format'),
                        ],
                        'mime' => 'application/pdf',
                        'config' => [
                            'mode' => 'UTF-8',
                            'format' => 'A4',
                            'destination' => 'I',
                            'marginTop' => 20,
                            'marginBottom' => 20,
                            'cssInline' => 'td{font-size:22pt;}'.
                                'th{font-size:24pt;text-align:center;}',
                            'methods' => [
                                'SetHeader' => '<table width="100%">
                                                    <tr>
                                                        <td class="text-right" width="50%" style="font-size:18pt;">
                                                            รายงานสถิติการจอดรถข้ามคืน
                                                        </td>
                                                    </tr>
                                                </table>',
                                'SetFooter' => '<table width="100%">
                                                    <tr>
                                                        <td class="text-left" width="50%" style="font-size:13pt;">
                                                            ระบบจัดการลานจอดรถยนต์ โรงพยาบาลราชวิถี อาคารเฉลิมพระเกียรติ
                                                        </td>
                                                        <td class="text-right" width="50%" style="font-size:13pt;">
                                                        ' . 'Print:' .date('d/m/Y').'
                                                        </td>
                                                    </tr>
                                                </table>',
                            ],
                            'options' => [
                                'title' => 'รายงานสถิติการจอดรถค้างคืน.PDF',
                                'defaultheaderline' => 0,
                                'defaultfooterline' => 0,
                            ],
                            'contentBefore' => '',
                            'contentAfter' => '',
                        ],
                    ],
                ],
                'clientOptions' => [
                    "data" => new \yii\web\JsExpression(Json::encode($queryReportNightsate)),
                    "lengthMenu" => [[5,10,15,20,-1],[5,10,15,20,"All"]],
                    "pageLength" => 20,
                    "processing" => true,
                    "columns" => [
                        [ "data" => "id" ],
                        [ "className"=>     'details-control skip-export',
                          "orderable"=>      true,
                          "data"=>         null,
                          "defaultContent"=> ""
                        ],
                        [ "data" => "licenceplate_no" ],
                        [ "data" => "card_owner_name" ],
                        [ "data" => "section_name" ],
                        [ "data" => "card_type_name" ],
                        [ "data" => "On_tran" ],
                        [ "data" => "card_id" ],
                    ],
                    //"order"=> [[1, 'asc']],
                    "columnDefs" => [
                        [ "sClass" => "text-center", "targets" => [0,2,5,6,7,] ],
                        [ "sClass" => "text-center skip-export", "targets" => [1] ]
                    ],
                    "initComplete" => new \yii\web\JsExpression("function (settings) {
                        initSt2(this,\"select41\",4,\"หน่วยงาน\");
                        initSt2(this,\"select42\",5,\"ประเภท\");

                        var t = $('#reportnightstate').DataTable();
                        t.on( 'order.dt search.dt', function () {
                            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                            } );
                        } ).draw();
                    }"),
                    'fnRowCallback' => new \yii\web\JsExpression("function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                        var sDirectionClass;
                        $(nRow).attr( 'data-key' ,aData.card_id);
                        return nRow;
                    } ")
                ],
                'columns' => [
                    [
                        'class' => 'msoft\widgets\grid\SerialColumn',
                        'width' => '5%',
                    ],
                    [
                        'header' => ' ',
                        'class' => 'msoft\widgets\grid\SerialColumn',
                        'width' => '5%',
                    ],
                    [
                        'header' => 'ทะเบียนรถ',
                        'headerOptions' => $classmidle,
                        'hAlign' => 'center',
                        'width' => '10%',
                    ],
                    [
                        'header' => 'ชื่อผู้ถือบัตร',
                        'headerOptions' => $classmidle,
                        'hAlign' => 'center',
                        'width' => '30%',
                    ],
                    [
                        'header' => 'หน่วยงาน',
                        'headerOptions' => $classmidle,
                        'hAlign' => 'center',
                        'width' => '15%',
                    ],
                    [
                        'header' => 'ประเภทบัตร',
                        'headerOptions' => $classmidle,
                        'hAlign' => 'center',
                        'width' => '20%',
                    ],
                    [
                        'header' => 'จำนวน (ครั้ง)',
                        'headerOptions' => $classmidle,
                        'hAlign' => 'center',
                        'width' => '5%',
                    ],
                    [
                        'header' => 'หมายเลขบัตร',
                        'headerOptions' => $classmidle,
                        'hAlign' => 'center',
                        'width' => '10%',
                    ],
                ],
            ]); ?>
            </div>
</div>
</div>
<?php 
$this->registerJsFile(
    '@web/js/dt-s2.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJs(
    $this->render('./js/_form_script.js')
);
$this->registerJs(<<<JS
function format ( card_id ) {
    var data;
    $.ajax({
        url: "/report/dashboard/return-expand",
        type: "get", 
        data: {card_id: card_id},
        dataType: "html",
        async: false,
        success: function (result) {
            data = result;
        }
    });
    return data;
}
$('#reportnightstate tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var card_id = $(this).closest("tr").data("key");
        var row = reportnightstate.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(card_id) ).show();
            tr.addClass('shown');
        }
    } );
$('#wait').on('beforeSubmit', function(e){
    LoadingClass();
});
$('#w0').on('pjax:success', function () {
    $('.page-content').waitMe('hide');
});
JS
)
?>
<script>
    function LoadingClass() {
        $('.page-content').waitMe({
            effect: 'ios',//roundBounce,progressBar
            text: 'กำลังโหลดข้อมูล...',
            bg: 'rgba(255,255,255,0.7)',
            color: '#53a93f',
            maxSize: '',
            source: 'img.svg',
            onClose: function () {
            }
        });
    }
</script>