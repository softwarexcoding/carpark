<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\modules\report\components\ReportQuery;
$title = 'รายงานแสดงข้อมูลรถบุคลากรทางการแพทย์(พื้นที่สีเหลือง) ที่ผ่านชั้น 1';
$result = ArrayHelper::index($model,null,'stk_id');
//******Setting style culumn pdf****** 
$font_heading =  'style="text-align:center;font-size:20pt"';
$font_nomal = 'style="text-align:center;font-size:16pt"';
$column_th_style ='style="text-align:center;font-size:16pt;border-top: 1px solid black;border-bottom: 1px solid black;padding-top: 5px;padding-bottom: 5px;"';
$column_th_style_rigth ='style="text-align:right;font-size:16pt;border-top: 1px solid black;border-bottom: 1px solid black;padding-top: 10px;"';
$column_td_style_center = 'style="font-size:16pt;text-align:center;vertical-align: top;"';
$column_td_style_right = 'style="font-size:16pt;text-align: right;vertical-align: top;"';
$column_td_style_left = 'style="font-size:16pt;vertical-align: top;"';
//******Setting style culumn pdf****** 
$i = 0;
?>
<table width="100%" border="0" >
    <tr>
        <td style="text-align:center;font-size:16pt" width="20%">
            <?= Html::img('@frontend/modules/report/views/dashboard/images/logo/logo_print.png', ['alt' => 'My logo','height' => '80px']) ?>
        </td>
        <td style="text-align:right;font-size:16pt" width="80%">  
            <strong><?= $title; ?></strong>
            <br>  
            <strong><?= Html::label('วันที่ ') ?></strong><?= date("d/m/Y") ?>
        </td>
    </tr>
</table>
<br>
<table border="0" cellpadding="0" cellspacing="0"  width="100%"> 
    <thead>
        <tr>
            <th <?= $column_th_style ?> width="5%">
                <strong>#</strong>
            </th>
            <th <?= $column_th_style ?> width="18%">
                <strong>ทะเบียนรถ</strong>
            </th>
            <th <?= $column_th_style ?> width="40%">
                <strong>ชื่อ - นามสกุล</strong>
            </th>
            <th <?= $column_th_style ?> width="10%">
                <strong>เวลาเข้า</strong>
            </th>
            <th <?= $column_th_style ?> width="15%">
                <strong>หมายเลขบัตร</strong>
            </th>
            <th <?= $column_th_style ?> width="12%">
                <strong>หมายเหตุ</strong>
            </th>
        </tr>
    </thead>
        <?php foreach ($model as $row) : ?>
        <tr>    
            <td  <?= $column_td_style_center?> >
                <?= $i+1; ?>
            </td>
            <td  <?= $column_td_style_center?> >
                <?= $row['licenceplate_no'] ?>
            </td>
            <td  <?= $column_td_style_left ?> >
                <?= $row['card_owner_name'] ?>
            </td>
            <td  <?= $column_td_style_center?> >
                <?= \metronic\components\DateConvert::mysql2phpTime($row['gate1_datetime']); ?>
            </td>
            <td  <?= $column_td_style_center ?> >
                <?= $row['card_id'] ?>
            </td>
            <td  <?= $column_td_style_center?>>
                
            </td>
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?> 
</table>
<table width="100%" border="0" style="border-top: 1px solid black;border-bottom: 1px solid black;">
    <tr>
        <td width="57%" style="font-size:16pt;">
        	<strong>รวมทั้งสิ้น <?= $i; ?> รายการ</strong>
        </td>
        <td>
        </td>
        <td  style="padding-left:30px">
        </td>
    </tr>
</table>

