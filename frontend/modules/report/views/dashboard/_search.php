<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\report\models\TbTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-transaction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'trans_id') ?>

    <?= $form->field($model, 'card_id') ?>

    <?= $form->field($model, 'licenceplate_no') ?>

    <?= $form->field($model, 'gate1_datetime') ?>

    <?= $form->field($model, 'gate2_datetime') ?>

    <?php // echo $form->field($model, 'gate3_datetime') ?>

    <?php // echo $form->field($model, 'total_min') ?>

    <?php // echo $form->field($model, 'fee_amt') ?>

    <?php // echo $form->field($model, 'disc_type') ?>

    <?php // echo $form->field($model, 'disc_amt') ?>

    <?php // echo $form->field($model, 'disc_time') ?>

    <?php // echo $form->field($model, 'total_amt') ?>

    <?php // echo $form->field($model, 'total_paid') ?>

    <?php // echo $form->field($model, 'invoice_no') ?>

    <?php // echo $form->field($model, 'trans_status') ?>

    <?php // echo $form->field($model, 'createby') ?>

    <?php // echo $form->field($model, 'updateby') ?>

    <?php // echo $form->field($model, 'createat') ?>

    <?php // echo $form->field($model, 'updateat') ?>

    <?php // echo $form->field($model, 'total_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
