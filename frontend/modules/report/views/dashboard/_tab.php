<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
?>
<ul class="nav nav-tabs " id="myTab5">
    <li id="tab_A">
        <a data-toggle="tab" href="#">
            <?= Html::encode('Dashboard') ?> 
        </a>
    </li> 
    <li id="tab_F">
        <a data-toggle="tab" href="#">
            <?= Html::encode('สถานะการจอดรถ') ?> 
        </a>
    </li>
    <li id="tab_G">
        <a data-toggle="tab" href="#">
            <?= Html::encode('ประวัติการจอดรถ') ?> 
        </a>
    </li>
    <li id="tab_B">
        <a data-toggle="tab" href="#">
            <?= Html::encode('สถานะการจอดรถค้างคืน') ?> 
        </a>
    </li>
    <li id="tab_C">
        <a data-toggle="tab" href="#">
            <?= Html::encode('สถานะการจอดรถไม่ตรงพื้นที่') ?> 
        </a>
    </li>
    <li id="tab_I">
        <a data-toggle="tab" href="#">
            <?= Html::encode('สถานะการจอด2เงื่อนไข') ?> 
        </a>
    </li>  
    <li id="tab_D">
        <a data-toggle="tab" href="#">
            <?= Html::encode('รายงานสถิติจอดรถค้างคืน') ?> 
        </a>
    </li>
    <li id="tab_E">
        <a data-toggle="tab" href="#">
            <?= Html::encode('รายงานสถิติจอดรถไม่ตรงพื้นที่') ?> 
        </a>
    </li>
    <li id="tab_J">
        <a data-toggle="tab" href="#">
            <?= Html::encode('รายงาน2เงื่อนไข') ?> 
        </a>
    </li>  
    <li id="tab_H">
        <a data-toggle="tab" href="#">
            <?= Html::encode('สถานะรถออก') ?> 
        </a>
    </li>
</ul>
<?php
    $script = <<< JS
    $("#tab_A").click(function (e) {               
        window.location.replace("/report/dashboard/index");
    });
    $("#tab_F").click(function (e) {               
        window.location.replace("/report/dashboard/status");
    });
    $("#tab_G").click(function (e) {               
        window.location.replace("/report/dashboard/status-history");
    });
     $("#tab_B").click(function (e) {               
        window.location.replace("/report/dashboard/status-nightstate");
    });
     $("#tab_C").click(function (e) {               
        window.location.replace("/report/dashboard/status-nonestate");
    });
     $("#tab_D").click(function (e) {               
        window.location.replace("/report/dashboard/report-nightstate");
    });
    $("#tab_E").click(function (e) {               
        window.location.replace("/report/dashboard/report-nonestate");
    });
    $("#tab_H").click(function (e) {               
        window.location.replace("/report/dashboard/status-out");
    });
    $("#tab_I").click(function (e) {               
        window.location.replace("/report/dashboard/status-condition2");
    });
    $("#tab_J").click(function (e) {               
        window.location.replace("/report/dashboard/report-condition2");
    });
JS;
$this->registerJs($script);
?>
