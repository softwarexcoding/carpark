<?php 
use yii\helpers\Html;
use msoft\widgets\Icon;
use yii\bootstrap\Modal;
use metronic\helpers\RegisterJS;
use frontend\modules\report\components\ReportQuery;
RegisterJS::regis(['sweetalert','ajaxcrud','waitme'],$this);
$this->registerCss("
div.col-xs-11{
    top: 60px !important;
}
.table-scrollable .dataTable td>.btn-group, .table-scrollable .dataTable th>.btn-group{
    position: inherit !important;
}
.actions{
    width: 25%;
}
textarea{
    display: none;
}
");
?>
<div class="col-md-offset-1 col-md-11">
    <div class="panel panel-default">
        <div class="from-group panel-heading" style="background-color:#dff0d8;">
        	<div class="panel-title">
        		<?= Html::encode('ข้อมูลการจอดรถไม่ตรงตามพื้นที่จอด') ?>
        	</div>
        	<div style="text-align: right;">
            	<?= Html::a(Icon::show('print').' พิมพ์รายงาน',['printreport','type'=>'nonestate','card_id'=>$card_id],['class' => 'btn btn-success','role' => 'modal-remote'])?>
        	</div>
        	
        </div>
		<table class="table table-striped table-condensed" width="100%" border="0">
		    <thead>
		        <tr>
		            <th style="text-align: center;">#</th>
		            <th style="text-align: center;">เวลาเข้า</th>
		            <th style="text-align: center;">เวลาออก</th>
		            <th style="text-align: center;">ระยะเวลาจอด</th>
		        </tr>
		    </thead>

		    <tbody>
		    	<?php foreach ($queryReportNonestate as $i => $key) {?> 
		        <tr>
		            <th style="text-align: center;"><?= $i+1; ?></th>
		            <th style="text-align: center;"><?= $key['gate1_datetime'] ?></th>
		            <th style="text-align: center;"><?= $key['gate3_datetime'] ?></th>
		            <th style="text-align: center;"><?= ReportQuery::getConvertHrsToDay($key['T13_hr'])?></th>
		        </tr>
		        <?php } ?>
		    </tbody>
		</table>
	</div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-md"
])?>
<?php Modal::end(); ?>
<script type="text/javascript">
function LoadingClass() {
    $('.page-content').waitMe({
        effect : 'orbit',
        text: 'กำลังโหลดข้อมูล...',
        bg : 'rgba(255,255,255,0.7)',
        color : '#000',
        maxSize : '',
        textPos : 'vertical',
        fontSize : '18px',
        source : ''
    });
}
</script>
<?php
$script = <<< JS
// $('#from').on('beforeSubmit', function(e){
//     LoadingClass();
// });
JS;
$this->registerJs($script);
?>

