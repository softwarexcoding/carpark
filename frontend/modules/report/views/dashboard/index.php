<?php
use yii\bootstrap\Modal;
use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;
use yii\helpers\Json;
use metronic\widgets\portlet\PortletBox;
use yii\widgets\Pjax;
use msoft\widgets\select2\Select2Asset;
use yii\helpers\ArrayHelper;
Select2Asset::register($this)->css[] = 'css/select2-krajee.css';
RegisterJS::regis(['sweetalert'],$this);
$this->registerJs('$("#tab_A").addClass("active");');
$this->title = 'Dashboard';
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
    .table-scrollable{
        overflow-x: hidden !important;
    }
    .portlet.light_2{
        background-color : #eef1f5 !important;
    }
    .portlet.light.bordered>.portlet-title{
        border-bottom: 1px solid #060606 !important;
    }
    .widget-thumb-half{
        height: 50px;
    }
    .bg-default {
        background: #f36a09b5 !important; 
    }
    .bg-doctor {
        background: #ebef14b8 !important; 
    }
    .bg-null {
        background: #7ebb50 !important; 
    }
    .thumbnail_door1{
        background-color: #ceff8e !important;
    }
    .thumbnail_door4b{
        background-color: #ff8efc82 !important;
    }
    .thumbnail_out{
        background-color: #fffc8e !important;
    }
    .thumbnail_none{
        background-color: #908eff !important;
    }  
    div.red .list-title ,.uppercase {
        color : white;
    }
');
?>
<?= SwalAlert::widget(); ?>

<div class="tab-content">
<?php echo $this->render('_tab'); ?>
<?= PortletBox::begin([
    'title' => false,
    'icon' => false,
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'tools' => [
    ],
    'actions' => [
       
    ]
]);
?>
<!-- สภานะการจอดรถปัจจุบัน -->
<div class="row widget-row" >
    <!-- header -->
    <div class="col-md-12">
        <h4 class="font-green-sharp" style="text-align:left;"><i class="fa fa-calendar"></i> ข้อมูลการจอดรถวันที่ <?php echo date("d/m/Y");?> </h4>
    </div>
    <div class="col-md-12">
    <div class="portlet-body">
        <div class="row">
            
            <div class="col-md-2">
                <div class="mt-widget-3">

                    <div class="mt-head bg-blue">
                        <div class="mt-head-icon">
                            <i class="fa fa-hospital-o"></i>
                        </div>
                        <div class="mt-head-desc">รถเข้าจอดทั้งหมด</div>
                            <h2><span class="mt-head-date" id="queryIN_OUT"></span></h2>
                    </div>
                    <div class="mt-body-actions-icons">
                        <div class="btn-group btn-group btn-group-justified">
                            <a href="/report/dashboard/status" class="btn ">
                                <span class="mt-icon">
                                    <i class="glyphicon glyphicon-align-justify"></i>
                                </span>รายการ </a>
                            <a href="/report/dashboard/printquery?id=1" class="btn" target="_blank">
                                <span class="mt-icon">
                                    <i class="fa fa-print"></i>
                                </span>พิมพ์ </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mt-widget-3">
                    <div class="mt-head bg-red">
                        <div class="mt-head-icon">
                            <i class="fa fa-user-md"></i>
                        </div>
                        <div class="mt-head-desc">รถบุคลากร</div>
                            <h2><span class="mt-head-date" id="queryIN_OUT234"></span></h2>
                    </div>
                    <div class="mt-body-actions-icons">
                        <div class="btn-group btn-group btn-group-justified">
                            <a href="/report/dashboard/status" class="btn ">
                                <span class="mt-icon">
                                    <i class="glyphicon glyphicon-align-justify"></i>
                                </span>รายการ </a>
                            <a href="/report/dashboard/printquery?id=2" class="btn " target="_blank">
                                <span class="mt-icon">
                                    <i class="fa fa-print"></i>
                                </span>พิมพ์ </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mt-widget-3">
                    <div class="mt-head bg-yellow">
                        <div class="mt-head-icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="mt-head-desc">รถผู้มารับบริการ</div>
                            <h2><span class="mt-head-date" id="queryIN_OUT1"></span></h2>
                    </div>
                    <div class="mt-body-actions-icons">
                        <div class="btn-group btn-group btn-group-justified">
                            <a href="/report/dashboard/status" class="btn ">
                                <span class="mt-icon">
                                    <i class="glyphicon glyphicon-align-justify"></i>
                                </span>รายการ </a>
                            <a href="/report/dashboard/printquery?id=3" class="btn " target="_blank">
                                <span class="mt-icon">
                                    <i class="fa fa-print"></i>
                                </span>พิมพ์ </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mt-widget-3">
                    <div class="mt-head bg-dark">
                        <div class="mt-head-icon">
                            <i class="fa fa-sign-out"></i>
                        </div>
                        <div class="mt-head-desc">รถออกทั้งหมด</div>
                            <h2><span class="mt-head-date" id="queryOut"></span></h2>
                    </div>
                    <div class="mt-body-actions-icons">
                        <div class="btn-group btn-group btn-group-justified">
                            <a href="/report/dashboard/status" class="btn ">
                                <span class="mt-icon">
                                    <i class="glyphicon glyphicon-align-justify"></i>
                                </span>รายการ </a>
                            <a href="/report/dashboard/printquery?id=4" class="btn " target="_blank">
                                <span class="mt-icon">
                                    <i class="fa fa-print"></i>
                                </span>พิมพ์ </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mt-widget-3">
                    <div class="mt-head bg-default">
                        <div class="mt-head-icon">
                            <i class="fa fa-pie-chart"></i>
                        </div>
                        <div class="mt-head-desc">รถที่ยังจอดอยู่</div>
                            <h2><span class="mt-head-date" id="queryBalance_member"></span></h2>
                    </div>
                    <div class="mt-body-actions-icons">
                        <div class="btn-group btn-group btn-group-justified">
                            <a href="/report/dashboard/status" class="btn ">
                                <span class="mt-icon">
                                    <i class="glyphicon glyphicon-align-justify"></i>
                                </span>รายการ </a>
                            <a href="/report/dashboard/printquery?id=5" class="btn " target="_blank">
                                <span class="mt-icon">
                                    <i class="fa fa-print"></i>
                                </span>พิมพ์ </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mt-widget-3">
                    <div class="mt-head bg-null">
                        <div class="mt-head-icon">
                            <i class="fa fa-braille"></i>
                        </div>
                        <div class="mt-head-desc">พื้นที่ว่าง</div>
                            <h2><span class="mt-head-date" id="lot_null"></span></h2>
                    </div>
                    <div class="mt-body-actions-icons">
                        <div class="btn-group btn-group btn-group-justified" style="text-align: center;">
                          <h3>จาก <span class="mt-head-date" id="lot"></span> ช่อง</h3> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ROW 1 -->
    <div class="row widget-row">
        <div class="col-md-12">
            <h4 class="font-blue-sharp">สถิติการจอดแยกตามประเภทของบุคลากร</h4>
        </div>
        <div class="col-md-3">
            <div class="mt-element-ribbon bg-grey-steel">
                <div class="ribbon ribbon-right ribbon-color-primary uppercase">ประตูเข้า</div>
                <div class="mt-element-list">
                    <div class="mt-list-head list-default ext-1 red">
                        <div class="row">
                            <a href="/report/dashboard/printquery?id=6" target="_blank">
                            <div class="col-xs-8">
                                <div class="list-head-title-container">
                                    <h3 class="list-title uppercase sbold" >บุคลากรทั้งหมด</h3>
                                    <h3 class="uppercase " >จำนวน
                                        <span id="queryNow_member"></span> คัน 
                                    </h3>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="mt-list-container list-default ext-1">
                        <ul>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNow_member2"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=7" target="_blank">
                                            บุคลากรมีช่องจอดประจำ
                                        </a>
                                    </h3>
                                </div>
                            </li>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;"> 
                                    <h2><span id="queryNow_member34"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=8" target="_blank">
                                            บุคลากรไม่มีช่องจอดประจำ
                                        </a>
                                    </h3>
                                </div>
                            </li>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNow_Doctor"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=21" target="_blank">
                                            บุคลากรทางการแพทย์ (พื้นที่จอดสีเหลือง)
                                        </a>
                                    </h3>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="mt-element-ribbon bg-grey-steel">
                <div class="ribbon ribbon-right ribbon-color-primary uppercase">ประตูชั้น 4B</div>
                <div class="mt-element-list">
                    <div class="mt-list-head list-default ext-1 green">
                        <div class="row">
                            <a href="/report/dashboard/printquery?id=9" target="_blank">
                            <div class="col-xs-8">
                                <div class="list-head-title-container">
                                    <h3 class="list-title uppercase sbold">บุคลากรทั้งหมด</h3>
                                    <h3 class="uppercase">จำนวน
                                        <span id="queryNow_Gate2"></span> คัน 
                                    </h3>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="mt-list-container list-default ext-1">
                        <ul>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNow_Gate2_2"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=10" target="_blank">
                                            บุคลากรมีช่องจอดประจำ
                                        </a>
                                    </h3>
                                </div>
                            </li>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNow_Gate2_34"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=11" target="_blank">
                                            บุคลากรไม่มีช่องจอดประจำ
                                        </a>
                                    </h3>
                                </div>
                            </li>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNow_Gate2_Doctor"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=22" target="_blank">
                                            บุคลากรทางการแพทย์ (พื้นที่จอดสีเหลือง)
                                        </a>
                                    </h3>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="mt-element-ribbon bg-grey-steel">
                <div class="ribbon ribbon-right ribbon-color-primary uppercase">ไม่ตรงพื้นที่</div>
                <div class="mt-element-list">
                    <div class="mt-list-head list-default ext-1 yellow-saffron">
                        <div class="row">
                            <a href="/report/dashboard/printquery?id=12" target="_blank">
                            <div class="col-xs-8">
                                
                                <div class="list-head-title-container">
                                    <h3 class="list-title uppercase sbold">บุคลากรทั้งหมด</h3>
                                    <h3 class="uppercase">จำนวน
                                        <span id="queryNone"></span> คัน 
                                    </h3>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="mt-list-container list-default ext-1">
                        <ul>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNone_2"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=13" target="_blank">
                                            บุคลากรมีช่องจอดประจำ
                                        </a>
                                    </h3>
                                </div>
                            </li>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNone_34"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=14" target="_blank">
                                            บุคลากรไม่มีช่องจอดประจำ
                                        </a>
                                    </h3>
                                </div>
                            </li>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNone_Doctor"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=23" target="_blank">
                                            บุคลากรทางการแพทย์ (พื้นที่จอดสีเหลือง)
                                        </a>
                                    </h3>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="mt-element-ribbon bg-grey-steel">
                <div class="ribbon ribbon-right ribbon-color-primary uppercase">ประตู ออก</div>
                <div class="mt-element-list">
                    <div class="mt-list-head list-default ext-1 purple">
                        <div class="row">
                            <a href="/report/dashboard/printquery?id=16" target="_blank">
                            <div class="col-xs-8">
                                <div class="list-head-title-container">
                                    <h2 class="list-title uppercase sbold">บุคลากรทั้งหมด</h2>
                                    <h3 class="uppercase">จำนวน
                                        <span id="queryNow_Gate3_member"></span> คัน 
                                    </h3>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="mt-list-container list-default ext-1">
                        <ul>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNow_Gate3_member2"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=16" target="_blank">
                                            บุคลากรมีช่องจอดประจำ
                                        </a>
                                    </h3>
                                </div>
                            </li>
                            <li class="mt-list-item done">
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNow_Gate3_member34"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=17" target="_blank">
                                            บุคลากรไม่มีช่องจอดประจำ
                                        </a>
                                    </h3>
                                </div>
                            </li>
                            <li class="mt-list-item done" >
                                <div class="list-icon-container">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="list-datetime" style="margin-top: -20px;">
                                    <h2><span id="queryNow_Gate3_Doctor"></span></h2>
                                </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase">
                                        <a href="/report/dashboard/printquery?id=24" target="_blank">
                                            บุคลากรทางการแพทย์ (พื้นที่จอดสีเหลือง)
                                        </a>
                                    </h3>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row widget-row">
        <div class="col-md-12">
            <h4 class="font-blue-sharp">สถิติการจอดของผู้มารับบริการ</h4>
        </div>
        <a href="/report/dashboard/printquery?id=18" target="_blank">
            <div class="col-md-3">
                <div class="mt-element-ribbon bg-yellow">
                    <div class="ribbon ribbon-right ribbon-color-primary uppercase">ประตูเข้า</div>
                    <div class="mt-element-list">
                        <div class="mt-list-head list-default ext-1 red">
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="list-head-title-container">
                                        <h3 class="list-title uppercase sbold">ผู้รับบริการ</h3>
                                        <h3 class="uppercase">จำนวน
                                            <span id="queryNow_visitor"></span> คัน 
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="/report/dashboard/printquery?id=19" target="_blank">
            <div class="col-md-3">
                <div class="mt-element-ribbon bg-dark">
                    <div class="ribbon ribbon-right ribbon-color-primary uppercase">ประตู ออก</div>
                    <div class="mt-element-list">
                        <div class="mt-list-head list-default ext-1 green">
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="list-head-title-container">
                                        <h3 class="list-title uppercase sbold">ผู้รับบริการ</h3>
                                        <h3 class="uppercase">จำนวน
                                            <span id="queryBalance_visitor"></span> คัน 
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="/report/dashboard/printquery?id=20" target="_blank">
            <div class="col-md-3">
                <div class="mt-element-ribbon bg-default">
                    <div class="ribbon ribbon-right ribbon-color-primary uppercase">จอดอยู่</div>
                    <div class="mt-element-list">
                        <div class="mt-list-head list-default ext-1 purple">
                            <div class="row">

                                <div class="col-xs-8">
                                    <div class="list-head-title-container">
                                        <h2 class="list-title uppercase sbold">ผู้รับบริการ</h2>
                                        <h3 class="uppercase">จำนวน
                                            <span id="queryNow_Gate3_visitor"></span> คัน 
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <!-- สถานะการจอดทั้งหมด -->
    <div class="row widget-row">
        <div class="col-md-8">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp sbold">สถิติการใช้งานพื้นที่จอดรถทั้งหมด</span>
                    </div>
                </div>
                
                <div class="portlet-body">
                    <div class="tiles">
                        <a href="/report/dashboard/status">
                            <div class="tile bg-blue">
                                <div class="tile-body">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name">รถทั้งหมด</div>
                                    <div class="number">
                                        <span id="queryAll"></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="/report/dashboard/status">
                            <div class="tile bg-red-sunglo">
                                <div class="tile-body">
                                    <i class="fa fa-user-md"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name">รถบุคลากร</div>
                                    <div class="number">
                                        <span id="queryMember"></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        
                            <div class="tile bg-red-sunglo">
                                <div class="tile-body">
                                    <i class="fa fa-braille"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> พื้นที่ว่าง </div>
                                    <div class="number">
                                        <span id="querySpace_member"></span>/<span id="Lot_member"></span>
                                    </div>
                                </div>
                            </div>
                        <a href="/report/dashboard/status-nightstate">
                            <div class="tile bg-red-sunglo">
                                <div class="tile-body">
                                    <i class="fa fa-moon-o"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name">จอดค้างคืน</div>
                                    <div class="number"><span id="queryNightstate_234"></span></div>
                                </div>
                            </div>
                        </a>
                        <a href="/report/dashboard/status-nonestate">
                            <div class="tile bg-red-sunglo">
                                <div class="tile-body">
                                    <i class="fa fa-user-md"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name">จอดไม่ตรงพื้นที่</div>
                                    <div class="number">
                                        <div class="number"><span id="queryNonestate"></span></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="/setting/report/index">
                            <div class="tile bg-yellow-lemon">
                                <div class="corner"> </div>
                                <div class="check"> </div>
                                <div class="tile-body">
                                    <i class="fa fa-cogs"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name">ตั้งค่าพื้นที่จอด</div>
                                </div>
                            </div>
                        </a>
                                    <div class="tile bg-yellow">
                                        <div class="tile-body">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name">รถผู้รับบริการ</div>
                                            <div class="number"><span id="queryVisitor"></span></div>
                                        </div>
                                    </div>
                                    
                                    <div class="tile bg-yellow">
                                        <div class="tile-body">
                                            <i class="fa fa-braille"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> พื้นที่ว่าง </div>
                                            <div class="number">
                                                <span id="querySpace_visitor"></span>/<span id="Lot_visitor"></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="tile bg-yellow">
                                        <div class="tile-body">
                                            <i class="fa fa-moon-o"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name">จอดค้างคืน</div>
                                            <div class="number">
                                                <span id="queryNightstate_1"></span>
                                            </div>
                                        </div>
                                    </div>
                                    
    
                                </div>
                            </div>
                        </div>
    </div>
        <div class="col-md-4">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <i class="icon-globe font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">รายงาน</span>
                    </div>
                </div>
                <div style="display: none;">
                    <i class="fa fa-warning" style="font-size:15px;color:red"></i>
                        <span style="font-size:15px;color:red">กำลังดำเนินการแก้ไข</span>
                </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <ul class="feeds">
                            <li>
                            <a href="/report/dashboard/printreport-bydate" role ="modal-remote">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-success">
                                                <i class="fa fa-print"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc">รายงานสถิติการจอดรถทั้งหมด</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            </li>
                            <li style="display: none;">
                            <a href="/report/dashboard/printighchart_member" role ="modal-remote">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-success">
                                                <i class="fa fa-bell-o"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc">สถิติการจอดรถยนต์ บุคลากร</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            </li>
                            <li style="display: none;">
                            <a href="/report/dashboard/printighchart_visitor" role ="modal-remote">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-success">
                                                <i class="fa fa-bell-o"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc">สถิติการจอดรถยนต์ ผู้รับบริการ</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            </li>
                        </ul>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
<?= PortletBox::end(); ?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-md"
])?>
<?php Modal::end(); ?>
<?php
$this->registerJs(<<<JS
    $( document ).ready(function() {
        getdata();
    });
    setInterval(function(){ 
        getdata();
    }, 60000);
    function getdata(){
        var percen = '%';
         $.ajax({
            url: "get-data",
            type: 'POST',
            dataType: 'json', 
            success: function(result){
                //1.ตัวเลขใหญ่
                //2.ค่าเปอร์เซนต์
                //3.ค่าหลอดเปอร์เซนต์
                //$('#queryNow').html((result.queryNow >= 0 ? result.queryNow : 0));
                $('#queryNow_member').html((result.queryNow_member >= 0 ? result.queryNow_member : 0));
                $('#queryNow_member2').html((result.queryNow_member2 >= 0 ? result.queryNow_member2 : 0));
                $('#queryNow_member34').html((result.queryNow_member34 >= 0 ? result.queryNow_member34 : 0));
                $('#queryNow_Doctor').html((result.queryNow_Doctor >= 0 ? result.queryNow_Doctor : 0));
                $('#queryNow_visitor').html((result.queryNow_visitor >= 0 ? result.queryNow_visitor : 0));
                $('#queryNow_Gate2_2').html((result.queryNow_Gate2_2 >= 0 ? result.queryNow_Gate2_2 : 0));
                $('#queryNow_Gate2_34').html((result.queryNow_Gate2_34 >= 0 ? result.queryNow_Gate2_34 : 0));
                $('#queryNow_Gate2_Doctor').html((result.queryNow_Gate2_Doctor >= 0 ? result.queryNow_Gate2_Doctor : 0));
                $('#queryNow_Gate3_member').html((result.queryNow_Gate3_member >= 0 ? result.queryNow_Gate3_member : 0));
                $('#queryNow_Gate3_member2').html((result.queryNow_Gate3_member2 >= 0 ? result.queryNow_Gate3_member2 : 0));
                $('#queryNow_Gate3_member34').html((result.queryNow_Gate3_member34 >= 0 ? result.queryNow_Gate3_member34 : 0));
                $('#queryNow_Gate3_Doctor').html((result.queryNow_Gate3_Doctor >= 0 ? result.queryNow_Gate3_Doctor : 0));
                $('#queryNow_Gate3_visitor').html((result.queryNow_Gate3_visitor >= 0 ? result.queryNow_Gate3_visitor : 0));
                $('#queryIN_OUT').html((result.queryIN_OUT >= 0 ? result.queryIN_OUT : 0));
                $('#queryNow_Gate2').html((result.queryNow_Gate2 >= 0 ? result.queryNow_Gate2 : 0));
                $('#queryBalance_member').html((result.queryBalance_member >= 0 ? result.queryBalance_member : 0));
                $('#queryBalance_visitor').html((result.queryBalance_visitor >= 0 ? result.queryBalance_visitor : 0));
                $('#queryNone_2').html((result.queryNone_2 >= 0 ? result.queryNone_2 : 0));
                $('#queryNone_34').html((result.queryNone_34 >= 0 ? result.queryNone_34 : 0));
                $('#queryNone_Doctor').html((result.queryNone_Doctor >= 0 ? result.queryNone_Doctor : 0));
                $('#queryNone').html((result.queryNone >= 0 ? result.queryNone : 0));
                $('#queryOut').html((result.queryOut >= 0 ? result.queryOut : 0));
                $('#queryIN_OUT234').html((result.queryIN_OUT234 >= 0 ? result.queryIN_OUT234 : 0));
                $('#queryIN_OUT1').html((result.queryIN_OUT1 >= 0 ? result.queryIN_OUT1 : 0));
                $('#lot_null').html((result.lot_null >= 0 ? result.lot_null : 0));
                $('#lot').html((result.lot >= 0 ? result.lot : 0));

                $('#queryAll').html((result.queryAll >= 0 ? result.queryAll : 0));
                $('#queryVisitor').html((result.queryVisitor >= 0 ? result.queryVisitor : 0));
                $('#querySpace_visitor').html((result.querySpace_visitor >= 0 ? result.querySpace_visitor : 0));
                $('#queryNightstate_234').html((result.queryNightstate_234 >= 0 ? result.queryNightstate_234 : 0));
                $('#queryMember').html((result.queryMember >= 0 ? result.queryMember : 0));
                $('#querySpace_member').html((result.querySpace_member >= 0 ? result.querySpace_member : 0));
                $('#queryNightstate_1').html((result.queryNightstate_1 >= 0 ? result.queryNightstate_1 : 0));
                $('#queryNonestate').html((result.queryNonestate >= 0 ? result.queryNonestate : 0));
                $('#Lot_visitor').html((result.Lot_visitor >= 0 ? result.Lot_visitor : 0));
                $('#Lot_member').html((result.Lot_member >= 0 ? result.Lot_member : 0));
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                console.log(errorThrown); 
            }  
        });
    }
JS
);?>   
    