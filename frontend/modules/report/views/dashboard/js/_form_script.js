$('body').addClass('page-sidebar-closed');
$('.page-sidebar .page-sidebar-menu, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu').addClass('page-sidebar-menu-closed');
//Form
DateRangePicker = {
    SortDateRang : function(start,end){
        $.fn.dataTable.ext.search.push(
            function( settings, searchData, index, rowData, counter ) {
                var rows = searchData[5];
                var res = rows.split("/");
                var rowDate = res[0]+"/"+res[1]+"/"+parseInt(res[2]-543);
                console.log(rowDate);

                var d1 = start.split("/");
                var d2 = end.split("/");
                var c = rowDate.split("/");

                var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
                var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
                var check = new Date(c[2], parseInt(c[1])-1, c[0]);

                //console.log(check >= from && check <= to)
                 // using the data from the 6th column
                 if(check >= from && check <= to) {
                    return true;
                 }  
                 return false;                         
             }

        );  
        detailstatus.draw();
        $.fn.dataTable.ext.search.pop();
    },
    SortDateRang2 : function(start,end){
        $.fn.dataTable.ext.search.push(
            function( settings, searchData, index, rowData, counter ) {
                var rows = searchData[5];
                var res = rows.split("/");
                var rowDate = res[0]+"/"+res[1]+"/"+parseInt(res[2]-543);
                console.log(rowDate);

                var d1 = start.split("/");
                var d2 = end.split("/");
                var c = rowDate.split("/");

                var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
                var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
                var check = new Date(c[2], parseInt(c[1])-1, c[0]);

                //console.log(check >= from && check <= to)
                 // using the data from the 6th column
                 if(check >= from && check <= to) {
                    return true;
                 }  
                 return false;                         
             }

        );  
        detailhistory.draw();
        $.fn.dataTable.ext.search.pop();
    },
    SortDateRang3 : function(start,end){
        $.fn.dataTable.ext.search.push(
            function( settings, searchData, index, rowData, counter ) {
                var rows = searchData[5];
                var res = rows.split("/");
                var rowDate = res[0]+"/"+res[1]+"/"+parseInt(res[2]-543);
                console.log(rowDate);

                var d1 = start.split("/");
                var d2 = end.split("/");
                var c = rowDate.split("/");

                var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
                var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
                var check = new Date(c[2], parseInt(c[1])-1, c[0]);

                //console.log(check >= from && check <= to)
                 // using the data from the 6th column
                 if(check >= from && check <= to) {
                    return true;
                 }  
                 return false;                         
             }

        );  
        detailnightstate.draw();
        $.fn.dataTable.ext.search.pop();
    },
    SortDateRang4 : function(start,end){
        $.fn.dataTable.ext.search.push(
            function( settings, searchData, index, rowData, counter ) {
                var rows = searchData[5];
                var res = rows.split("/");
                var rowDate = res[0]+"/"+res[1]+"/"+parseInt(res[2]-543);
                console.log(rowDate);

                var d1 = start.split("/");
                var d2 = end.split("/");
                var c = rowDate.split("/");

                var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
                var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
                var check = new Date(c[2], parseInt(c[1])-1, c[0]);

                //console.log(check >= from && check <= to)
                 // using the data from the 6th column
                 if(check >= from && check <= to) {
                    return true;
                 }  
                 return false;                         
             }

        );  
        detailnonetstate.draw();
        $.fn.dataTable.ext.search.pop();
    },
    SortDateRang5 : function(start,end){
        $.fn.dataTable.ext.search.push(
            function( settings, searchData, index, rowData, counter ) {
                var rows = searchData[5];
                var res = rows.split("/");
                var rowDate = res[0]+"/"+res[1]+"/"+parseInt(res[2]-543);
                console.log(rowDate);

                var d1 = start.split("/");
                var d2 = end.split("/");
                var c = rowDate.split("/");

                var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
                var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
                var check = new Date(c[2], parseInt(c[1])-1, c[0]);

                //console.log(check >= from && check <= to)
                 // using the data from the 6th column
                 if(check >= from && check <= to) {
                    return true;
                 }  
                 return false;                         
             }

        );  
        detailout.draw();
        $.fn.dataTable.ext.search.pop();
    }
}
Checkboxx = {
    
    SortByDate : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        var self = this;
        var d = new Date().toISOString()

        if(grpid == 1){
            detailstatus.search(self.Splitdate(d)).draw();
        }else{
            detailstatus.search( " " ).draw();
        }
    },
    SortByDate1 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        var self = this;
        var d = new Date().toISOString()

        if(grpid == 1){
            detailnightstate.search(self.Splitdate1(d)).draw();
        }else{
            detailnightstate.search( " " ).draw();
        }
    },
    SortByDate2 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        var self = this;
        var d = new Date().toISOString()

        if(grpid == 1){
            detailnonetstate.search(self.Splitdate(d)).draw();
        }else{
            detailnonetstate.search( " " ).draw();
        }
    },
    SortByDate4 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        var self = this;
        var d = new Date().toISOString()

        if(grpid == 1){
            detailout.search(self.Splitdate(d)).draw();
        }else{
            detailout.search( " " ).draw();
        }
    },
    Splitdate : function(d)
    {
        var array_date = d.split("T");
        var arr_d = array_date[0];
        var new_d = arr_d.split("-");
        return new_d[2]+"/"+new_d[1]+"/"+(Number(new_d[0])+543);
    },
    Splitdate1 : function(d)
    {
        var array_date = d.split("T");
        var arr_d = array_date[0];
        var new_d = arr_d.split("-");
        return Number((new_d[2])-1)+"/"+new_d[1]+"/"+(Number(new_d[0])+543);
    },
    SortByUser : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        if(grpid == 1){
            detailnightstate.search( "ผู้รับบริการ" ).draw();
        }else{
            detailnightstate.search( " " ).draw();
        }
    },
    SortByUser2 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        if(grpid == 1){
            detailhistory.search( "ผู้รับบริการ" ).draw();
        }else{
            detailhistory.search( " " ).draw();
        }
    },
    SortByUser4 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        if(grpid == 1){
            detailout.search( "ผู้รับบริการ" ).draw();
        }else{
            detailout.search( " " ).draw();
        }
    },
    S_ByUser : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        if(grpid == 1){
            reportnightstate.search( "ผู้รับบริการ" ).draw();
        }else{
            reportnightstate.search( " " ).draw();
        }
    },
    SortByUser1 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        if(grpid == 1){
            detailstatus.search( "ผู้รับบริการ" ).draw();
        }else{
            detailstatus.search( " " ).draw();
        }
    },
    SortByDoctor : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        console.log(grpid);
        if(grpid == 1){
            detailnightstate.search( "บุคลากร" ).draw();
        }else{
            detailnightstate.search( " " ).draw();
        }
    },
    SortByDoctor2 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        console.log(grpid);
        if(grpid == 1){
            detailhistory.search( "บุคลากร" ).draw();
        }else{
            detailhistory.search( " " ).draw();
        }
    },
    SortByDoctor4 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        console.log(grpid);
        if(grpid == 1){
            detailout.search( "บุคลากร" ).draw();
        }else{
            detailout.search( " " ).draw();
        }
    },
    SortByDoctor1 : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        console.log(grpid);
        if(grpid == 1){
            detailstatus.search( "บุคลากร" ).draw();
        }else{
            detailstatus.search( " " ).draw();
        }
    },
    S_ByDoctor : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        console.log(grpid);
        if(grpid == 1){
            reportnightstate.search( "บุคลากร" ).draw();
        }else{
            reportnightstate.search( " " ).draw();
        }
    },
    
    AutoSave: function(){
        var frm = $('form[id="TbProject"]');
        //var Form = $('.tb-meeting-form').find("form")[0];
        var data = new FormData($(frm)[0]);
        console.log(data);
        $.ajax({
            "type":frm.attr('method'),
            "url": frm.attr('action'),//frm.attr('action')
            "data":data,
            "async": false,
            "dataType":"json",
            "success":function (result) {
                AppNotify.SaveCompleted();
            },
            "error":function (xhr, status, error) {
                AppNotify.Error(error);
            },
            contentType: false,
            cache: false,
            processData: false
        });
        return false;
    },
    initTabsActive: function(){
        //sessionStorage.removeItem('activeid');
        var sessionID = sessionStorage.getItem("activeid");
        if(sessionID !== null && (window.location.pathname === '/project/prj/update' || window.location.pathname === '/project/prj/sumup-project')){
            //Remove Class
            $('ul#tabs-left > li.active').removeClass('active');
            for (i = 0; i < 15; i++) { 
                $('div[id="tabs'+i+'"]').removeClass('active in');
            }
            //Add Class
            var tabsid = sessionID.replace("#", "");
            $('li[data-id="'+tabsid+'"]').addClass('active');
            $('div[id="'+tabsid+'"]').addClass('active in');
        }
    }
};
//
/*
$(function(){
    $('#TbProject input,select,textarea').on('change',function(e){
        Project.AutoSave();
    });
});*/
// $(function(){
//     //Loading
//     $('.page-container').waitMe({
//         effect : 'facebook',
//         text : 'Please wait...',
//         bg : 'rgba(255,255,255,0.7)',
//         color : '#000',
//         textPos : 'vertical',
//     });

//     $('ul#tabs-left > li >a').on('click',function(e){
//         if(window.location.pathname === '/project/prj/update' || window.location.pathname === '/project/prj/sumup-project'){
//             sessionStorage.setItem('activeid',$(this).attr('href'));
//         }
//     });
//     Project.initTabsActive();
// });

// window.onload = function () { $(".page-container").waitMe("hide"); }