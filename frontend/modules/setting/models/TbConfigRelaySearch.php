<?php

namespace frontend\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\setting\models\TbConfigRelay;

/**
 * TbConfigRelaySearch represents the model behind the search form of `frontend\modules\setting\models\TbConfigRelay`.
 */
class TbConfigRelaySearch extends TbConfigRelay
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gate_id', 'delay'], 'integer'],
            [['relay_name', 'ip', 'port', 'status_on', 'status_close'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbConfigRelay::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gate_id' => $this->gate_id,
            'delay' => $this->delay,
        ]);

        $query->andFilterWhere(['like', 'relay_name', $this->relay_name])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'status_on', $this->status_on])
            ->andFilterWhere(['like', 'status_close', $this->status_close]);

        return $dataProvider;
    }
}
