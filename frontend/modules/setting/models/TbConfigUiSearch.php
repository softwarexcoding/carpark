<?php

namespace frontend\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\setting\models\TbConfigUi;

/**
 * TbConfigUiSearch represents the model behind the search form about `frontend\modules\setting\models\TbConfigUi`.
 */
class TbConfigUiSearch extends TbConfigUi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'member_modaltime'], 'integer'],
            [['member_charge', 'member_modaldisplay', 'member_modalautosave'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbConfigUi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'member_modaltime' => $this->member_modaltime,
        ]);

        $query->andFilterWhere(['like', 'member_charge', $this->member_charge])
            ->andFilterWhere(['like', 'member_modaldisplay', $this->member_modaldisplay])
            ->andFilterWhere(['like', 'member_modalautosave', $this->member_modalautosave]);

        return $dataProvider;
    }
}
