<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\setting\models\TbConfigRelay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tab-content">
    <div class="tb-setting-form">
        <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL,'id'=>'form_setting']); ?>
        <div class="form-group">
          <label class="col-sm-2 control-label">ประตู</label>
            <div class="col-sm-7">
                <?=  $form->field($model, 'gate_id',['showLabels' => false])->widget(\msoft\widgets\Select2::classname(), [
                'data' => ArrayHelper::map(\frontend\modules\gate\models\TbGate::find()->all(), 'gate_id', 'gate_name'),
                'options' => ['placeholder' => 'เลือกประตู ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">ชื่อกล้อง</label>
            <div class="col-sm-7">
            <?= $form->field($model, 'camera_name', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">ไอพี</label>
            <div class="col-sm-7">
            <?= \yii\widgets\MaskedInput::widget([
                'name' => 'TbConfigCamera[ip]',
                'value' => !empty($model->ip)?$model->ip:'',
                'clientOptions' => [
                    'alias' =>  'ip'
                ],
            ]);
            ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">เวลาหน่วง</label>
            <div class="col-sm-7">
            <?= $form->field($model, 'port', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">ชื่อผู้ใช้งาน</label>
            <div class="col-sm-7">
            <?= $form->field($model, 'username', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">รหัสผ่าน</label>
            <div class="col-sm-7">
            <?= $form->field($model, 'password', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>     
        <br>        
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group" style="text-align:right;">
                <div class="col-sm-9">
                    <?= Html::a('Close', ['index'], ['class' => 'btn btn-default']) ?>
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
                </div>
            </div>
        <?php } ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php
$this->registerJs(<<<JS
$( document ).ready(function() {
    $('#form_setting').on('beforeSubmit', function(e){
        LoadingClass();
    });
});
JS
);?>
<script type="text/javascript">
function LoadingClass() {
    $('.page-content').waitMe({
        effect : 'orbit',
        text: 'กำลังโหลดข้อมูล...',
        bg : 'rgba(255,255,255,0.7)',
        color : '#000',
        maxSize : '',
        textPos : 'vertical',
        fontSize : '18px',
        source : ''
    });
}
</script>