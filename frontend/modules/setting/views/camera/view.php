<?php

use yii\helpers\Html;
use msoft\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\setting\models\TbConfigCamera */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tb Config Cameras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-config-camera-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'gate_id',
                'label' => 'ชื่อประตู',
                'value'=> @$model->gate->gate_name,
            ],
            'camera_name',
            'ip',
            'port',
            'username',
            'password',
        ],
    ]) ?>

</div>
