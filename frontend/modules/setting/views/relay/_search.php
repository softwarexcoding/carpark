<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\setting\models\TbConfigRelaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-config-relay-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'gate_id') ?>

    <?= $form->field($model, 'relay_name') ?>

    <?= $form->field($model, 'ip') ?>

    <?= $form->field($model, 'port') ?>

    <?php // echo $form->field($model, 'delay') ?>

    <?php // echo $form->field($model, 'status_on') ?>

    <?php // echo $form->field($model, 'status_close') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
