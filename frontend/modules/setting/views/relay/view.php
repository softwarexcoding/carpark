<?php

use yii\helpers\Html;
use msoft\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\setting\models\TbConfigRelay */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tb Config Relays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-config-relay-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             [
                'attribute'=>'gate_id',
                'label' => 'ชื่อประตู',
                'value'=> @$model->gate->gate_name,
            ],
            'relay_name',
            'ip',
            'port',
            'delay',
            'status_on',
            'status_close',
        ],
    ]) ?>

</div>
