<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use metronic\helpers\RegisterJS;
RegisterJS::regis(['ajaxcrud','waitme'], $this);

$this->title = 'ตั้งค่าหน้าแสดงผล';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
div.col-xs-11{
    top: 60px !important;
}
");
?>
<?php
echo Tabs::widget([
    'items' => [
        [
            'label' => $this->title,
            'content' => 'Anim pariatur cliche...',
            'active' => true,
        ],
    ],
    'encodeLabels' => false,
    'renderTabContent' => false
]);
?>
<div class="tab-content">
    <div class="tb-setting-form">
        <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL,'id'=>'form_setting']); ?>
        <div class="form-group">
          <label class="col-sm-2 control-label">Member Charge</label>
            <div class="col-sm-6">
                <?= $form->field($model, 'member_charge',[
                    'showLabels' => false,
                ])->widget(\msoft\widgets\CheckboxX::classname(), [
                    'pluginOptions' => [
                        'threeState' => false,
                        'size' => 'md',
                    ],
                ]);?>
          </div>
        </div>
        <div class="form-group">
        <label class="col-sm-2 control-label">Modal Show</label>
            <div class="col-sm-6">
                <?= $form->field($model, 'member_modaldisplay',[
                    'showLabels' => false,
                ])->widget(\msoft\widgets\CheckboxX::classname(), [
                    'pluginOptions' => [
                        'threeState' => false,
                        'size' => 'md',
                    ],
                ]);?>
          </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Delay Modal</label>
            <div class="col-sm-1">
                <?= $form->field($model, 'member_modalautosave',[
                    'showLabels' => false,
                ])->widget(\msoft\widgets\CheckboxX::classname(), [
                    'pluginOptions' => [
                        'threeState' => false,
                        'size' => 'md',
                    ],
                ]);?>
            </div>
            <label class="col-sm-1 control-label">Time</label>
            <div class="col-sm-1">
            <?= $form->field($model, 'member_modaltime', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                    'style' => 'text-align:right',
                ]);
            ?>
            </div>
            <label class="col-sm-1 control-label" style="text-align: left;">Second</label>
        </div>
        <br>
        <div class="form-group">
            <div class="col-sm-offset-1 col-sm-10">
                <pre style="font-size: 14px">หมายเหตุ: ใส่เครื่องหมาย <i class="glyphicon glyphicon-ok"></i> ในช่องว่างหรือเอาออกเพื่อ<u style="color: red;">เปิดหรือปิด</u>หน้าแสดงผลของโปรแกรม</pre>
            </div>
        </div>
        <br>         
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group" style="text-align:right;">
                <div class="col-sm-11">
                    <?= Html::a('Close', ['index'], ['class' => 'btn btn-default']) ?>
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
                </div>
            </div>
        <?php } ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php
$this->registerJs(<<<JS
$( document ).ready(function() {
    $('#form_setting').on('beforeSubmit', function(e){
        LoadingClass();
    });
});
JS
);?>
<script type="text/javascript">
function LoadingClass() {
    $('.page-content').waitMe({
        effect : 'orbit',
        text: 'กำลังโหลดข้อมูล...',
        bg : 'rgba(255,255,255,0.7)',
        color : '#000',
        maxSize : '',
        textPos : 'vertical',
        fontSize : '18px',
        source : ''
    });
}
</script>