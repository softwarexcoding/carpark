<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model msoft\core\models\TablesFields */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tables-fields-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);?>

    <?= $form->field($model, 'table_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'table_varname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'table_field_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'table_length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'table_default')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'table_index')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'input_field')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'input_label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'input_hint')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_specific')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_required')->textInput() ?>

    <?= $form->field($model, 'input_validate')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_meta')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_order')->textInput() ?>

    <?= $form->field($model, 'action_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'begin_html')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'end_html')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group" style="text-align:right;">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>
