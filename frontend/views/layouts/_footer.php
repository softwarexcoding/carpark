<div class="page-footer">
    <div class="page-footer-inner">
        <?= Yii::$app->params['Copyright']; ?>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>