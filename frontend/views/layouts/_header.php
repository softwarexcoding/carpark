<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
?>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?= Url::to(['/']) ?>">
                <img src="<?= Yii::getAlias('@web') ?>/images/logo/ap-logo.png" alt="logo" class="logo-default" /> 
            </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>
        <!-- END LOGO -->
        <?php if(Yii::$app->user->can('SuperAdmin')): ?>
        <div class="hor-menu  hor-menu-light hidden-sm hidden-xs">
            <?php
            echo Menu::widget([
                'items' => [
                    [
                        'label' => 'Admins', 
                        'url' => 'javascript:;',
                        'options' => [
                            'class' => 'classic-menu-dropdown',
                            'aria-haspopup' => 'true'
                        ],
                        'template' => '<a href="{url}" data-hover="megamenu-dropdown" data-close-others="true">{label} <i class="fa fa-angle-down"></i></a>',
                        'items' => [
                            ['label' => '<i class="fa fa-bookmark-o"></i> Gii', 'url' => '/gii'],
                            ['label' => '<i class="icon-bar-chart"></i> Report Settings', 'url' => '/report/default/index'],
                            ['label' => '<i class="icon-grid"></i> Guide Helpers', 'url' => '/site/widgets-theme'],
                            ['label' => '<i class="icon-wrench"></i> App Config', 'url' => '/site/config'],
                            ['label' => '<i class="icon-tag"></i> Demo Ajax Form', 'url' => '/tb-schemas/index'],
                            ['label' => '<i class="fa fa-bookmark-o"></i> Generate Code', 'url' => '/generate/default/gii'],
                            ['label' => '<i class="fa fa-bookmark-o"></i> Core Modules', 'url' => '/core'],
                            ['label' => '<i class="fa fa-bookmark-o"></i> Core Menus', 'url' => '/menu'],
                        ],
                    ],
                ],
                'submenuTemplate' => '<ul class="dropdown-menu pull-left">{items}</ul>',
                'options' => [
                    'class' => 'nav navbar-nav'
                ],
                'encodeLabels' => false
            ]);?>
        </div>
        <?php endif; ?>
        <div class="hor-menu  hor-menu-light hidden-sm hidden-xs">
        <?php
        echo Menu::widget([
            'items' => [
                [
                    'label' => isset(Yii::$app->params['appName'])  ? Yii::$app->params['appName'] : Yii::$app->name, 
                    'url' => Url::home(true),
                    'options' => [
                        'class' => 'classic-menu-dropdown',
                        'aria-haspopup' => 'true'
                    ],
                    'template' => '<a href="{url}" style="color: white;font-size: 14pt;font-weight: bolder;" data-hover="megamenu-dropdown" data-close-others="true">{label}</a>',
                ],
            ],
            'submenuTemplate' => '<ul class="dropdown-menu pull-left">{items}</ul>',
            'options' => [
                'class' => 'nav navbar-nav'
            ],
            'encodeLabels' => false
        ]);?>
        </div>
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->

                <!-- END TODO DROPDOWN -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <?php if(!Yii::$app->user->isGuest):?>
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="" />
                        <span class="username username-hide-on-mobile"><?= !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : ''; ?></span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <?php
                    echo Menu::widget([
                        'items' => [
                            ['label' => '<i class="icon-user"></i>'.' ข้อมูลส่วนตัว', 'url' => ['/user/settings/profile']],
                            //['label' => '<i class="icon-calendar"></i>'.' ปฏิทิน', 'url' => ['/site/calendar']],
                            ['label' => '<i class="icon-users"></i>'.' จัดการผู้ใช้งาน', 'url' => ['/user/admin/index'],'visible' => Yii::$app->user->can('SuperAdmin')],
                            ['label' => '<i class="icon-users"></i>'.' กำหนดสิทธิ์', 'url' => ['/admin/assignment/index'],'visible' => Yii::$app->user->can('SuperAdmin')],
                            ['label' => '<i class="icon-lock"></i>'.' เปลี่ยนรหัสผ่าน', 'url' => ['/user/settings/profile','#' => 'changePassword']],
                            ['label' => false, 'url' => false,'options' => ['class' => 'divider']],
                            ['label' => '<i class="icon-logout"></i>'.' Log Out', 'url' => ['/user/security/logout'],'template' => '<a href="{url}" data-method="post">{label}</a>'],
                            
                            //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                        ],
                        'options' => [
                            'class' => 'dropdown-menu dropdown-menu-default'
                        ],
                        'encodeLabels' => false
                    ]);?>
                    <!-- <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="page_user_profile_1.html">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li>
                            <a href="app_calendar.html">
                                <i class="icon-calendar"></i> My Calendar </a>
                        </li>
                        <li>
                            <a href="app_inbox.html">
                                <i class="icon-envelope-open"></i> My Inbox
                                <span class="badge badge-danger"> 3 </span>
                            </a>
                        </li>
                        <li>
                            <a href="app_todo.html">
                                <i class="icon-rocket"></i> My Tasks
                                <span class="badge badge-success"> 7 </span>
                            </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="page_user_lock_1.html">
                                <i class="icon-lock"></i> Lock Screen </a>
                        </li>
                        <li>
                            <a href="page_user_login_1.html">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul> -->
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <?= Html::a('<i class="icon-logout"></i>', ['/user/security/logout'],['class' => 'dropdown-toggle','data-method' => 'post']); ?>
                </li>
                <?php endif; ?>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<?=
$this->registerJs(<<<JS
/*
$(".dropdown-user").hover(
  function () {
    $(this).addClass("open");
  },
  function () {
    $(this).removeClass("open");
  }
);*/
JS
);
?>