<?php 
use msoft\menu\widgets\Menu;
use metronic\components\menuItems;
use msoft\menu\models\Navigate;

$action = '/'.$this->context->action->uniqueId;
?>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <?php
        echo Menu::widget([
            'items' => [
                // Important: you need to specify url as 'controller/action',
                // not just as 'controller' even if default action is used.
                [   'label' => 'หน้าหลัก',
                    'icon'=> 'fa fa-home', 
                    'url' => ['/site/index'],
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('gate')
                ],
                [   'label' => 'ประตูทางเข้า1',
                    'icon'=> 'fa fa-car',
                    'url' => ['/gate/transaction/index?gate_id=1'],
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('gate')
                ],
                [   'label' => 'ประตูทางเข้า2',
                    'icon'=> 'fa fa-car',
                    'url' => ['/gate/transaction/index?gate_id=2'],
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('gate')
                ],
                [   'label' => 'ประตูทางออก1',
                    'icon'=> 'fa fa-car',
                    'url' => ['/gate/transaction/index?gate_id=3'],
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('gate')
                ],
                [   'label' => 'นำส่งเงินสด',
                    'icon'=> 'fa fa-usd',
                    'url' =>  ['/payment/payment/index'],
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('payment')
                ],
                [   'label' => 'ลงทะเบียนบัตร',
                    'icon'=> 'fa fa-address-card-o',
                    'url' => ['/card/register-card/index'],
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('card')
                ],
                [   'label' => 'บันทึกส่วนลด',
                    'icon'=> 'fa fa-usd',
                    'url' => ['/payment/discount/index'],
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('discount')
                ],
                [   'label' => 'รายงาน',
                    'icon'=> 'fa fa-file-text-o',
                    'url' => ['/report/dashboard/index'],
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('report')
                ],
                [   'label' => 'ตั้งค่า',
                    'icon'=> 'fa fa-cog',
                    'url' => '#',
                    'parentUrl'=>[],
                    'visible' => Yii::$app->user->can('Settings'),
                    'route'=>[],
                    'options'=> ['class' => 'nav-item'],
                    'items' => [
                        ['label' => 'ตั้งค่าหน้าแสดงผล','icon'=> 'fa fa-cog','url' => ['/setting/ui/index'],'parentUrl'=>[]],
                        ['label' => 'ตั้งค่าหน้ารายงาน','icon'=> 'fa fa-cog','url' => ['/setting/report/index'],'parentUrl'=>[]],
                        ['label' => 'ตั้งค่ากล้อง','icon'=> 'fa fa-cog','url' => ['/setting/camera/index'],'parentUrl'=>[]],
                        ['label' => 'ตั้งค่ารีเลย์','icon'=> 'fa fa-cog','url' => ['/setting/relay/index'],'parentUrl'=>[]],
                        ['label' => 'จัดการผู้ใช้งาน','icon'=> 'fa fa-cog','url' => ['/user/admin/index'],'parentUrl'=>[]],
                        ['label' => 'จัดการสิทธิ์','icon'=> 'fa fa-cog','url' => ['/admin/assignment'],'parentUrl'=>[]],
                    ]],
                ],
                'options'=> [
                        'class' => 'page-sidebar-menu page-header-fixed page-sidebar-menu-light page-sidebar-closed page-sidebar-menu-closed',
                        'data-keep-expanded' => 'false',
                        'data-auto-scroll' => 'true',
                        'data-slide-speed' => 200,
                        'style' => 'padding-top: 20px'
                ]
            ]);
        ?>
    </div>
</div>