<?php
//use common\widgets\Alert;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@frontend/assets');
?>
<?php $this->beginContent('@frontend/views/layouts/_body.php',[
  'page' => 'login',
]); ?>
<?php $this->registerCssFile($directoryAsset.'/pages/css/login.min.css?v1.0', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]); ?>
<?= $content; ?>
<?php $this->registerJsFile($directoryAsset.'/global/plugins/jquery-validation/js/jquery.validate.min.js?v1.0', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile($directoryAsset.'/pages/scripts/login.min.js?v1.0', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->endContent(); ?>