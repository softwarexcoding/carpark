<?php
//use common\widgets\Alert;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@frontend/assets');
?>
<?php $this->beginContent('@frontend/views/layouts/_body.php',[
  'page' => 'main',
]); ?>
<!-- Main Wrapper -->
<div class="page-wrapper">
    <!-- Header -->
    <?=$this->render('_header',['directoryAsset' => $directoryAsset])?>
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- Navigation -->
    <div class="page-container">

        <?=$this->render('_menu')?>
        
        <?=$this->render('_content',['content' => $content])?>

        <a href="javascript:;" class="page-quick-sidebar-toggler">
            <i class="icon-login"></i>
        </a>
        <!-- Footer-->
        <?=$this->render('_footer')?>
    </div>
</div>

<?php $this->endContent(); ?>