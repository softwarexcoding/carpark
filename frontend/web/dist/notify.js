AppNotify = {
    Error : function(message = 'เกิดข้อผิดพลาด'){
        $.notify({
            // options
            icon: 'glyphicon glyphicon-exclamation-sign',
            title: 'Error!',
            message: message,
            url: '',
            target: '_blank'
        },{
            element: 'body',
            position: 'fixed',
            type: "danger",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "top",
                align: "right"
            },
            offset: 20,
            spacing: 10,
            z_index: 15000,
            delay: 5000,
            timer: 1000,
            url_target: '_blank',
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' + '<hr class="kv-alert-separator">' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress kv-progress-bar" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>' 
        });
    },
    SaveCompleted : function(message = 'บันทึกข้อมูลเสร็จเรียบร้อย!'){
        $.notify({
            // options
            icon: 'fa fa-users',
            title: 'Success',
            message: message,
            url: '',
            target: '_blank'
        },{
            element: 'body',
            position: 'fixed',
            type: "success",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "top",
                align: "right"
            },
            offset: 20,
            spacing: 10,
            z_index: 15000,
            delay: 5000,
            timer: 1000,
            url_target: '_blank',
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' + '<hr class="kv-alert-separator">' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress kv-progress-bar" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>' 
        });
    },
    Show : function(type = 'success',icon = 'fa fa-users',title = 'Success',message = ''){
        $.notify({
            // options
            icon: icon,
            title: title,
            message: message,
            url: '',
            target: '_blank'
        },{
            element: 'body',
            position: 'fixed',
            type: type,
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "top",
                align: "right"
            },
            offset: 20,
            spacing: 10,
            z_index: 15000,
            delay: 6000,
            timer: 1000,
            url_target: '_blank',
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' + '<hr class="kv-alert-separator">' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress kv-progress-bar" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>' 
        });
    }
};