echo "Installing..."
php init --env=Development --overwrite=All
composer install

echo "RUN:  Migration initialization Default data."
php yii migrate/up --migrationPath=@common/modules/yii2-user/migrations --interactive=0
php yii migrate --migrationPath=@yii/rbac/migrations --interactive=0
php yii migrate/up --migrationPath=@common/modules/yii2-admin/migrations --interactive=0
php yii migrate/up --migrationPath=@common/modules/yii2-menu/migrations --interactive=0
php yii migrate/up --migrationPath=@common/modules/yii2-core/migrations --interactive=0

echo "Update..."

#composer update

echo "Install Success...!"